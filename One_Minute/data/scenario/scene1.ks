[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[bg  storage="room.jpg"  time="1000"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
Masukkan nama anda :[p]
[_tb_end_text]

[edit  left="572"  top="303"  width="200"  height="40"  size="20"  maxchars="200"  name="f.Nama"  reflect="false"  ]
[button  storage="scene1.ks"  target="*lanjut"  graphic="button/approved-151676_1280.png"  width="98"  height="98"  x="1159"  y="5"  _clickable_img=""  name="img_5"  ]
[s  ]
*lanjut

[commit  ]
[cm  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
HOAMM, sangat membosankan hari ini[p]
(Di dalam kelas yang sunyi tersebut sang tokoh utama terlihat sedang duduk bersandar sambil memasang ekspresi bosan.[p]
Begini lah kehidupan ku tidak ada yang spesial.aku adalah mahasiswa baru di universitas bla bla jurusan bla bla dan aku juga tidak punya banyak teman, hanya teman dari satu SMA yang sama.[p]
Ini adalah hari ketiga ku masuk kuliah setelah orientasi kampus, Aku juga tidak memiliki hobi yang spesifik ataupun kegiatan yang aku sukai. Serasa hidup ini begitu kosong tak ada tujuan yang ingin aku raih.[p]
Pikiran ku hanya fokus untuk menyelesaikan kuliah setelah mendapatkan gelar, aku akan kerja mengumpulkan uang, kemudian menikah ,memiliki anak, dan hidup bahagia...[p][p]
Siklus hidup robot....)[p][p]
Huuuu[p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="SOUND_EFFECT_door_open_close.ogg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#
(Suara Pintu)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[chara_show  name="dina"  time="1000"  wait="true"  storage="chara/1/dina_kamp_tatap_tajam.png"  width="187"  height="600"  left="575"  top="137"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#???
Hoiiiiiiii  [emb exp = "f.Nama"][p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Apa?[p]

[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#???
Bukannya  kita ada janjian untuk tugas kelompok dirumah adi, [p] Apa kamu tidak mau pulang sekarang?[p]
[_tb_end_text]

[tb_start_tyrano_code]
@camera zoom =2 x=70 y=30 time= 900
[_tb_end_tyrano_code]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Dia adalah dina teman semasa SMA dulu, Orangnya sangat berbakat lulusan terbaik di sekolah ku dulu dan dia juga seorang atlit bulu tangkis.[p]
Berbeda dengan ku yang hanyalah seorang pemuda biasa tanpa bakat.[p]
Tapi aku kadang berpikir kenapa wanita secantik dan sepintar dia belum memiliki pacar sampai sekarang hehehe.)[p]
[_tb_end_text]

[tb_start_tyrano_code]
@reset_camera
[_tb_end_tyrano_code]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Din kamu duluan saja entar aku yang nyusul.[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Dina
Huff kamu selalu saja seperti ini [emb exp = "f.Nama"],[p]
Kalau begitu aku duluan tapi jangan lupa WR aku.[p] kalau mau kerumah adi.[p]
[_tb_end_text]

[tb_show_message_window  ]
[chara_hide  name="dina"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Sebenarnya aku memiliki alasan tersendiri selalu menolak ajakan dina untuk pulang bersama walaupun rumah kami di satu komplek yang sama. [p]
Dia begitu populer , saat masa orientasi saja banyak dari kakak kelas ataupun satu angkatan yang berusaha mendekatinya namun hasilnya nihil.[p]
Pada saat sehari setelah masa orientasi aku pernah berangkat sekolah bersama dengan dina dengan vespa tua ku, ketika memasuki area parkir kampus semua laki-laki melirik ku dengan ganasnya seolah-olah seperti singa yang melihat rusa kecil.[p] dari situ lah aku mulai menolak ajakan dina hingga sekarang karena aku tidak mau menjadi sorotan ataupun terlibat masalah.)[p][p]
sepertinya sudah mulai sore[p]
(Aku pun melihat jam tanganku)[p]
Waduhhhh gawat aku kebanyakan melamun [p] bisa-bisa yang lainnya marah nih[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="rouka.jpg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Ku berlari cepat melewati lorong kampus dan menuruni tangga-tangga kampus.)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="8c61690c14560f9035a37f25f2054165.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Akhirnya aku pun sampai di area parkiran.[p] Dengan nafas tergesa-gesa aku berusaha cepat menghidupkan vespa tua ku.)[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Namun aku terhenti sejenak saat melihat ke arah matahari.[p]
Seperti ada sebuah bayangan gelap menutupi matahari.)[p][p]
Ahhhh mungkin hanya khayalan ku.[p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Motorcycle_4__No_Copyright_Sound_Effect.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="331595a977f12fd606156b029d8a6a34.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Aku pun bergegas menuju rumah Adi. Adi adalah teman semasa SMA ku, dia adalah bintang futsal di sekolah ku dulu banyak prestasi yang ia cetak dalam bidang olahraga lebih tepatnya futsal. [p]
Jika aku di bandingkan dengan dia bagaikan bumi dan langit [p][p]
tapi dia adalah sahabat karib ku dari SMA ia tidak memandang ku sebelah mata tidak seperti yang lainnya.[p] Berteman untuk membesarkan nama ataupun untuk mendatangkan untung.)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[bg  time="1000"  method="crossfade"  storage="1bcaa962a6fb5dd74b3ac7e7e1260792.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Setelah beberapa menit di perjalanan akupun sampai di depan rumah adi. [p] Ohhh satu lagi dia juga anak konglomerat bapaknya memiliki perusahaan minyak di kalimantan, ia hidup dengan kemewahan.[p]
Andai saja aku yang berada di posisinya hehehe.)[p]
Pak Herman !!!!![p]
[_tb_end_text]

[chara_show  name="PakHerman"  time="1000"  wait="true"  storage="chara/2/Space_Captain.png"  width="448"  height="546"  left="292"  top="189"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Pak Herman
Mas [emb exp = "f.Nama"] [p]tunggu yah saya buka kan pagar[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Metal_gate_sound_effect.ogg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#
(Suara pintu pagar)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Pak Herman
Monggo mas[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Adi ada di dalam?[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Pak Herman
Ada mas dan banyak juga teman-teman lainnya mas adi tadi datang[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Ohh begitu makasih yah pak herman.[p] aku masuk dulu[p]
[_tb_end_text]

[chara_hide  name="PakHerman"  time="1000"  wait="true"  pos_mode="true"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="Motorcycle_4__No_Copyright_Sound_Effect.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="f4baf2644d2589b8896bd67dcc3bb376.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Rumah adi begitu luas dan megah tak bisa di tuturkan oleh kata-kata, setiap kali aku memasuki rumahnya pasti selalu terkagum-kagum berharap suatu hari nanti aku bisa memiliki rumah seperti ini.[p][p]

[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Setelah memakirkan vespa, aku pun bergegas menuju pintu rumah adi dan menekan bell.)[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Sound_effects__Bell_House.ogg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]

(Suara Bell pintu)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="SOUND_EFFECT_door_open_close.ogg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]

(Suara pintu terbuka)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/3/adi_baju_biasa_senyum_kecil.png"  width="242"  height="600"  left="388"  top="154"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Adi
[emb exp = "f.Nama"]...[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Yoo maaf aku telat tadi kena macet di[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="slap.ogg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Adi
Alaah alasan aja kamu[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
ouch[p]sialan,kamu mau ngajak kelai sama saya?[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Adi
hahahaha macam kamu bisa kelai aja [emb exp = "f.Nama"][p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Hahahaha[p] (Begini lah kami jika bertemu selalu bercanda memang beda antara sahabat dan teman.[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Adi
Ayo masuk[p]
[_tb_end_text]

[chara_hide  name="Adi"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Aku pun mengikutinya.)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="e577d4eaacc920e18d517738d751ecda.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Walaupun aku sudah berkali-kali ke rumah adi,[p]
aku selalu tercengang melihat seisi rumahnya yang begitu mewah.)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="eca54be56c683f9fea5b3339236f3c48.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Kami pun tiba di kamarnya.[p]Ketika kami berdua masuk ke dalam kamar adi.[p] Suasana pada saat itu tengah ribut dengan canda tawa[p][p]
namun tiba-tiba menjadi sunyi ketika anggota kelompok lainnya melihat ku datang.)[p][p]
Permisi[p]
[_tb_end_text]

[chara_show  name="reni"  time="1000"  wait="true"  storage="chara/6/reni_baju_biasa_tatap_tajam.png"  width="275"  height="600"  left="187"  top="144"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#???
hmmmm ini lah mengapa aku tidak suka satu kelompok dengan orang yang tidak memiliki bakat ataupun kemampuan?[p]
apapun selalu menganggap remeh[p]
[_tb_end_text]

[tb_start_tyrano_code]
@camera zoom=2 x=-187 y=43 time=900
[_tb_end_tyrano_code]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Name]
(Dia adalah reni wanita sombong yang  aku temui semasa hidupku.[p] Sebenarnya aku mengenalnya baru-baru saja[p]hanya karena satu kelompok tugas namun dari kabar yang beredar dia adalah anak seorang konglomerat dan juga termasuk siswi terpintar di sekolahnya dulu.)[p]
[_tb_end_text]

[tb_start_tyrano_code]
@reset_camera
[_tb_end_tyrano_code]

[chara_show  name="erik"  time="1000"  wait="true"  storage="chara/4/erick_baju_kampus_senyum.png"  width="328"  height="561"  left="627"  top="167"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
Sudah ren..[p] kamu tidak boleh begitu[p]

[_tb_end_text]

[tb_start_tyrano_code]
@camera zoom=2 x= 187 y=43 time=900
[_tb_end_tyrano_code]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Pria itu adalah Erik pacar dari reni berbeda dengan reni walaupun ia juga seorang anak konglomerat tapi ia tidak sombong, walau prestasi akademiknya tidak ada yang menonjol.)[p]
[_tb_end_text]

[chara_mod  name="reni"  time="600"  cross="true"  storage="chara/6/reni_baju_biasa_malu_terpojok.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#reni
Hmmm...[p]
[_tb_end_text]

[chara_show  name="siela"  time="1000"  wait="true"  storage="chara/5/seila_biasa_sedih.png"  width="225"  height="600"  left="-3"  top="119"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Dan yang terakhir duduk di pojok ruangan adalah siela wanita culun yang selalu saja membaca buku di setiap harinya, kadang aku penasaran apakah dia tidak bosan membaca)[p][p]
(Kelompok ku ini terdari dari 6 orang aku,adi,dina,reni,erik dan siela.)[p][p]
(Menurutku sendiri ini adalah kelompok terburuk dalam hidupku,[p] dimana semua anggotanya orang spesial kecuali diriku yang hanya biasa-biasa saja.)[p]
[_tb_end_text]

[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/3/adi_baju_biasa_senyum_kecil.png"  width="233"  height="577"  left="1018"  top="134"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#Adi
Kalau begitu ayo kita mulai mengerjakan tugasnya karena name juga sudah datang[p]
[_tb_end_text]

[chara_hide  name="Adi"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="erik"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="siela"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="reni"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Setelah beberapa jam kami mengerjakan akhirnya selesai.[p] Pukul sudah menunjukkan 18.04)[p][p]

[_tb_end_text]

[tb_show_message_window  ]
[chara_show  name="erik"  time="1000"  wait="true"  storage="chara/4/erick_baju_kampus_senyum.png"  width="350"  height="600"  left="501"  top="174"  reflect="false"  ]
[tb_start_text mode=1 ]
#erik
waduhh udah mau malam[p]
[_tb_end_text]

[chara_show  name="reni"  time="1000"  wait="true"  storage="chara/6/reni_baju_biasa_tatap_tajam.png"  width="275"  height="600"  left="-1"  top="145"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#reni
beb, ayo pulang![p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#erika
Iya[p]
[_tb_end_text]

[chara_show  name="siela"  time="1000"  wait="true"  storage="chara/5/seila_senyum_biasa.png"  width="225"  height="600"  left="853"  top="159"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#siela
Aku juga mau pulang[p]
[_tb_end_text]

[chara_show  name="dina"  time="1000"  wait="true"  storage="chara/1/dina_biasa_tenang.png"  width="187"  height="600"  left="282"  top="142"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#dina
iya nih adi, udah malam.[p]
[_tb_end_text]

[chara_hide  name="erik"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/3/adi_baju_biasa_senyum_kecil.png"  width="242"  height="600"  left="574"  top="150"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#adi
din mau ku antar pulang?[p]

[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#dina
tidak usah lah di. lagian ada tokoh utama aku bisa pulang bareng sama dia[p] kan name[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
Aku..?[p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#adi
ohh ya kalau begitu din.[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="e577d4eaacc920e18d517738d751ecda.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Kami pun berpamitan kepada adi dan orang tuanya.)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="f4baf2644d2589b8896bd67dcc3bb376.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
(Aku pun membonceng dina hingga keluar gerbang rumah adi.)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="e8b86c1168489f02178783cf2d1ac199.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Sesampainya kami di pinggir jalan.)[p][p]
din..[p][p]
aku ada urusan bentar nih kamu bisa kan pesan ojek online atau apa gitu buat pulang sendiri.[p]
[_tb_end_text]

[chara_show  name="dina"  time="1000"  wait="true"  storage="chara/1/dina_biasa_tatap_tajam.png"  width="187"  height="600"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#dina
Kamu tuh..,[p][p]
selalu aja begini masa kamu tega suruh perempuan pulang sendiri malam-malam!!!!![p]
[_tb_end_text]

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.Nama]
(Aku hanya bisa terdiam melihat dina marah dan berjalan pergi meninggalkan ku.)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="5de54a1347d809e276b7f76d0b0aec98.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
Itulah salah satu penyesalan terbesar dalam hidupku dan terakhir kalinya aku melihat dina.[p]
Dua minggu setelah kerja kelompok tersebut, seluruh kehidupan ku berubah.[p]
Disinilah awal dari kisah ku yang sebenarnya...[p][p]
[_tb_end_text]

[movie  volume="100"  ]
[s  ]
[tb_start_text mode=1 ]

[_tb_end_text]

[iscript]
[endscript]

