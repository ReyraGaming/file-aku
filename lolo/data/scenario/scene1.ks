[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[edit  left="373"  top="342"  width="174"  height="34"  size="20"  maxchars="200"  reflect="false"  name="f.nama"  ]
[button  storage="scene1.ks"  target="*Lanjut"  graphic="title/approved-151676_1280.png"  width="96"  height="97"  x="823"  y="14"  _clickable_img=""  name="img_3"  ]
[s  ]
*Lanjut

[commit  ]
[cm  ]
[tb_show_message_window  ]
[bg  storage="2d8c95500ba5e8758916327e4bbaa919.png"  time="1000"  ]
[tb_start_text mode=1 ]
#&[f.nama]
Hoaammm[p]...[p]
Sangat membosankan hari ini.[p][p]
(Beginilah kehidupanku,[p] tidak ada yang spesial.[p]
Aku adalah mahasiswa baru di Universitas Teknologi dan Game,  jurusan Sistem Informasi,[p] dan aku juga tidak punya banyak teman.[p]
Hanya teman dari satu teman SMA yang sama.[p]
Ini adalah hari ketigaku masuk kuliah setelah orientasi kampus.[p]Aku juga tidak memiliki hobi yang spesifik ataupun kegiatan yang aku sukai.[p]
Serasa hidup ini begitu kosong tak ada tujuan yang ingin aku raih.[p]
Pikiranku hanya fokus untuk menyelesaikan kuliah setelah mendapatkan gelar.[p]
Aku akan kerja mengumpulkan uang[p]kemudian  menikah dan memiliki anak serta hidup bahagia[p]...[p]
Itulah pikiran kebanyakan orang saat ini[p][p]
Siklus hidup robot.....)[p]


[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Pintu.ogg"  ]
[stopse  time="1000"  buf="0"  ]
[chara_show  name="Dina"  time="1000"  wait="true"  storage="chara/1/dina_kamp_tatap_tajam.png"  width="187"  height="600"  left="404"  top="50"  reflect="false"  ]
[tb_start_text mode=1 ]
#??
Woy [emb exp ="f.nama"] !!![p]
#&[f.nama]
Apa?[p]
#??
Bukannya kita ada janjian untuk tugas kelompok dirumah adi?[p]
Apa kamu tidak mau pulang sekarang?[p]
[_tb_end_text]

[tb_start_tyrano_code]
@camera zoom=2 x=40 y=80 time=1000
[_tb_end_tyrano_code]

[tb_start_text mode=1 ]
#&[f.nama]
(Dia adalah Dina, teman semasa SMA dulu.[p]
Orangnya berbakat,[p]
Lulusan terbaik di sekolahku dulu dan dia juga seorang atlet Bulu Tangkis.[p]
Berbeda dengan saya yang hanyalah pemuda biasa tanpa bakat.[p]
Tapi aku kadang berpikir,[p] kenapa wanita secantik dan sepintar dia belum pacar sampai sekarang ya?[p]
Hehehe)[p]

[_tb_end_text]

[tb_start_tyrano_code]
@reset_camera
[_tb_end_tyrano_code]

[tb_start_text mode=1 ]
#&[f.nama]
Din, kamu duluan aja, nanti aku nyusul![p]


[_tb_end_text]

[chara_mod  name="Dina"  time="600"  cross="true"  storage="chara/1/dina_baju_kampus_sedikit_kecewa.png"  ]
[tb_start_text mode=1 ]
#Dina
Hufff,[p]
Kamu selalu saja seperti ini [emb exp ="f.nama"]![p]
[_tb_end_text]

[chara_mod  name="Dina"  time="600"  cross="true"  storage="chara/1/dina_kamp_tatap_tajam.png"  ]
[tb_start_text mode=1 ]
#Dina
Kalau begitu aku duluan tapi jangan lupa UltraBook aku[p] jika kamu mau ke rumah adi.[p]


[_tb_end_text]

[chara_hide  name="Dina"  time="1000"  wait="true"  pos_mode="true"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="Pintu.ogg"  ]
[stopse  time="1000"  buf="0"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Sebenarnya aku pun memiliki alasan tersendiri[p]untuk menolak ajakan dina untuk  pulang bersama[p]
walaupun rumah kami di satu komplek yang sama.[p]
Dia begitu populer.[p]
Pada masa orientasi saja banyak kakak tingkat atau satu angkatan yang berusaha mendekatinya namun hasinya nihil.[p]
Pada saat sehari setelah masa orientasi, aku pernah berangkat kuliah bersama dina dengan vespa tua aku.[p]
Ketika memasuki area kampus, semua laki - laki melirik aku dengan ganasnya seperti singa yang melihat rusa kecil.[p]
Dari situlah aku menolak ajakan Dina hingga sekarang  karena aku tidak mau menjadi sorotan atau terlibat masalah.)[p][p]
Sepertinya sudah mulai sore[p]...[p]
Waduh gawat aku kebanyakan melamun, bisa - bisa yang lainnya marah nih!![p]
[_tb_end_text]

[jump  storage="Scene2.ks"  target="*Lanjutbro"  ]
[s  ]
