[_tb_system_call storage=system/_Scene4.ks]

[cm  ]
*aha

[bg  time="1000"  method="crossfade"  storage="d1bf1ab8c3270b4c962ec4ec62c02576.png"  ]
[chara_show  name="Sheila"  time="1000"  wait="true"  storage="chara/3/seila_senyum_biasa.png"  width="225"  height="600"  left="720"  top="74"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Dan yang terakhir duduk di pojok ruangan adalah Sheila, wanita culun yang selalu saja membaca buku setiap hari.[p]
Kadang aku penasaran apakah dia tidak bosan membaca terus.)[p]

[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Kelompok aku ada 6 yaitu Aku, Adi, Dina, Erik, Rina, dan Sheila.)[p]
(Menurutku kelompok ini adalah kelompok terburuk dalam hidupku,[p]
dimana semua anggotanya adalah orang yang spesial kecuali aku yang biasa - biasa saja.)[p]


[_tb_end_text]

[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/2/adi_baju_biasa_senyum_kecil.png"  width="242"  height="600"  ]
[tb_start_text mode=1 ]
#Adi
Kalau begitu ayo kita mulai mengerjakan tugasnya karena [emb exp = "f.nama"] sudah datang.[p]

[_tb_end_text]

[chara_hide  name="Adi"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Setelah beberapa jam kami  mengerjakan, akhirnya selesai juga)[p]
[_tb_end_text]

[jump  storage="Scene5.ks"  target="*HaloHalo"  ]
[s  ]
