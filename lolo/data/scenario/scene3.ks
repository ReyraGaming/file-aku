[_tb_system_call storage=system/_scene3.ks]

[cm  ]
*gas

[bg  time="1000"  method="crossfade"  storage="f4baf2644d2589b8896bd67dcc3bb376.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Setelah memarkirkan vespa, aku pun bergegas menuju pintu dan menekan tombol bel.)[p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Sound_effects__Bell_House.ogg"  ]
[stopse  time="1000"  buf="0"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="Pintu.ogg"  ]
[stopse  time="1000"  buf="0"  ]
[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/2/adi_baju_santai_senyum.png"  width="242"  height="600"  left="381"  top="96"  reflect="false"  ]
[tb_start_text mode=1 ]
#Adi
[emb exp = "f.nama"]...[p]
#&[f.nama]
Yoo..[p]
Maaf aku terlambat,[p]
tadi terjebak macet di jalan...[p]
[_tb_end_text]

[chara_mod  name="Adi"  time="600"  cross="true"  storage="chara/2/adi_baju_biasa_senyum_kecil.png"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="slap.ogg"  ]
[tb_start_text mode=1 ]
#Adi
Alah, banyak betul alasanmu![p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_start_text mode=1 ]
#&[f.nama]
Aduh, sakit tahu !![p]
#Adi
Hahaha, baru begitu saja sudah sakit,[p]
kayak cewek aja![p]
#&[f.nama]
Hahahaha[p]
(Beginilah kami jika bertemu selalu bercanda.[p]
Memang beda antara sahabat dan teman.) [p]
#Adi
Ayo masuk.[p]
#&[f.nama]
(Aku pun masuk dan mengikutinya)[p]
[_tb_end_text]

[chara_hide  name="Adi"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="e577d4eaacc920e18d517738d751ecda.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Walaupun aku sudah berkali - kali ke rumah adi,[p]
aku selalu tercengang melihat isi rumahnya yang begitu mewah.)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="d1bf1ab8c3270b4c962ec4ec62c02576.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Kami pun tiba di kamarnya)[p][p]
(Ketika kamu berdua masuk ke dalam adi, suasana pada saat itu tengah ribut dengan canda tawa.[p]
Namun tiba-tiba menjadi sunyi ketika anggota kelompok lainnya melihat aku datang)[p]
Permisi...[p]

[_tb_end_text]

[chara_show  name="Reni"  time="1000"  wait="true"  storage="chara/4/reni_baju_biasa_tatap_tajam.png"  width="275"  height="600"  left="38"  top="77"  reflect="false"  ]
[tb_start_text mode=1 ]
#??
Hmmm...[p]
Aku jadi malas berkelompok dengan orang yang tidak memiliki bakat ataupun kemampuan.[p]
Tidak berguna!![p]
#&[f.nama]
(Dia adalah reni, Wanita sombong yang aku temui semasa hidupku.[p]
Sebenarnya aku baru saja mengenalnya saat satu kelompok tugas. [p]
Namun kabar yang beredar bahwa dia adalah anak seorang konglomerat dan juga termasuk siswi terpintar saat sekolahnya dulu.) [p]

[_tb_end_text]

[chara_show  name="Erick"  time="1000"  wait="true"  storage="chara/5/erick_baju_kampus_senyum.png"  width="350"  height="600"  left="353"  top="82"  reflect="false"  ]
[tb_start_text mode=1 ]
#??
Sudah ren..[p]
Kamu tidak boleh begitu.[p]
#&[f.nama]
Pria itu namanya Erik, pacarnya reni.[p]
Berbeda dengan reni walaupun dia juga seorang anak konglomerat tapi dia tidak sombong.[p]
Kalau prestasinya sih tidak ada yang menonjol.[p]
#Reni
Hmmm..[p]
[_tb_end_text]

[jump  storage="Scene4.ks"  target="*aha"  ]
[s  ]
