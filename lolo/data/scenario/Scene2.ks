[_tb_system_call storage=system/_Scene2.ks]

[cm  ]
*Lanjutbro

[bg  time="1000"  method="crossfade"  storage="37a6ca3395a0700b6999cb94abc04fc2.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Aku berlari cepat melewati lorong kampus dan menuruni tangga kampus.)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="8c61690c14560f9035a37f25f2054165.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Akhirnya aku pun sampai di area parkiran.[p]
Dengan nafas tergesa-gesa aku berusaha cepat menghidupkan vespa tuaku.)[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_start_sfx.ogg"  ]
[stopse  time="1000"  buf="0"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Namun aku terhenti sejenak saat melihat matahari.)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="solar-eclipse-2575133_1920.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Seperti ada bayangan yang menutupi matahari.)[p]
Ahh mungkin cuman khayalanku saja.[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_Jalan.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="331595a977f12fd606156b029d8a6a34.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Aku pun bergegas ke rumah adi.)[p]...[p]
(Adi adalah teman semasa SMA aku, [p]
Dia adalah bintang futsal di sekolahku dulu, banyak prestasi yang dia cetak dalam bidang olahraga lebih tepatnya futsal.[p]
Jika aku dibandingkan dengan dia maka seperti Bumi dan Langit,[p]...[p]
tapi dia adalah sahabat karibku dari SMA, ia tidak memandangku sebelah mata tidak seperti lainnya.)[p]
(Setelah beberapa menit di perjalanan....[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[bg  time="1000"  method="crossfade"  storage="1bcaa962a6fb5dd74b3ac7e7e1260792.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
Aku pun sampai di rumah Adi.[p][p]
Ohh ya satu lagi, dia juga anak konglomerat,[p]
bapaknya memilki perusahaan minyak di Kalimantan,[p]
Dia hidup dengan kemewahan.[p]
Andai saja aku berada di posisinya,[p]
Hehehe.)[p]
PAK HERMAN !!![p]
[_tb_end_text]

[tb_start_text mode=1 ]
#Pak Herman
Mas [emb exp = "f.nama"] ...[p]
Tunggu yah, saya buka pagar dulu![p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Pagar.ogg"  ]
[stopse  time="1000"  buf="0"  ]
[chara_show  name="Herman"  time="1000"  wait="true"  storage="chara/6/Lance_character.png"  width="427"  height="673"  left="321"  top="151"  reflect="false"  ]
[tb_start_text mode=1 ]
#Pak Herman
Monggo mas [emb exp = "f.nama"] .[p]
#&[f.nama]
Adi ada di dalam?[p]
#Pak Herman
Ada mas di dalam, [p]
kebetulan temannya ada juga di dalam.[p]
#&[f.nama]
Oh kalau begitu terima kasih mas,[p]
Saya masuk dulu mas![p]
#Pak Herman
Iya, silakan mas![p]

[_tb_end_text]

[chara_hide  name="Herman"  time="1000"  wait="true"  pos_mode="true"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_Jalan.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="f4baf2644d2589b8896bd67dcc3bb376.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Rumah Adi begitu luas dan megah yang tak bisa dituturkan oleh kata-kata,[p]
setiap kali aku memasuki rumahnya pasti selalu terkagum-kagum melihatnya[p]
dan aku pun berharap bisa memiliki rumah seperti ini.)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[jump  storage="scene3.ks"  target="*gas"  ]
[s  ]
