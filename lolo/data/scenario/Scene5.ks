[_tb_system_call storage=system/_Scene5.ks]

[cm  ]
*HaloHalo

[bg  time="1000"  method="crossfade"  storage="37e46332b6f37c42932fb0e27e8abeab.png"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Waktu menunjukan pukul 18.04)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="d1bf1ab8c3270b4c962ec4ec62c02576.png"  ]
[chara_show  name="Erick"  time="1000"  wait="true"  storage="chara/5/erick_baju_kantor_bosan.png"  width="350"  height="600"  left="1"  top="94"  reflect="false"  ]
[tb_start_text mode=1 ]
#Erik
Waduh mau malam..[p]
Beb, ayo kita pulang![p]
[_tb_end_text]

[chara_show  name="Reni"  time="1000"  wait="true"  storage="chara/4/reni_tenang_biasa.png"  width="275"  height="600"  left="691"  top="54"  reflect="false"  ]
[tb_start_text mode=1 ]
#Reni
Iya, ayo![p]
[_tb_end_text]

[chara_show  name="Sheila"  time="1000"  wait="true"  storage="chara/3/seila_senyum_biasa.png"  width="225"  height="600"  left="444"  top="58"  reflect="false"  ]
[tb_start_text mode=1 ]
#Sheila
Iya nih sudah mau malam.[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/2/adi_baju_biasa_senyum_kecil.png"  width="242"  height="600"  left="-2"  top="73"  reflect="false"  ]
[tb_start_text mode=1 ]
#Adi
Dina, kamu mau aku antarkan pulang tidak?[p]
[_tb_end_text]

[chara_show  name="Dina"  time="1000"  wait="true"  storage="chara/1/dina_baju_biasa_senang.png"  width="187"  height="600"  left="711"  top="61"  reflect="false"  ]
[tb_start_text mode=1 ]
#Dina
Tidak usah, lagian ada [emb exp = "f.nama"] yang mengantarkan aku pulang, ya kan [emb exp = "f.nama"]?[p]
#&[f.nama]
Aku...?![p]
#Adi
Oh ya kalau begitu din, hati - hati di jalan![p]
#Dina
Ya.[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="e577d4eaacc920e18d517738d751ecda.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Kami pun berpamitan kepada Adi dan orang tuanya.)[p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_Jalan.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="1bcaa962a6fb5dd74b3ac7e7e1260792.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Aku pun membonceng Dina hingga keluar gerbang rumah Adi)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="e8b86c1168489f02178783cf2d1ac199.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Sesampainya pinggir jalan.)[p]

[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_start_text mode=1 ]
#&[f.nama]
Din...[p]
Aku ada urusan bentar, kamu bisa kan pesan Ojek Online?[p]

[_tb_end_text]

[chara_show  name="Dina"  time="1000"  wait="true"  storage="chara/1/dina_biasa_tatap_tajam.png"  width="187"  height="600"  left="407"  top="42"  reflect="false"  ]
[tb_start_text mode=1 ]
#Dina
Kamu tuh...[p]
Selalu saja begini, masa kamu tega suruh perempuan pulang sendirian di malam hari![p]
#&[f.nama]
(Aku hanya bisa terdiam melihat Dina marah dan berjalan meninggalkanku.)[p]
[_tb_end_text]

[chara_hide  name="Dina"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="videoblocks-4k-green-particle-smoothy-flow-opacity-to-change-black-screen-background-to-green-screen-background-concept-for-changing-to-advertisement-by-insert-another-business-video_buh2nb1tg_thumbnail-full.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Itulah salah satu penyesalan terbesar dalam hidupku[p]
dan terakhir kalinya aku melihat dina.)[p][p]
(2 minggu setelah kerja kelompok tersebut.[p]
Seluruh hidupku berubah.[p]
Disinilah awal kisahku sebenarnya[p].....)[p]

[_tb_end_text]

[movie  volume="100"  storage="trailer-full.webm"  ]
