
; == title.ks ==============================================

; This scenario file creates the greeting screen.

; ==========================================================



; [jump] If the greeting has already been seen, then jump to the contents page.
; (Note the cond parameter.
;  cond is short for condition. If the condition is TRUE, the tag will activate.
;  In this case, if the value of the system variable sf.title_moumita is 1, 
;  the tag will activate.)
[jump cond=" sf.title_moumita == 1 " storage="select.ks"]



; ----------------------------------------------------------
*Start
; ----------------------------------------------------------


; [bg] Show the background.
; The time is 700ms, the image is rouka.jpg.
[bg time="700" storage="rouka.jpg"]

; [chara_show] Display a character.
[chara_show name="akane" time="700"]

; [layopt] Set message layer 0 to visible.
[layopt layer="message0" visible="true"]

; [set_message_window] Set the message window.
; This is a standalone macro (the scenario file that defines it is macro.ks).
; Processes related to the message window are collected in the macro.
[set_message_window]

; Display #？？？ as the speaker's name.
#? ? ?

; [delay] Adjust the text display apeed to 200ms per character.
[delay speed="200"]



・・・・・・・・・[p]



; [resetdelay] Reset the text display speed to default.
[resetdelay]

; [chara_mod] Change Akane's expression to happy.
[chara_mod name="akane" face="happy" time="700"]



Hello there!![p]



; [chara_mod] Change Akane's expression to default.
[chara_mod name="akane" face="default" time="700"]



I'm Akane. [l]
I'll be your guide.[p]



; #akane:default Change the name box at the same time as changing the expression.
; Change the name box contents to 'Akane' and change Akane's expression to default.
; This is a special syntax that contracts[chara_ptext name="akane"][chara_mod name="akane" face="default"]
#akane:default
Today we'll be going through...[p]



#akane:happy



The TyranoScript Technical Samples Collection Volume 1!![p]



#akane:default



We'll take a look at many fundamental & useful TyranoScript techniques![p]
All righty. Let's head over to the contents page!![p]



; [eval]
; If the greeting has already been seen,
; assign the value 1 to the sf.title_moumita.
[eval exp=" sf.title_moumita = 1 "]

; [jump]
; Jump to the table of contents.
[jump storage="select.ks"]