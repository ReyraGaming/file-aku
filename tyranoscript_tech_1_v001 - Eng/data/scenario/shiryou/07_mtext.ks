; [reset_all] Macro that resets all game screens and returns the game to the startup state. (See 「macro.ks」 for details.)
[reset_all]

Text Object Reference.[p]

; [layopt]x2 Hide messsage layer 0 and the fix layer.
[layopt layer=1        visible=true ]
[layopt layer=message0 visible=false]
[layopt layer=fix      visible=false]

; [image][anim] Stretch and display a black image the cover the whole screen and immediately set the opacity to 0.
[image layer=0 x=0 y=0 width=960 height=640 storage=color/black.png name=black]
[anim name=black opacity=0 time=0]

; [anim][wa] Adjust the opacity of the black image to 190 over 1,000ms, and wait for the animation to complete.
[anim name=black opacity=190 time=1000 effect=easeOutExpo]
[wa]



*Font_1

; [call] Call the subroutine that places multiple tutorial text objects.
[call target="*Sub_Font_Ptext"]

; [glink][s]Place the 'Play' link and stop the game.
[glink color="black" text="Play" x="310" y="250" target="*Font_2" width="240"]
[s]

*Font_2

; [call] Call the subroutine that replays the text object.
[call target="*Sub_Font_Mtext"]

; [glink]x2 [s]Place teh 'Replay' and 'Next' links, and stop the game.
[glink color="black" text="Replay" x="310" y="250" target="*Font_2"   width="240"]
[glink color="red"   text="Next"             x="310" y="350" target="*Effect_1" width="240"]
[s]

*Effect_1

[freeimage layer="1"]
[call target="*Sub_Effect_1_Ptext"]
[glink color="black" text="Replay" x="310" y="250" target="*Effect_2" width="240"]
[s]

*Effect_2

[call target="*Sub_Effect_1_Mtext"]
[glink color="black" text="Replay" x="310" y="250" target="*Effect_2" width="240"]
[glink color="red"   text="Next"             x="310" y="350" target="*Effect_3" width="240"]
[s]

*Effect_3

[freeimage layer="1"]
[call target="*Sub_Effect_2_Ptext"]
[glink color="black" text="Replay" x="310" y="250" target="*Effect_4" width="240"]
[s]

*Effect_4

[call target="*Sub_Effect_2_Mtext"]
[glink color="black" text="Replay" x="310" y="250" target="*Effect_4" width="240"]
[glink color="red"   text="Next"             x="310" y="350" target="*Sync_1"   width="240"]
[s]

*Sync_1

[freeimage layer="1"]
[call target="*Sub_Sync_Ptext"]
[glink color="black" text="Replay" x="310" y="250" target="*Sync_2" width="240"]
[s]

*Sync_2

[call target="*Sub_Sync_Mtext"]
[glink color="black" text="Replay" x="310" y="250" target="*Sync_2"  width="240"]
[glink color="red"   text="Next"             x="310" y="350" target="*Delay_1" width="240"]
[s]

*Delay_1

[freeimage layer="1"]
[call target="*Sub_Delay_Ptext"]
[glink color="black" text="Replay" x="310" y="250" target="*Delay_2" width="240"]
[s]

*Delay_2

[call target="*Sub_Delay_Mtext"]
[glink color="black" text="Replay" x="310" y="250" target="*Delay_2"       width="240"]
[glink color="red"   text="Next"             x="310" y="350" target="*Delay_Scale_1" width="240"]
[s]

*Delay_Scale_1

[freeimage layer="1"]
[call target="*Sub_Delay_Scale_Ptext"]
[glink color="black" text="Replay" x="310" y="250" target="*Delay_Scale_2" width="240"]
[s]

*Delay_Scale_2

[call target="*Sub_Delay_Scale_Mtext"]
[glink color="black" text="Replay" x="310" y="250" target="*Delay_Scale_2" width="240"]
[glink color="red"   text="Next"             x="310" y="350" target="*End"           width="240"]
[s]

*End

[jump storage="select.ks"]



; ※ Below are labels for subroutines ※



; ●
; ●Formatting text
; ●

; ------------------------------------------------------
*Sub_Font_Ptext

[ptext layer="1" x=" 21" y=" 10" size="18" color="0xffcccc" text="Regular"]
[ptext layer="1" x="501" y=" 10" size="18" color="0xffcccc" text="Colored&ensp;Text"]
[ptext layer="1" x=" 21" y=" 85" size="18" color="0xffcccc" text="Stroke"]
[ptext layer="1" x="501" y=" 85" size="18" color="0xffcccc" text="Shadow"]

[ptext layer="1" x=" 21" y="160" size="18" color="0xffcccc" text="Bold"]
[ptext layer="1" x="501" y="160" size="18" color="0xffcccc" text="Vertical&ensp;Text"]

[return]

; ------------------------------------------------------
*Sub_Font_Mtext

[mtext layer="1" x=" 20" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false"]
[mtext layer="1" x="500" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" color="0xff0000"]
[mtext layer="1" x=" 20" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" edge="0xff0000"]
[mtext layer="1" x="500" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" shadow="0xff0000"]

[mtext layer="1" x=" 20" y="180" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" bold="bold"]
[mtext layer="1" x="500" y="180" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="true"  vertical="true"]

[return]






; ●
; ●Formatting text (1)
; ●

; ------------------------------------------------------
*Sub_Effect_1_Ptext

[ptext layer="1" x=" 21" y=" 10" size="18" color="0xffcccc" text="Flash/Flash"]
[ptext layer="1" x="501" y=" 10" size="18" color="0xffcccc" text="Bounce/Bounce"]
[ptext layer="1" x=" 21" y=" 85" size="18" color="0xffcccc" text="Shake/Shake"]
[ptext layer="1" x="501" y=" 85" size="18" color="0xffcccc" text="Tada/Tada"]

[ptext layer="1" x=" 21" y="160" size="18" color="0xffcccc" text="Swing/Swing"]
[ptext layer="1" x="501" y="160" size="18" color="0xffcccc" text="Wobble/Wobble"]
[ptext layer="1" x=" 21" y="235" size="18" color="0xffcccc" text="Pulse/Pulse"]
[ptext layer="1" x="501" y="235" size="18" color="0xffcccc" text="Flip/Flip"]

[ptext layer="1" x=" 21" y="310" size="18" color="0xffcccc" text="FlipInX/FlipOutX"]
[ptext layer="1" x="501" y="310" size="18" color="0xffcccc" text="FlipInY/FlipOutY"]
[ptext layer="1" x=" 21" y="385" size="18" color="0xffcccc" text="FadeIn/FadeOut"]
[ptext layer="1" x="501" y="385" size="18" color="0xffcccc" text="FadeInUp/FadeOutUp"]

[ptext layer="1" x=" 21" y="460" size="18" color="0xffcccc" text="FadeInDown/FadeOutDown"]
[ptext layer="1" x="501" y="460" size="18" color="0xffcccc" text="FadeInLeft/FadeOutLeft"]
[ptext layer="1" x=" 21" y="535" size="18" color="0xffcccc" text="FadeInRight/FadeOutRight"]
[ptext layer="1" x="501" y="535" size="18" color="0xffcccc" text="FadeInUpBig/FadeOutUpBig"]

[return]

; ------------------------------------------------------
*Sub_Effect_1_Mtext

[mtext layer="1" x=" 20" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="flash"  out_effect="flash"  wait="false"]
[mtext layer="1" x="500" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="bounce" out_effect="bounce" wait="false"]
[mtext layer="1" x=" 20" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="shake"  out_effect="shake"  wait="false"]
[mtext layer="1" x="500" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="tada"   out_effect="tada"   wait="false"]

[mtext layer="1" x=" 20" y="180" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="swing"  out_effect="swing"  wait="false"]
[mtext layer="1" x="500" y="180" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="wobble" out_effect="wobble" wait="false"]
[mtext layer="1" x=" 20" y="255" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="pulse"  out_effect="pulse"  wait="false"]
[mtext layer="1" x="500" y="255" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="flip"   out_effect="flip"   wait="false"]

[mtext layer="1" x=" 20" y="330" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="flipInX"  out_effect="flipOutX"  wait="false"]
[mtext layer="1" x="500" y="330" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="flipInY"  out_effect="flipOutY"  wait="false"]
[mtext layer="1" x=" 20" y="405" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeIn"   out_effect="fadeOut"   wait="false"]
[mtext layer="1" x="500" y="405" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInUp" out_effect="fadeOutUp" wait="false"]

[mtext layer="1" x=" 20" y="480" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInDown"  out_effect="fadeOutDown"  wait="false"]
[mtext layer="1" x="500" y="480" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInLeft"  out_effect="fadeOutLeft"  wait="false"]
[mtext layer="1" x=" 20" y="555" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInRight" out_effect="fadeOutRight" wait="false"]
[mtext layer="1" x="500" y="555" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInUpBig" out_effect="fadeOutUpBig" wait="true "]

[return]






; ●
; ● Formatting text (2)
; ●

; ------------------------------------------------------
*Sub_Effect_2_Ptext

[ptext layer="1" x=" 21" y=" 10" size="18" color="0xffcccc" text="FadeInDownBig/FadeOutDownBig"]
[ptext layer="1" x="501" y=" 10" size="18" color="0xffcccc" text="FadeInLeftBig/FadeOutLeftBig"]
[ptext layer="1" x=" 21" y=" 85" size="18" color="0xffcccc" text="FadeInRightBig/FadeOutRightBig"]
[ptext layer="1" x="501" y=" 85" size="18" color="0xffcccc" text="BounceIn/BounceOut"]

[ptext layer="1" x=" 21" y="160" size="18" color="0xffcccc" text="BounceInDown/BounceOutDown"]
[ptext layer="1" x="501" y="160" size="18" color="0xffcccc" text="BounceInUp/BounceOutUp"]
[ptext layer="1" x=" 21" y="235" size="18" color="0xffcccc" text="BounceInLeft/BounceOutLeft"]
[ptext layer="1" x="501" y="235" size="18" color="0xffcccc" text="BounceInRight/BounceOutRight"]

[ptext layer="1" x=" 21" y="310" size="18" color="0xffcccc" text="RotateIn/RotateOut"]
[ptext layer="1" x="501" y="310" size="18" color="0xffcccc" text="RotateInDownLeft/RotateOutDownLeft"]
[ptext layer="1" x=" 21" y="385" size="18" color="0xffcccc" text="RotateInDownRight/RotateOutDownRight"]
[ptext layer="1" x="501" y="385" size="18" color="0xffcccc" text="RotateInUpLeft/RotateOutUpLeft"]

[ptext layer="1" x=" 21" y="460" size="18" color="0xffcccc" text="RotateInUpRight/RotateOutUpRight"]
[ptext layer="1" x="501" y="460" size="18" color="0xffcccc" text="RollIn/RollOut"]
[ptext layer="1" x=" 21" y="535" size="18" color="0xffcccc" text="FadeIn/Hinge"]

[return]

; ------------------------------------------------------
*Sub_Effect_2_Mtext

[mtext layer="1" x=" 20" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInDownBig"  out_effect="fadeOutDownBig"  wait="false"]
[mtext layer="1" x="500" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInLeftBig"  out_effect="fadeOutLeftBig"  wait="false"]
[mtext layer="1" x=" 20" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeInRightBig" out_effect="fadeOutRightBig" wait="false"]
[mtext layer="1" x="500" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="bounceIn"       out_effect="bounceOut"      wait="false"]

[mtext layer="1" x=" 20" y="180" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="bounceInDown"  out_effect="bounceOutDown"  wait="false"]
[mtext layer="1" x="500" y="180" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="bounceInUp"    out_effect="bounceOutUp"    wait="false"]
[mtext layer="1" x=" 20" y="255" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="bounceInLeft"  out_effect="bounceOutLeft"  wait="false"]
[mtext layer="1" x="500" y="255" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="bounceInRight" out_effect="bounceOutRight" wait="false"]

[mtext layer="1" x=" 20" y="330" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="rotateIn"          out_effect="flipOutX"           wait="false"]
[mtext layer="1" x="500" y="330" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="rotateInDownLeft"  out_effect="rotateOutDownLeft"  wait="false"]
[mtext layer="1" x=" 20" y="405" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="rotateInDownRight" out_effect="rotateOutDownRight" wait="false"]
[mtext layer="1" x="500" y="405" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="rotateInUpLeft"    out_effect="rotateOutUpLeft"    wait="false"]

[mtext layer="1" x=" 20" y="480" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="rotateInUpRight" out_effect="rotateOutUpRight"  wait="false"]
[mtext layer="1" x="500" y="480" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="rollIn"          out_effect="rollOut"           wait="false"]
[mtext layer="1" x=" 20" y="555" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." in_effect="fadeIn"          out_effect="hinge"             wait="true "]

[return]






; ●
; ●Text order
; ●

; ------------------------------------------------------
*Sub_Sync_Ptext

[ptext layer="1" x=" 21" y=" 10" size="18" color="0xffcccc" text="Regular (from the left)"]
[ptext layer="1" x="501" y=" 10" size="18" color="0xffcccc" text="Simultaneously"]
[ptext layer="1" x=" 21" y=" 85" size="18" color="0xffcccc" text="Random"]
[ptext layer="1" x="501" y=" 85" size="18" color="0xffcccc" text="From the right"]

[return]

; ------------------------------------------------------
*Sub_Sync_Mtext

[mtext layer="1" x=" 20" y=" 30" size="36" text="Text object" wait="false"]
[mtext layer="1" x="500" y=" 30" size="36" text="Text object" wait="false" in_sync="true"    out_sync="true"]
[mtext layer="1" x=" 20" y="105" size="36" text="Text object" wait="false" in_shuffle="true" out_shuffle="true"]
[mtext layer="1" x="500" y="105" size="36" text="Text object" wait="true " in_reverse="true" out_reverse="true"]

[return]






; ●
; ●Delayed text effect (set in milliseconds)
; ●

; ------------------------------------------------------
*Sub_Delay_Ptext

[ptext layer="1" x=" 21" y=" 10" size="18" color="0xffcccc" text="0ms delay"]
[ptext layer="1" x="501" y=" 10" size="18" color="0xffcccc" text="50ms delay"]
[ptext layer="1" x=" 21" y=" 85" size="18" color="0xffcccc" text="100ms delay"]
[ptext layer="1" x="501" y=" 85" size="18" color="0xffcccc" text="200ms delay"]

[return]

; ------------------------------------------------------
*Sub_Delay_Mtext

[mtext layer="1" x=" 20" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" in_delay="  0" out_delay="  0"]
[mtext layer="1" x="500" y=" 30" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" in_delay=" 50" out_delay=" 50"]
[mtext layer="1" x=" 20" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="false" in_delay="100" out_delay="100"]
[mtext layer="1" x="500" y="105" size="36" text="This&ensp;is&ensp;a&ensp;text&ensp;object." wait="true " in_delay="200" out_delay="200"]

[return]






; ●
; ●Delaying text effects (set in multiple)
; ●

; ------------------------------------------------------
*Sub_Delay_Scale_Ptext

[ptext layer="1" x=" 21" y=" 10" size="18" color="0xffcccc" text="x0 delay"]
[ptext layer="1" x="501" y=" 10" size="18" color="0xffcccc" text="x1 delay"]
[ptext layer="1" x=" 21" y=" 85" size="18" color="0xffcccc" text="x2 delay"]
[ptext layer="1" x="501" y=" 85" size="18" color="0xffcccc" text="x3 delay"]

[return]

; ------------------------------------------------------
*Sub_Delay_Scale_Mtext

[mtext layer="1" x=" 20" y=" 30" size="36" text="Text effect object" wait="false" in_delay_scale="0" out_delay_scale="0"]
[mtext layer="1" x="500" y=" 30" size="36" text="Text effect object" wait="false" in_delay_scale="1" out_delay_scale="1"]
[mtext layer="1" x=" 20" y="105" size="36" text="Text effect object" wait="false" in_delay_scale="2" out_delay_scale="2"]
[mtext layer="1" x="500" y="105" size="36" text="Text effect object" wait="true " in_delay_scale="3" out_delay_scale="3"]

[return]