; [reset_all] Macro that resets all game screens and returns the game to the startup state. (See 「macro.ks」 for details.)
[reset_all]

[position width=920 height=211 top=371 left=20]
[position page=fore margint=45 marginl=10 marginr=20 marginb=10 vertical=false opacity="180" color="0x000000" ]
[bg storage="room.jpg" time=600]

[er]Character Object References[l]










[er]～Set the Characters～[l]
[er]［chara_new］… Define a character.[l]
[r ]［chara_face］… Register expressions for a defined character.[l]


[chara_new  name="akane"  jname="Akane" storage="chara/akane/normal.png"  ]
[chara_face name="akane"   face="angry"  storage="chara/akane/angry.png"   ]
[chara_face name="akane"   face="doki"   storage="chara/akane/doki.png"    ]
[chara_face name="akane"   face="happy"  storage="chara/akane/happy.png"   ]
[chara_face name="akane"   face="sad"    storage="chara/akane/sad.png"     ]

; [chara_new][chara_face]x4 Define Yamato.
[chara_new  name="yamato" jname="やまと" storage="chara/yamato/normal.png" ]
[chara_face name="yamato"  face="angry"  storage="chara/yamato/angry.png"  ]
[chara_face name="yamato"  face="happy"  storage="chara/yamato/happy.png"  ]
[chara_face name="yamato"  face="sad"    storage="chara/yamato/sad.png"    ]
[chara_face name="yamato"  face="tohoho" storage="chara/yamato/tohoho.png" ]

; [chara_new][chara_face]x2 Define the mascot character. The name will be initially set as 「???」.
[chara_new   name="tyrano" jname="???" storage="chara/mascot_1/normal.png" ]
[chara_face  name="tyrano"  face="happy"  storage="chara/mascot_1/happy.png"  ]
[chara_face  name="tyrano"  face="angry"  storage="chara/mascot_1/angry.png"  ]










[er]～Place a character～[l]
[er]［chara_show］… Place a character.[l]


[chara_show name="akane"]


[chara_hide name=akane time=0]
[er]Set the join duration.[l]


[chara_show name=akane time=3000]


[chara_hide name=akane time=0]
[er]Use wait=false to proceed without waiting for the animation to complete.[l]


[chara_show name=akane time=3000 wait=false]


[er]AAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH[l]
[chara_hide name=akane time=0]
[er]Set the position.[l]


[chara_show name=akane left=100 top=100]


[chara_hide name=akane time=0]
[er]You can set the width and height.[l]


[chara_show name=akane width=300]


[chara_hide name=akane time=0]
[er]Flip horizontally. (Retains the width/height settings.)[l]


[chara_show name=akane reflect=true]


[chara_hide name=akane time=0]
[er]Set the expression when the character joins. (Retains the width/height settings.)[l]


[chara_show name=akane face=happy]


[chara_hide name=akane time=0]
[er]Revert to default.[l]


[chara_show name=akane width=400 reflect=false]











[er]～Change the Expression～[l]
[er]［chara_mod］… Change the expression.[l]


[chara_mod name=akane face=doki]


[er]Use face=default to use the default expression.[l]


[chara_mod name=akane face=default]


[er]You can set the transition duration and whether or not to wait for the animation to end.[l]


[chara_mod name=akane face=happy time=2000 wait=false]


[er]AAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH[l]
[er]Use cross=false to disable crossfade. [l]


[chara_mod name=akane face=default cross=false]


[er]Use reflect=true to flip horizontally while changing expression.[l]


[chara_mod name=akane face=happy reflect=true]


[er]Revert to default.[l]


[chara_mod name=akane face=default reflect=false]










[er]～Change the Position～[l]
[er]［chara_move］… Change the position.[l]

[chara_move name=akane left=100]


[er]You can set the transition duration and whether or not to wait for the animation to complete.[l]


[chara_move name=akane left=500 time=3000 wait=false]


[er]AAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH[l]
[er]「left="-=200"」「left="+=100"」, etc. will move the character relative to the current position.[l]


[chara_move name=akane left="-=200"]
[chara_move name=akane left="+=100"]


[er]You can also change the size.[l]


[chara_move name=akane width=600]


[er]You can use animation for smooth transitions.[l]

[chara_move name=akane left=260 width=400 anim=true]


[er]You can animate the character image.[l]

[chara_move name=akane left=0   anim=true effect=linear]
[wait time=500]
[chara_move name=akane left=600 anim=true effect=easeOutBack]
[wait time=500]
[chara_move name=akane left=260   anim=true effect=easeOutBounce]












[er]～Show the Character Name Window～[l]
[er]［ptext name=XXX］… Place a text object.[l]
[r ]［chara_config ptext=XXX］… Set the text object that will be the character name window.[l]


[ptext         name="chara_name_area" layer="message0" color=0xFFFFFF size=26 x=30 y=381 bold="bold" edge="" shadow="" text="【Use&ensp;here&ensp;as&ensp;the&ensp;character&ensp;name&ensp;window】"]
[chara_config ptext="chara_name_area"]


[er]…… We placed a text object.[l]
[r ]［chara_ptext］… Enter a name in the text object.[l]


[chara_ptext name=akane]


[er]You can also change the expression while entering the name.[l]


[chara_ptext name=akane face=happy]


[er]Remove the name.[l]


[chara_ptext]


[er]The［chara_ptext］tag has special transition methods.[l]
[r]For example,［chara_ptext name=akane］has the same effect as writing #Akane.[l]


#Akane


[er]You can remove a name like this:[l]


#


[er]You can use this script to change the expression while entering a name:[l]


#Akane:default








[er]～Remove a Character～[l]
[er]［chara_hide］… Remove a specific character on a specific layer.[l]
[r ]［chara_hide_all］… Remove all characters on a specific layer.[l]


[chara_hide name=akane]









[er]～Managing Multiple Characters～[l]
[er]［chara_show］… If you place a character with auto-position enabled, characters on the same layer will be automatically repositioned.[l]


[chara_show name=akane]
[chara_show name=yamato]


[er] You can have new characters join on the left.[l]
[er] By the way, characters that are placed without auto-position enabled will not be adjusted.[l]


[chara_show name=tyrano left=10 top=10]


[er]［chara_hide］… You can set enable or disable repositioning when a character leaves.[l]
[r ]Here is an example of a character exiting with repositioning disabled.[l]


[chara_hide name=tyrano pos_mode=false]
[chara_hide name=yamato pos_mode=false]
[chara_hide name=akane pos_mode=false]


[chara_show name=akane]
[chara_show name=yamato]
[chara_show name=tyrano]
[er]Here is an example of a character exiting with repositioning enabled.[l]


[chara_hide name=tyrano]
[chara_hide name=yamato]
[chara_hide name=akane]


[er]Set wait=false to place or remove multiple characters simultaneously.[l]


[chara_show name=akane wait=false]
[chara_show name=yamato wait=false]
[chara_show name=tyrano]


[er]In these cases, repositioning animation is carried out in serial. Do avoid this, do the following.[l]
[chara_hide_all time=0]
[er]Example of disabling repositioning by directly setting the positions.[l]


[chara_show name=akane wait=false left=520 top=40]
[chara_show name=yamato wait=false left=200 top=0]
[chara_show name=tyrano           left=90 top=200]









[chara_hide_all time=1000]
[er]～Adjusting Character Configuration～[l]
[er]［chara_config］… Adjust character configurations.[l]

[er]You can disable the auto-positioning function. (It is enabled by default.)[l]


[chara_config pos_mode=false]


[chara_show name=yamato]
[chara_show name=akane]
[chara_hide_all]
[r ]…… Revert to enabled.[l]


[chara_config pos_mode=true]


[er]Set the auto- duration.[l]


[chara_config pos_change_time=100]


[chara_show name=yamato]
[chara_show name=akane]
[chara_show name=tyrano]
[er]You can set the default value of the duration over which expressions change （using the ［chara_mod］ tag）.[l]


[chara_config time=1200]


[chara_mod name=akane face=happy]


[chara_config time=0]


[chara_mod name=akane face=sad]
[er]You can have TyranoScript remember the character expression when the character exits and reflectit the next time the character joins a scene. (This is disabled by default.)[l]


[chara_config memory=true]


[chara_hide name=akane]
[chara_show name=akane]
[r ]…… Revert to disabled.[l]


[chara_config memory=false]


[chara_hide name=akane]
[chara_show name=akane]
[er]You can enable auto highlighting of the currently speaking character. (This is disabled by default.)[l]
[er]An example of highlighting using different levels of 'blur'. The degree of bluriness can be adjusted.[l]


[chara_config talk_focus=blur blur_value=2]


#Akane
[er]Akane is speaking.[l]
#Yamato
[er]Yamato is speaking.[l]
#Tyrano
[er]The mascot is rawring.[l]
#
[er]An example of highlighting using different levels of 'brightness'. The degree of brightness can be adjusted.[l]


[chara_config talk_focus=brightness brightness_value=60]


#Akane
[er]Akane is speaking.[l]
#Yamato
[er]Yamato is speaking.[l]
#Tyrano
[er]The mascot is rawring.[l]
#
[er]In addition, you can automatically highlight using animation.[l]
[r ]An example of hightlighting using 'float' animation. The float distance and time can be adjusted.[l]


[chara_config talk_anim=up talk_anim_value=20 talk_anim_time=200]


#Akane
[er]Akane is speaking.[l]
#Yamato
[er]Yamato is speaking.[l]
#Tyrano
[er]The mascot is speaking.[l]
#
[er]An example of hightlighting using 'sink' animation. The sink distance and time can be adjusted.[l]


[chara_config talk_anim=down]


#Akane
[er]Akane is speaking.[l]
#Yamato
[er]Yamato is speaking.[l]
#Tyrano
[er]The mascot is speaking.[l]
#
[er]Disable auto-highlighting.[l]


[chara_config talk_anim=none talk_focus=none]


#Akane
[er]Akane is speaking.[l]
#Yamato
[er]Yamato is speaking.[l]
#Tyrano
[er]The mascot is speaking.[l]
#
[chara_hide name=tyrano pos_mode=false]
[chara_hide name=yamato]
#Akane:happy
[er]This completes the resources.[l]


[chara_config pos_change_time=600 time=600]
[jump storage="select.ks"]