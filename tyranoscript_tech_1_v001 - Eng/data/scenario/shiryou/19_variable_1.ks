; [reset_all] Macro that resets all game screens and returns the game to the startup state. (See「macro.ks」for details.)
[reset_all]

Variables Reference.[p]





; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
～Store and Retrieve Various Values in Variables～[p]
; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

●Use「values」,「text strings」,「true/false」.[r][r]

[iscript]
f.xxx = 100
f.aaa = 'Akane'
f.bbb = true
f.ccc = false
[endscript]

... Added them.[p]

●Display the variable contents in a message.[r][r]
…… The contents of f.xxx, f.aaa, f.bbb, and f.ccc are:[r]

[emb exp=f.xxx]、
[emb exp=f.aaa]、
[emb exp=f.bbb]、
[emb exp=f.ccc][p]






; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
～Let's Do Some  Math～[p]
; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

The current content of f.xxx is: [emb exp=f.xxx].[p]
●Add 10.[r][r]

[iscript]
f.xxx = f.xxx + 10
[endscript]

The current content of f.xxx is: [emb exp=f.xxx].[p]
●Add 10 using another method.[r][r]

[iscript]
f.xxx += 10
[endscript]

The current content of f.xxx is: [emb exp=f.xxx].[p]
●Double it.[r][r]

[iscript]
f.xxx *= 2
[endscript]

The current content of f.xxx is: [emb exp=f.xxx].[p]
●Calculate 2×11÷7 and store it in the variable.[r][r]

[iscript]
f.xxx = 2 * 11 / 7
[endscript]

Now the content of f.xxx is: [emb exp=f.xxx].[p]
●Calcuate the remainder of 5＋5 divided by 3 and store it in the variable.[r][r]

[iscript]
f.xxx = (5 + 5) % 3
[endscript]

Now the content of f.xxx is: [emb exp=f.xxx].[p]
●Store「100」in the variable「f.yyy」, [r]add「f.yyy」and「f.xxx」, [r]and store the result in the variable「f.zzz」.[r][r]

[iscript]
f.yyy = 100
f.zzz = f.xxx + f.yyy
[endscript]

Now the content of f.zzz is: [emb exp=f.zzz].[p]






; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
～Various Operations～[p]
; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

●Add the values「1」and「1」.[r][r]

[iscript]
f.xxx = 1 + 1
[endscript]

The answer is [emb exp=f.xxx].[p]
●Add the text strings「1」and「1」.[r][r]

[iscript]
f.xxx = '1' + '1'
[endscript]

The result is [emb exp=f.xxx].[p]
●Add the text string「1」and the value「1」.[r][r]

[iscript]
f.xxx = '1' + 1
[endscript]

The result is [emb exp=f.xxx].[l][r]
In other words, when adding text strings and values, the text string rule takes priority.[p]






; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
～Using Variables as Tag Parameters～[p]
; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

●The current content of f.zzz is: [emb exp=f.zzz]. Let's use this to set the width and height parameters in an ［image］ tag.[r][r]

[layopt layer=0 visible=true]
[image width=&f.zzz        height=&f.zzz          storage=color/red.png left=0 top=0 layer=0]

An image with width and height of [emb exp=f.zzz] picels has appeared.[p]
●This time, let's not just retrieve it as a parameter, but also do some math. Set f.zzz times 3 as the width and height.[r][r]

[image width="& f.zzz * 3" height="& f.zzz * 3"   storage=color/red.png left=0 top=0 layer=0]

An imaeg with wdiget and height of [emb exp=f.zzz*3] pixels has appeared.[p]

[freeimage layer=0]





; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
～Comparing Variables～[p]
; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

●Compare variables and find the true/false result.[r][r]

[iscript]
f.x = 100
f.y = 'Akane'
f.z = false

f.a = (f.x >  100)
f.b = (f.x <= 100)
f.c = (f.y == 'Akane')
f.d = (f.y != 'Yamato' && f.z)
f.e = (f.y != 'Yamato' || f.z)
[endscript]

Is f.x greater than 100? [emb exp=f.a].[r]
Is f.x lower than 100? [emb exp=f.b].[r]
Is f.y equal to「Akane」? [emb exp=f.c].[p]
Is f.y not「Yamato」AND is f.z true? [emb exp=f.d].[r]
Is f.y not「Yamato」OR is f.z true? [emb exp=f.e].[p]

●Compare variables and use the result to branch the story. （Using the ［jump］ tag and cond parameter.）[r][r]

[jump target=*Happy  cond="f.x >= 100 && f.z"]
[jump target=*Good   cond="f.x >= 100"       ]
[jump target=*Normal cond="f.x >=   0"       ]
[jump target=*Bad                            ]

*Happy

Congratulations! You got the happy ending![p][jump target=*Next]

*Good

This is the good ending.[p][jump target=*Next]

*Normal

This is the normal ending.[p][jump target=*Next]

*Bad

Hm... this is the bad ending.[p][jump target=*Next]

*Next

●Compare variables and use the result to branch the story. （Using the ［if］ tag.）[r][r]

[if    exp="f.x >= 100 && f.z"]…… Congratulations! You got the happy ending![p]
[elsif exp="f.x >=  50"       ]…… This is the good ending.[p]
[elsif exp="f.x >=   0"       ]…… This is the normal ending.[p]
[else                         ]…… Hm... this is the bad ending.[p]
[endif]






[jump storage="select.ks"]