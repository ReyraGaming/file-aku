; [reset_all] Macro that resets all game screens and returns the game to the startup state. (See「macro.ks」for details.)
[reset_all]

Subroutine and Macro Reference.[p]






; ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ===

～Calling a Subroutine～[p]


[call target=*Sub_1]
[call target=*Sub_1]
[call target=*Sub_1]
[er]
[jump target=*Next]


*Sub_1
This is the subroutine「*Sub_1」.[l][r]
[return]


*Next






; ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ===

～Using Macros～[p]


[macro name=my_p]
  [l]
  [er]
[endmacro]


Let's use the macro「my_p」. [my_p]






; ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ===

～Passing Parameters to Macros～[my_p]


[macro name=strong]
  [font edge=%iro]
[endmacro]


[strong iro=0x999900]Pass the parameter「iro=0x999900」to the macro「strong」.[my_p]






; ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ===

～Pass a Parameter to a Macro and Apply the Default Value～[my_p]


[macro name=strong]
  [font edge=%iro|0x0000FF]
[endmacro]


[strong iro=0x999900] Pass the parameter「iro=0x999900」to the macro「strong」.[l][r]
[strong             ] This is what happens when no parameter is passed to the「strong」macro.[my_p]






; ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ===

～Pass All Parameters in a Macro to the Internal Tag～[my_p]


[macro name=strong]
  [font * ]
[endmacro]


[strong size=50 bold=bold edge=0x999900] Pass all parameters in the Macro「strong」to the 「font」tag in the macro..[my_p]






; ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ===

～Pass all parameters in a Macro to an internal tag and partially overwrite～[my_p]


[macro name=strong]
  [font * color=0xFFFFFF edge=0x999900]
[endmacro]


[strong size=25 color=0x000000 edge=0x0000FF] Pass all parameters in the Macro「strong」to the 「font」tag in the macro., but overwrite the「color」and「edge」parametrs.[my_p]









[jump storage="select.ks"]