
; == room.ks ===============================================

; Small room.

; ==========================================================



[eval exp="f.current_scenario = TYRANO.kag.stat.current_scenario"]
[sb]
[scm]
[s]



;-------------------------------------------------------
*Sub_Set_Background

[cm]
[freeimage layer=0 page=back]
[image layer=0 page=back x=0 y=0 storage=escape/room.png]
[image layer=0 page=back x=0 y=0 storage=escape/room_bomb.png   name=layerbomb  cond="f.develops[f.current_scenario] == 0 || f.develops[f.current_scenario] == 1"]
[image layer=0 page=back x=0 y=0 storage=escape/room_black.png  name=layerblack cond="f.develops[f.current_scenario] >= 3                          "]
[image layer=0 page=back x=0 y=0 storage=escape/room_box.png    name=layerbox   cond="f.develops[f.current_scenario] == 1 || f.develops[f.current_scenario] == 2"]
[trans layer=0 time=400 method=puffIn]
[wt]
[return]

;-------------------------------------------------------
*Sub_Set_Clickable_Map

[mc 1 Cavity x=278 y=548 width=179 height=81  target=*Pos_1 cond="f.develops[f.current_scenario] == 0                          "]
[mc 2 Hand&ensp;grenade x=327 y=6   width=116 height=117 target=*Pos_2 cond="f.develops[f.current_scenario] == 0 || f.develops[f.current_scenario] == 1"]
[mc 3 Wooden&ensp;box x=277 y=434 width=180 height=192 target=*Pos_3 cond="f.develops[f.current_scenario] == 1 || f.develops[f.current_scenario] == 2"]
[mc 4 Sign&ensp;of&ensp;an&ensp;explosion x=191 y=533 width=360 height=101 target=*Pos_4 cond="f.develops[f.current_scenario] >= 3                          "]
[button storage=escape/left.ks graphic=escape/button_down.png x=30  y=550]
[return]

;-------------------------------------------------------
*Pos_1
; Progress is 0
[if exp="f.develops[f.current_scenario] == 0"]
  ; Box is not selected
  [if exp="f.select_item != 'box'"]
    [sm]
    There seems to be a cavity in the floor.[l][er]
    It looks like a square object would fit...[l][er]
    [hm]
  ; The box is not seelcted
  [else]
    [use item=box]
    [image layer=0 x=0 y=0 storage=escape/room_box.png name=layerbox]
    [eval exp="f.develops[f.current_scenario] = 1"]
  [endif]
; Progress is 1
[elsif exp="f.develops[f.current_scenario] >= 1"]
  [ERROR!!]
[endif]
[scm][s]

;-------------------------------------------------------
*Pos_2
; Progress is 0
[if exp="f.develops[f.current_scenario] == 0"]
  [sm]
  A hand grenade is suspended from the ceiling.[l][er]
  It is within reach.[l][er]
  [hm]
; Progress is 1
[elsif exp="f.develops[f.current_scenario] == 1"]
  [get item=bomb]
  [eval exp="f.develops[f.current_scenario] = 2"]
; Progress is 2
[elsif exp="f.develops[f.current_scenario] >= 2"]
  [sm]
  There was a hand grenade hanging from the ceiling, but there's nothing there now.[l][er]
  [hm]
[endif]
[scm][s]

;-------------------------------------------------------
*Pos_3
; Progress is 0
[if exp="f.develops[f.current_scenario] == 0"]
  [ERROR!!]
; Progress is 1~2
[elsif exp="f.develops[f.current_scenario] <= 2"]
  ; Hand grenade is not selected
  [if exp="f.select_item != 'bomb'"]
    [sm]
    The wooden crate that you used to reach the hand grenade.[l][er]
    It would take more than punching or kicking to break it.[l][er]
    [hm]
  ; Hand grenade is selected
  [else]
    [sm]
    You try to break the wooden crate with the hand grenade...[l][er]
    KABLOOEY!!![l][er]
    [mask time=500]
    [use item=bomb]
    [free name=layerbox layer=0]
    [image layer=0 x=0 y=0 storage=escape/room_black.png]
    [cursor storage=default]
    [eval exp="f.develops[f.current_scenario] = 3"]
    [mask_off time=500]
    The crate was blown to smithereens.[l][er]
    [hm]
   [endif]
; Progress is 3
[elsif exp="f.develops[f.current_scenario] >= 3"]
  [ERROR!!]
[endif]
[scm][s]

;-------------------------------------------------------
*Pos_4
; Progress is 2
[if exp="f.develops[f.current_scenario] <= 2"]
  [ERROR!!]
; Progress is 3
[elsif exp="f.develops[f.current_scenario] == 3"]
  [sm]
  That's where you set off the hand grenade.[l][er]
  There's something there...[l][er]
  You found a small key.[l][er]
  [get item=key]
  [eval exp="f.develops[f.current_scenario] = 4"]
  [hm]
; Progress is 4
[elsif exp="f.develops[f.current_scenario] >= 4"]
  [sm]
  There is nothing left.[l][er]
  [hm]
[endif]
[scm][s]

;-------------------------------------------------------
*Pos_5
[eval exp="f.chest_top_is_open = false"]
[anim name=chest_top left=960 time=0]
[scm][s]