
; == subroutine.ks =========================================

; Subroutines that call fixed buttons.

; ==========================================================



;-----------------------------------------------------------
*Sub_Empty

[return]



;-----------------------------------------------------------
*Sub_Select

; ※ PLEASE NOTE ※
; After jumping to this label, 
; based on the fixed button exp parameters,
; predefined values are stored in the temporary variables tf.top and tf.select.

; When an unselected item is clicked
[if exp="tf.select != f.select_item"]
  ; Change the cursor / Update the variables / Move the button highlight image(name=select)
  [cursor storage="& 'escape/cursor_' + tf.select + '.png' "]
  [eval exp="f.select_item = tf.select"]
  [anim name=select left=840 top=&tf.top opacity=255 time=0]
; When the presently selected item is clicked again
[else]
  ; Change the cursor / Update the variables / Move the button highlight image(name=select)
  [cursor storage=default]
  [eval exp="f.select_item = 'empty' "]
  [anim name=select opacity=0 time=0]
[endif]

[return]



;-----------------------------------------------------------
*Sub_Check_Box

[sm2]
It is a light wooden box.[l][er]
There is no opening on it.[l][er]
[hm2]

[return]



;-----------------------------------------------------------
*Sub_Check_Bomb

[sm2]
It's a hand grenade.[l][er]
It will explode if the pin is pulled out.[l][er]
[hm2]

[return]



;-----------------------------------------------------------
*Sub_Check_Key

[sm2]
It's a small key.[l][er]
[hm2]

[return]



;-----------------------------------------------------------
*Sub_Check_LPaper

[sm2]
It's a scrap of paper with '45' written on it.[l][er]
The right-hand side has been ripped off.[l][er]
[hm2]

[return]



;-----------------------------------------------------------
*Sub_Check_RPaper

[sm2]
It's a scrap of paper with '27' written on it.[l][er]
The left-hand side has been ripped off.[l][er]
[hm2]

[return]