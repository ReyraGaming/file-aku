
; == right.ks ==============================================

; The right-hand side of the room.

; ==========================================================



[eval exp="f.current_scenario = TYRANO.kag.stat.current_scenario"]
[sb]
[scm]
[s]


;-------------------------------------------------------
*Sub_Set_Background

[cm]
[freeimage layer=0 page=back]
[image layer=0 page=back x=0 y=0 storage=escape/right.png]
[trans layer=0 time=400 method=fadeInRight]
[wt]
[return]

;-------------------------------------------------------
*Sub_Set_Clickable_Map

[mc 1 Chest x=201 y=381 width=188 height=233 target=*Pos_1]
[mc 2 Safe   x=247 y=306 width=107 height=91  target=*Pos_2]
[button storage=escape/left.ks graphic=escape/button_left.png x=30  y=300]
[return]

;-------------------------------------------------------
*Pos_1
[jump storage=escape/chest.ks]

;-------------------------------------------------------
*Pos_2
; Progress is 0
[if exp="f.develops[f.current_scenario] == 0"]
  ; The key is unselected
  [if exp="f.select_item != 'key'"]
    [sm]
    There is a sturdy-looking safe.[l][er]
    It would take quite some force to dent it.[l][er]
    [hm]
  ; The key is selected
  [else]
    [use item=key]
    [sm]
    You used the key to open the safe.[l][er]
    ……[l][er]
    Inside is a piece of paper.[l][er]
    [hm]
    [get item=rpaper]
    [eval exp="f.develops[f.current_scenario] = 1"]
  [endif]
; Progress is 1
[elsif exp="f.develops[f.current_scenario] >= 1"]
  [sm]
  There is nothing left.[l][er]
  [hm]
[endif]
[scm][s]