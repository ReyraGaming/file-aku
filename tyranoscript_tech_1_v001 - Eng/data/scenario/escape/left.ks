
; == left.ks ===============================================

; Left-hand side of the room.

; ==========================================================

; Store the name of the current scenario file in teh variable f.current_scenario.
; (You can obtain the filename of the current scenario by using TYRANO.kag.stat.current_scenario.)
[eval exp="f.current_scenario = TYRANO.kag.stat.current_scenario"]
; Set the background and clickable map, then stop.
[sb]
[scm]
[s]

;-------------------------------------------------------
*Sub_Set_Background

[cm]
[freeimage layer=0 page=back]
; Show only if the progress of the box image is 0
[image layer=0 page=back x=0 y=0 storage=escape/left.png]
[image layer=0 page=back x=0 y=0 storage=escape/left_box.png name=layerbox cond="f.develops[f.current_scenario] == 0"]
[trans layer=0 time=400 method=fadeInLeft]
[wt]
[return]

;-------------------------------------------------------
*Sub_Set_Clickable_Map

; Show only if the progress of the clickable map for the box is 0
[mc 1 Trash&ensp;can x=222 y=495 width=75  height=87  target=*Pos_1]
[mc 2 Wooden&ensp;box x=467 y=466 width=126 height=136 target=*Pos_2 cond="f.develops[f.current_scenario] == 0"]
[mc 3 Wall     x=364 y=345 width=53  height=32  target=*Pos_3]
[mc 4 Door   x=55  y=255 width=114 height=381 target=*Pos_4]
[button storage=escape/right.ks graphic=escape/button_right.png x=700  y=300]
[return]

;-------------------------------------------------------
*Pos_1
[sm]
It's a trash can.[l][er]
It's empty.[l][er]
[hm]
[scm][s]

;-------------------------------------------------------
*Pos_2
; Progress is 0
[if exp="f.develops[f.current_scenario] == 0"]
  [sm]
  There is a wooden box.[l][er]
  It has no lid.[l][er]
  It looks light and easy to carry.[l][er]
  [hm]
  ; You obtained a box.
  [get item=box]
  ; Update progress to 1
  [eval exp="f.develops[f.current_scenario] = 1"]
; Progress 1
[elsif exp="f.develops[f.current_scenario] >= 1"]
  ; The clickable map cannot be pasted when progress =1,
  ; This label(*Pos_2) cannot be read, so an error message will be displayed. (This helps with debugging.)
  [ERROR!!]
[endif]
[scm][s]

;-------------------------------------------------------
*Pos_3
[jump storage=escape/wall.ks]

;-------------------------------------------------------
*Pos_4
[jump storage=escape/room.ks]