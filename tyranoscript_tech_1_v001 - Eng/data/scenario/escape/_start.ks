
[reset_all]
[call target=*Sub_Opening]
[call target=*Sub_Setting]
[jump storage="escape/left.ks"]
[s]



; Show the opening
; ------------------------------------------------------------------------------
*Sub_Opening

[layopt layer=message visible=false]
[layopt layer=1       visible=true ]
[mtext  layer=1 x=0 y=260 text=Created&ensp;by&ensp;STRIKEWORKS width=960 align=center size=40 color=0xffffff in_effect=bounce out_sync=true time=1000]
; 「&emsp;」 in the text parameters adds a double byte space.

[return]



; Configure various settings
; ------------------------------------------------------------------------------
*Sub_Setting

[call target=*Sub_Position]
[call target=*Sub_Variable]
[call target=*Sub_Macro   ]
[call target=*Sub_Chara   ]
[call target=*Sub_Button  ]

[return]



; Message window settings
; ------------------------------------------------------------------------------
*Sub_Position

[position layer="message0" frame="none" color="0x000000" opacity="200"]
[position layer="message0" width="750" height="220" left="25" top="395"]
[position layer="message0" margint="55" marginr="25" marginb="25" marginl="25"]

[position layer="message1" frame="none" color="0x000000" opacity="200"]
[position layer="message1" width="750" height="140" left="25" top="25"]
[position layer="message1" margint="25" marginr="25" marginb="25" marginl="25"]

[ptext name="chara_name_area"  layer="message0" zindex="102" x="60" y="410" text="" color="0xffffff" size="40"  edge="0x000000"]
[chara_config ptext="chara_name_area"]

[return]



; Define variables
; ------------------------------------------------------------------------------
*Sub_Variable

/*
       TABLE OF ESCAPE ROOM VARIABLES

------------------------------------------------------------------------------
Variable                    Type     Explanation
--------------------------  --------  ----------------------------------------
f.select_item               Text string     Currently selected item name
f.current_scenario          Text string     Current scenario filename
f.is_open_chest_top         True/False      Was the upper draw opened?
f.is_open_chest_bottom      True/False      Was the lower draw opened?
f.develops                  {}              Tracks in-room progress
f.dials                     {}              Tracks the value of each dial
f.items                     {}              Tracks the number of carried objects
 f.develops['...']          Value           Progress in room ['...']に
 f.dials['...']             Value           The value of dial ['...']
 f.items['...']             Value           The number of ['...'] items carried
------------------------------------------------------------------------------
*/

[iscript]
f.select_item = 'empty'
f.current_scenario  = 'escape/_start.ks'
f.is_open_chest_top    = false
f.is_open_chest_bottom = false
f.develops = {}
 f.develops['escape/left.ks']  = 0
 f.develops['escape/right.ks'] = 0
 f.develops['escape/wall.ks']  = 0
 f.develops['escape/room.ks']  = 0
 f.develops['escape/chest.ks'] = 0
f.dials = {}
 f.dials['1'] = 0
 f.dials['2'] = 0
 f.dials['3'] = 0
 f.dials['4'] = 0
f.items = {}
 f.items['box']    = 0
 f.items['bomb']   = 0
 f.items['key']    = 0
 f.items['lpaper'] = 0
 f.items['rpaper'] = 0
[endscript]

[return]



; Define macros
; ------------------------------------------------------------------------------
*Sub_Macro

/*
       TABLE OF ESCAPE ROOM VARIABLES

--------------------------------------------------------------------------
Macro name & meaning        Explanation of Contents
--------------------------  ----------------------------------------------
mc  (my clickable)          A clickable map with set design

sm  (show message)          Show message layer 0
hm  (hide message)          Hide message layer 0
sm2 (show message 2)        Show message layer 1
hm2 (hide message 2)        Hide message layer 1

sv  (set variable)          Set variable (just calls a subroutine as the contents of the macro)
scm (set clickable map)     Set the clickable map (as above)
sb  (set background)        Set the background (as above)

use                         Use an item
get                         Get an item

ERROR!!                     Dispaly an error message
--------------------------------------------------------------------------
*/

[macro name=mc]
  [clickable * opacity=0 mouseopacity=40 border=1px:dashed:black color=pink]
[endmacro]

[macro name=sm]
  [cm]
  [layopt layer=message0 visible=true]
[endmacro]

[macro name=hm]
  [layopt layer=message0 visible=false]
[endmacro]

[macro name=sm2]
  ; If something is on the free layer, hide the free layer.
  ; This is necessary to show a message on message layer 1 (the item explanation layer) while maintaining information that is on the clickable map, etc.
  [eval cond="$('.layer_free').html() != ''" exp="$('.layer_free').hide(0)"]
  [layopt layer=message1 visible=true]
  [current layer=message1]
[endmacro]

[macro name=hm2]
  [layopt layer=message1 visible=false]
  [current layer=message0]
  ; If something is on the free layer, show the free layer. 
  [eval cond="$('.layer_free').html() != ''" exp="$('.layer_free').show(0)"]
[endmacro]

; [sv] Set variablea .
[macro name=sv ]
  ; Call the *Sub_Set_Variable label. storage is referenced from the variable.
  [call storage=&f.current_scenario target=*Sub_Set_Variable]
[endmacro]

; [sb] Set a background.
[macro name=sb ]
  [call storage=&f.current_scenario target=*Sub_Set_Background]
[endmacro]

; [scm] Set a clickable map.
[macro name=scm]
  [call storage=&f.current_scenario target=*Sub_Set_Clickable_Map]
[endmacro]

; [use] Use the selected item.
[macro name=use]
  ; Update a variable
  [eval exp="f.select_item = 'empty'"]
  ; Move the item buttons / background highlighting button off screen
  [anim name="&'fix'+mp.item" left="+=1000" time=0]
  [anim name="select"         left="+=1000" time=0]
  ; Apply the default cursor
  [cursor storage=default]
[endmacro]

; [get] Get an item.
[macro name=get]
  ; Release an item image from the screen. Names such as 「layerbomb」 and 「fixbomb」 are passed to the name parameter.
  [free name="&'layer'+mp.item" layer=0]
  [anim name="&'fix'+mp.item" left="-=1000" time=0]
  ; Update the variable
  [eval exp="f.items[mp.item] = 1"]
[endmacro]

[macro name=ERROR!!]
  [iscript]
  alert('【ERROR】 You're investigating somewhere that shouldn't be possible.')
  [endscript]
[endmacro]

[return]



; Define a Character
; ------------------------------------------------------------------------------
*Sub_Chara

[chara_new  name="master"  jname="Masked Man" storage="escape/c_normal.png"]
[chara_face name="master" face="ase"        storage="escape/c_ase.png"]
[chara_face name="master" face="happy"      storage="escape/c_happy.png"]



; Place a button
; ------------------------------------------------------------------------------
*Sub_Button

; Place the button background on foreground layer.
[image layer=1 x=800 y=0   storage=escape/button_bg.png]
[image layer=1 x=840 y=640 storage=escape/button_select.png name=select]

; Show the Use Item buttons. All buttons are fixed. Buttons are placed offscreen, then moved onscreen as items are obtained.
[button fix=true name=fixbox    graphic=escape/item_box.png    x=1840 y=30  storage=escape/_subroutine.ks target=*Sub_Select auto_next=false exp="tf.select = 'box'   ; tf.top =  30"]
[button fix=true name=fixbomb   graphic=escape/item_bomb.png   x=1840 y=120 storage=escape/_subroutine.ks target=*Sub_Select auto_next=false exp="tf.select = 'bomb'  ; tf.top = 120"]
[button fix=true name=fixkey    graphic=escape/item_key.png    x=1840 y=210 storage=escape/_subroutine.ks target=*Sub_Select auto_next=false exp="tf.select = 'key'   ; tf.top = 210"]
[button fix=true name=fixlpaper graphic=escape/item_lpaper.png x=1840 y=300 storage=escape/_subroutine.ks target=*Sub_Select auto_next=false exp="tf.select = 'lpaper'; tf.top = 300"]
[button fix=true name=fixrpaper graphic=escape/item_rpaper.png x=1840 y=390 storage=escape/_subroutine.ks target=*Sub_Select auto_next=false exp="tf.select = 'rpaper'; tf.top = 390"]

; Shoe the Item Explanation buttons. All buttons are fixed. Buttons are placed offscreen, then moved onscreen as items are obtained.
[button fix=true name=fixbox    graphic=escape/button_question.png x=1915 y=30  storage=escape/_subroutine.ks target=*Sub_Check_Box     auto_next=false]
[button fix=true name=fixbomb   graphic=escape/button_question.png x=1915 y=120 storage=escape/_subroutine.ks target=*Sub_Check_Bomb    auto_next=false]
[button fix=true name=fixkey    graphic=escape/button_question.png x=1915 y=210 storage=escape/_subroutine.ks target=*Sub_Check_Key     auto_next=false]
[button fix=true name=fixlpaper graphic=escape/button_question.png x=1915 y=300 storage=escape/_subroutine.ks target=*Sub_Check_LPaper  auto_next=false]
[button fix=true name=fixrpaper graphic=escape/button_question.png x=1915 y=390 storage=escape/_subroutine.ks target=*Sub_Check_RPaper  auto_next=false]

[return]
