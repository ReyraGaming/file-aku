
; == _end.ks ===============================================

; Lefthand side of the room

; ==========================================================



[eval exp="f.current_scenario = TYRANO.kag.stat.current_scenario"]
[sb]
[scm]
[s]

;-------------------------------------------------------
*Sub_Set_Background

[cm]
[freeimage layer=0 page=back]
[image layer=0 page=back x=0 y=0 storage=escape/left.png]
[image layer=0 page=back x=0 y=0 storage=escape/left_box.png name=layerbox cond="f.develops['escape/left.ks'] == 0"]
[chara_show    page=back name=master left=170 time=0]
[trans layer=0 time=400 method=fadeIn]
[wt]
[return]

;-------------------------------------------------------
*Sub_Set_Clickable_Map

[mc 1 Door           x=55  y=255 width=114 height=381 target=*Pos_1]
[mc 2 Game Master x=189 y=6   width=454 height=631 target=*Pos_2]
[return]

;-------------------------------------------------------
*Pos_1
[sm]
[chara_mod name=master face=ase]
#Master
Hey, hey! Don't ignore me!![l][er]
#
[hm]
[scm][s]

;-------------------------------------------------------
*Pos_2
; Bomb is not selected
[if exp="f.select_item != 'bomb'"]
  [chara_mod name=master face=happy]
  [sm]
  #Master
  Congratulations! You cleared the game.[l][er]
  You are every bit as smart as I thought you were.[l][er]
  You are free to leave.[l][er]
  #
  NORMAL ENDING[l][er]
  [hm]
  [jump target=*End]
; Bomb is selected
[else]
  [chara_mod name=master face=happy]
  [sm]
  #Master
  Congratulations. You cleared the game.[l][er]
  [chara_mod name=master face=ase]
  !!?? Wh-why are you carrying that???!!![l][er]
  You should have used that already!![l][er]
  H-hang on! WAIT!!![l][er]
  [use item=bomb]
  #
  You removed the pin from the hand grenade.[l][er]
  [freeimage layer=0 time=1000]
  #Master
  UWAHHHHHHH!!!![l][er]
  #
  ANOTHER ENDING[l][er]
  [hm]
  [jump target=*End]
[endif]
[scm][s]

;-------------------------------------------------------
*End
[mask time=500]
[wait time=500]
[destroy]
[mask_off time=0]
[mtext  layer=0 x=0 y=260 text=Thank&ensp;you&ensp;for&ensp;playing! width=960 align=center size=60 color=white in_effect=bounce]
; 「&ensp;」 in the text parameters adds a single byte space.
[wait time=500]
[jump storage=select.ks]