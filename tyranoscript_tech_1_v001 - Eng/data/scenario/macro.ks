
; == macro.ks ==============================================

; This scenario file simply defines macros and returns.
; It is called as a subroutine from first.ks.
; The final [return] tag is required.
;
;
; To find out more about macros, visit the below (Japanese language) page:
; http://tyrano.jp/usage/tech/macro
;
;
; ＝List of defined macros＝
; 
; ├Change expressions
; │├Akane
; ││├[akn/def]
; ││├[akn/hap]
; ││├[akn/sad]
; ││├[akn/dok]
; ││├[akn/ang]
; │├Yamato
; │　├[ymt/def]
; │　├[ymt/hap]
; │　├[ymt/sad]
; │　├[ymt/thh]
; │　├[ymt/ang]
; │
; ├Wipe / rebuild the screen
; │├[destroy]
; │├[reset_all]
; │├[set_message_window]
; │├[set_default_view]
; │
; ├Display the list of contents
; 　├[index_reset]
; 　└[list_item]
;
; ==========================================================




; ----------------------------------------------------------
; Macros to change expressions2
; ----------------------------------------------------------

; ----------------------------------------------------------
[macro name="akn/def"]
[chara_ptext name="akane"]
[chara_mod   name="akane" face="default"]
[endmacro]

; ----------------------------------------------------------
[macro name="akn/hap"]
[chara_ptext name="akane"]
[chara_mod   name="akane" face="happy"]
[endmacro]

; ----------------------------------------------------------
[macro name="akn/sad"]
[chara_ptext name="akane"]
[chara_mod   name="akane" face="sad"]
[endmacro]

; ----------------------------------------------------------
[macro name="akn/dok"]
[chara_ptext name="akane"]
[chara_mod   name="akane" face="doki"]
[endmacro]

; ----------------------------------------------------------
[macro name="akn/ang"]
[chara_ptext name="akane"]
[chara_mod   name="akane" face="angry"]
[endmacro]

; ----------------------------------------------------------
[macro name="ymt/def"]
[chara_ptext name="yamato"]
[chara_mod   name="yamato" face="default"]
[endmacro]

; ----------------------------------------------------------
[macro name="ymt/hap"]
[chara_ptext name="yamato"]
[chara_mod   name="yamato" face="happy"]
[endmacro]

; ----------------------------------------------------------
[macro name="ymt/sad"]
[chara_ptext name="yamato"]
[chara_mod   name="yamato" face="sad"]
[endmacro]

; ----------------------------------------------------------
[macro name="ymt/thh"]
[chara_ptext name="yamato"]
[chara_mod   name="yamato" face="tohoho"]
[endmacro]

; ----------------------------------------------------------
[macro name="ymt/ang"]
[chara_ptext name="yamato"]
[chara_mod   name="yamato" face="angry"]
[endmacro]








; ----------------------------------------------------------
; Macro to wipe / rebuild the screen
; ----------------------------------------------------------



; ----------------------------------------------------------
; [destroy]
; Wipes out images, text, and butons in all layers and
; clears the game screen.
[macro name="destroy"]
; Hide the buttons
[hidemenubutton]
; Delete messages and release the free layers
; (Free layers are layers used to insert buttons, HTML, etc.)
[cm]
; Release fix layers
[clearfix]
; Release combined layers
[free_layermode time="0"]
; Release filters
[free_filter]
; Release layers
[freeimage layer="base"]
[freeimage layer="0"]
[freeimage layer="1"]
[freeimage layer="2"]
[freeimage layer="base" page="back"]
[freeimage layer="0"    page="back"]
[freeimage layer="1"    page="back"]
[freeimage layer="2"    page="back"]
; Hide the message window
[layopt layer="message0" visible="false"]
[layopt layer="message1" visible="false"]
[endmacro]

[macro name="reset_all"]
[destroy]
[showmenubutton]
[position width="928" height="608" left="16" top="16" color="0x000000" opacity="128" margint="8" marginr="8" marginb="8" marginl="8" frame="none"]
[deffont size="24" face="Meiryo"]
[resetfont]
[free name="chara_name_area" layer="message0"]
[layopt layer="base" visible="true"]
[layopt layer="0" visible="false"]
[layopt layer="1" visible="false"]
[layopt layer="2" visible="false"]
[layopt layer="message0" visible="true"]
[layopt layer="message1" visible="false"]
[endmacro]

; ----------------------------------------------------------
; [set_default_view]
; After masking and wiping the screen,
; displays the classroom background, Akane's portrait, and the message window,
; and disable the mask.
; Set chara="off" to hide a character.
[macro name="set_default_view"]

; [mask]～[mask_off]　Wipe and rebuild the screen while the screen is masked.
[mask color="white" time="400"]

; [destroy] Standalone macro. Clears all objects and effects from the screen.
[destroy]

; [set_message_window] Standalone macro. Resets the message window.
[set_message_window]

; [layopt] Show the messages window.
[layopt layer="message0" visible="true"]

; [bg][chara_show][wait] Pause after displaying the background and character. 
[bg         time="  0" storage="room.jpg"]
[chara_show time="  0" name="akane" face="default"]
[wait       time="100"]

[mask_off time="400"]


[endmacro]



; ----------------------------------------------------------
; [set_message_window]
[macro name="set_message_window"]

; [position]
; Adjust the message window position, image, inner margin, etc. 
[position left="0" top="440" width="960" height="200" frame="window0/_frame.png"]
[position margint="50" marginl="25" marginr="25" marginb="10"]

; [free] If a text object with the name chara_name_area already exists on layer 0,
; delete it. (If no such object exists, this tag will have no action.)
[free  name="chara_name_area"  layer="message0"]

; [clearfix] Delete the fix button
[clearfix]

; ※ Addendum ※
; [free][clearfix] above are to reset.
; The tags were added in order to reset the message window setting and prevent doubling up objects
; in cases where  the macro is run and the ptext and fix buttons are already present.

; [ptext][chara_config]
; Insert a ptext object called chara_name_area into the message layer 0,
; and tell TyranoScript that this ptext is for the speaking character's box.
[ptext name="chara_name_area"  layer="message0" zindex="102" size="32" face="logo type Gothic, Meiryo, sans-serif" x="36" y="445" color="0xffffff" edge="0x000000"]
[chara_config ptext="chara_name_area"]

; [deffont][resetfont]
; Apply the default font settings.
; The size is 40px, font is logo type Gothic, font color is white, and stroke color is black.
[deffont size="30" face="logo type Gothic,Meiryo,sans-serif" color="0xffffff" edge="0x000000"]
[resetfont]

; ※ Addendum ※
; To enable logo type Gothic in TyranoScript,
; the font file is placed in data/others
; the required settings are applied in data/others/font.css,
; which is loaded by [loadcss].

; [button]
; Place the button roll.
[button name="role_button" role="skip"       graphic="window0/skip.png"   enterimg="window0/skip2.png"   x="& 0 * 80" y="615"]
[button name="role_button" role="auto"       graphic="window0/auto.png"   enterimg="window0/auto2.png"   x="& 1 * 80" y="615"]
[button name="role_button" role="save"       graphic="window0/save.png"   enterimg="window0/save2.png"   x="& 2 * 80" y="615"]
[button name="role_button" role="load"       graphic="window0/load.png"   enterimg="window0/load2.png"   x="& 3 * 80" y="615"]
[button name="role_button" role="quicksave"  graphic="window0/qsave.png"  enterimg="window0/qsave2.png"  x="& 4 * 80" y="615"]
[button name="role_button" role="quickload"  graphic="window0/qload.png"  enterimg="window0/qload2.png"  x="& 5 * 80" y="615"]
[button name="role_button" role="backlog"    graphic="window0/log.png"    enterimg="window0/log2.png"    x="& 6 * 80" y="615"]
[button name="role_button" role="window"     graphic="window0/close.png"  enterimg="window0/close2.png"  x="& 7 * 80" y="615"]
[button name="role_button" role="fullscreen" graphic="window0/screen.png" enterimg="window0/screen2.png" x="& 8 * 80" y="615"]
[button name="role_button" role="menu"       graphic="window0/menu.png"   enterimg="window0/menu2.png"   x="& 9 * 80" y="615"]
[button name="role_button" role="sleepgame"  graphic="window0/config.png" enterimg="window0/config2.png" x="&10 * 80" y="615" storage="config.ks"]
[button name="role_button" role="title"      graphic="window0/title.png"  enterimg="window0/title2.png"  x="&11 * 80" y="615"]

[endmacro]














; ----------------------------------------------------------
; Display the List of Contents
; ----------------------------------------------------------



; ----------------------------------------------------------
; [index_reset]
; This macro inserts the value 0 into the temporary variable tf.index.
; tf.index is used in the macro that displays the selected contents item. The number assigned represents an item in the list.
; tf.index == 0 when the uppermost item is selected.
; The count increase in the order 1, 2, 3... each time an items is displayed.
[macro name="index_reset"]
[eval exp="tf.index = 0"]
[endmacro]



; ----------------------------------------------------------
; [list_item text1="XXX" text2="XXX" storage="XXX" shiryou="XXX"]
; Displays the items in the list.
; The code determines the position using the selection value in tf.index.
[macro name="list_item"]

; [eval]
; The path of the image to be displayed is stored in temp variable tf.image.
; The current page number (1-4) is stored in f.current_page_index,
; so the result should be that tf.image contains a string 'color/col1.png' or 'color/col2.png', etc.
[eval exp="tf.image = 'color/col' + f.current_page_index + '.png'"]

; [if]～[endif]
; Branches depending on whether reference materials is available or not.
[if exp="mp.shiryou == 'yes'"]

; If the tutorial has reference material...

; [image]
; Displays a colored box as the background to the selected item.
[image layer="0" storage="& tf.image"      x="  0" y="& tf.index * 95 +  5 " width="730" height="90"]

; The '&' prefixing the values of the storage and other attributes is
; the signal to 'evaluate the following text as JavaScript'.
; In the case of the storage attribute, the "tf.image" that follows the '&' is evaluated as JavaScript, so
; the contents of the variable tf.image will be given to the storage attribute.
; NB: If '&' is omitted, "tf.image" is simply a string of characters.

; [ptext]
; Display the text.
[ptext layer="0" text="% text1"            x=" 20" y="& tf.index * 95 +  9 " bold="bold"   size="40"]
[ptext layer="0" text="% text2"            x=" 20" y="& tf.index * 95 + 56 "               size="22"]
; The '%' prefixing the text attribute signals to
; 'pass the value with the same attribute name as the one that was passed to the macro'.

; [clickable]
; Creates a clickable transparent button.
[clickable color="white" opacity="0"      x="  0" y="& tf.index * 95 +  5 " width="730" height="90" mouseopacity="50" storage="& 'kaisetsu/' + mp.storage" target="%target"]

; Do the same for the reference materials.
[image layer="0" storage="&tf.image"      x="735" y="& tf.index * 95 +  5 " width="250" height="90"]
[ptext layer="0" text="Reference"            x="771" y="& tf.index * 95 + 27 " bold="bold"   size="30"]
[clickable color="white" opacity="0"      x="735" y="& tf.index * 95 +  5 " width="250" height="90" mouseopacity="50" storage="& 'shiryou/' + mp.storage" target="%target"]

[else]

; If there are no reference materials, there is no need to provide a link.
;                                                                                  !!!
[image layer="0" storage="&tf.image"      x="  0" y="& tf.index * 95 +  5 " width="960" height="90"]
[ptext layer="0" text="%text1"            x=" 20" y="& tf.index * 95 +  9 " bold="bold"   size="40"]
[ptext layer="0" text="%text2"            x=" 20" y="& tf.index * 95 + 56 "               size="22"]
[clickable color="white" opacity="0"      x="  0" y="& tf.index * 95 +  5 " width="960" height="90" mouseopacity="50" storage="& 'kaisetsu/' + mp.storage" target="%target"]

[endif]

; [eval]
; When a selection is displayed, 
; increase the selection number by one.
[eval exp="tf.index = tf.index + 1"]

[endmacro]



[return]