
; == first.ks ==============================================

; This is the first scenario file that is loaded when the game is run.
; This file is used to load any plugins or set macros, etc. that need
; to be done prior to the game itself.
; When the initial processes are complete, move to the next (e.g. title) screen using a [jump] tag.
; NB: Do not rename this file.
;
; ==========================================================



; [title] Change the text displayed in the title bar.
[title name="TyranoScript | Technical Samples Part 1"]

; [call]x3 calls 3 subroutines used for settings.
[call target="*Sub_Layer"    ]
[call target="*Sub_Plugin"   ]
[call target="*Sub_Character"]

; [jump] to title.ks.
[jump storage="title.ks"]

[s]







;　~ Below are labels used for settings ~



; ----------------------------------------------------------
*Sub_Layer
; ----------------------------------------------------------

; This subroutine label shows or hides layers.
; It is called by a [call] tag.
; When it reaches [return], it returns to the [call] tag position.

; [hidemenubutton] hides the menu button.
[hidemenubutton]

; [layopt] hides message layer 0.
[layopt layer="message0" visible="false"]

; [layopt] shows layers 0, 1, and 2.
; NB: The default state is hidden.
[layopt layer="0" visible="true"]
[layopt layer="1" visible="true"]
[layopt layer="2" visible="true"]

; [return] returns to the [call] tag position.
[return]



; ----------------------------------------------------------
*Sub_Plugin
; ----------------------------------------------------------

; This subroutine label loads plugins, etc.

; [call] calls the subroutine.
[call storage="macro.ks"]

; [loadcss] loads the css file. This is for use with custom fonts.
[loadcss file="data/others/font/font.css"]
[loadcss file="data/others/glink/glink.css"]

; [return] returns to the [call] tag position.
[return]



; ----------------------------------------------------------
*Sub_Character
; ----------------------------------------------------------

; This subroutine label defines the characters.

; [chara_new][chara_face]x4 defines Akane.
[chara_new  name="akane"  jname="Akane" storage="chara/akane/normal.png"  ]
[chara_face name="akane"   face="angry"  storage="chara/akane/angry.png"   ]
[chara_face name="akane"   face="doki"   storage="chara/akane/doki.png"    ]
[chara_face name="akane"   face="happy"  storage="chara/akane/happy.png"   ]
[chara_face name="akane"   face="sad"    storage="chara/akane/sad.png"     ]

; [return] returns to the [call] tag position.
[return]