
; == select.ks =============================================

; This scenario file creates the title screen.

; This file is a little more technically advanced that most.
; Most of the content has nothing to do with creating visual novels,
; so there's really no need to go through this in detail.

; ==========================================================






; ----------------------------------------------------------
*Start
; ----------------------------------------------------------


; [layopt]
; Delete the message layer.
[layopt layer="message0" visible="false"]
[layopt layer="message1" visible="false"]
[layopt layer="0" visible="true"]
[layopt layer="1" visible="true"]
[layopt layer="2" visible="true"]

; [hidemenubutton] Hide the menu button.
[hidemenubutton]

; [deffont][resetfont] Apply the default font settings.
[deffont size="25" face="logo type gothic, meiryo,sans-serif" color="0xffffff" edge="0x000000"]
[resetfont]

; [eval]
; Define 2 game variables using [eval]
;
;     f.current_page_index … What is the current page number? (1,2,3...)
;     f.current_page_label … What is the label name of the current page? (text. '*Sub_Page1', '*Sub_Page2',...)
;
; Note that here cond="f.current_page_index == undefined"!
; This means, 'if nothing is defined in f.current_page_index, then run this tag'.
; If at any point, 1 is stored in f.current_page_index via this scenario,
; then don't run this operation again.
; By doing so, the previous page number is carried over.
[eval exp="f.current_page_index = 1" cond="f.current_page_index == undefined"]
[eval exp="f.current_page_label = '*Sub_Page' + f.current_page_index" ]

; [mask]
; Apply a mask and prepare to rebuild the screen  clean.
[mask graphic="mask_logo.png" time="800" effect="fadeInUp"]

; [destroy]
; This is a macro (→macro.ks)。
; Destroy and completely wipe the screen.
[destroy]

; [bg]
; Display a background.
; The storage parameter of [bg] usually points to a file located in the ｢bgimage｣ folder, but
; you can point to other folders by using the format ｢../fgimage/｣, you can specify another folder. 
; (｢fgimage｣ in the this example.)
; This displays a single white pixel in PNG format and stretches it across the entire screen.
[bg time="0" storage="../fgimage/color/white.png"]

; [wait]
; We'll show a mask image, so wait a little while.
[wait time="600"]

; [button]
; Display the ｢Previous｣ and ｢Next｣ buttons.
; These buttons will be always shown, so the setting fix="true" is used.
; (When fix="true", the jump destination is called as a subroutine and the call stack is built up.)
; Please note the x coordinate here. The coordinates are -400, 960.
; At this point, it is offscreen!
; Later we will use [anim] to move it onscreen.
[button name="prev" graphic="title_button/prev.png" x="-400" y="575" fix="true" target="*Sub_Prev"]
[button name="next" graphic="title_button/next.png" x=" 960" y="575" fix="true" target="*Sub_Next"]

; [call]
; Call the subroutine that displays the selections.
[call target="& f.current_page_label"]

; [mask_off]
; The game screen is now prepared, so we remove the mask.
[mask_off time="400"]

; Stop the game.
; As a rule, the game should be stopped after displaying selections.
[s]



; ----------------------------------------------------------
*Sub_Page1
; ----------------------------------------------------------

; Selections display subroutine: Page 1.

; [index_reset]
; This is a macro (→macro.ks)。
; Reset the list item number (temporary variable).
[index_reset]

; [list_item]
; This is a macro (→macro.ks)。
; Display the list items.
[list_item text1="01… Message&thinsp;Window&thinsp;Basics!      " text2="[l][r][er][cm][p] + more     " storage="01_text.ks      " shiryou="no"]
[list_item text1="02… Formatting&thinsp;Message&thinsp;Text    " text2="[font][ruby][graph] + more   " storage="02_decotext.ks  " shiryou="no"]
[list_item text1="03… Understanding&thinsp;Layers" text2="[trans][wt][backlay] + more  " storage="03_layer.ks     " shiryou="no"]
[list_item text1="04… Setting&thinsp;Background&thinsp;Images              " text2="[bg]                    " storage="04_bg.ks"         shiryou="no"]
[list_item text1="05… Placing&thinsp;Images  " text2="[image]                 " storage="05_image.ks     " shiryou="no"]
[list_item text1="06… Placing&thinsp;Text  " text2="[ptext]                 " storage="06_ptext.ks     " shiryou="no"]

; [anim]
; The ｢Previous｣ button is offscreen; the ｢Next｣ button is onscreen.
[anim name="prev" left="-400" time="0"]
[anim name="next" left=" 660" time="0"]

; [button]
; Place the button to go to the welcome screen.
; This is not a fix button, so it can be removed using [cm].
[button graphic="title_button/title.png" x="-100" y="575" target="*BackTitle"]

; [return]
; This label is called as a subroutine, so [return] must be used.
[return]



; ----------------------------------------------------------
*Sub_Page2
; ----------------------------------------------------------

; Selections display subroutine: Page 2.

[index_reset]
[list_item text1="07… Using&thinsp;Text&thinsp;Effects    " text2="[mtext]                                           " storage="07_mtext.ks     " shiryou="yes"]
[list_item text1="08… Character&thinsp;Objects" text2="[chara_new][chara_show][chara_mod][chara_config]+ more" storage="08_character.ks " shiryou="yes"]
[list_item text1="09… Image&thinsp;Buttons  " text2="[button]                                          " storage="09_button.ks    " shiryou="no"]
[list_item text1="10… Fixed&thinsp;Buttons  " text2="[button&thinsp;fix=true]                            " storage="10_fixbutton.ks " shiryou="no"]
[list_item text1="11… Text&thinsp;Buttons  " text2="[glink][loadcss]                                  " storage="11_glink.ks     " shiryou="no"]
[list_item text1="12… Animating&thinsp;Objects!        " text2="[anim][kanim][chara_move][quake][keyframe] + more      " storage="12_anim.ks      " shiryou="no"]
[anim name="prev" left="-100" time="0" effect="easeOutBack"]
[anim name="next" left=" 660" time="0" effect="easeOutBack"]
[return]



; ----------------------------------------------------------
*Sub_Page3
; ----------------------------------------------------------

; Selections display subroutine: Page 3.

[index_reset]
[list_item text1="13… Create&thinsp;a&thinsp;Clickable&thinsp;Map      " text2="[clickable]             " storage="13_clickable.ks" shiryou="no"]
[list_item text1="14… Displaying&thinsp;Selections!　    " text2="[link][glink][button]   " storage="14_select.ks"    shiryou="no"]
[list_item text1="15… Using&thinsp;User&thinsp;Input&thinsp;①  " text2="[edit][commit]          " storage="15_input_1.ks"   shiryou="no"]
[list_item text1="16… Using&thinsp;User&thinsp;Input&thinsp;②  " text2="[button][iscript]       " storage="16_input_2.ks"   shiryou="no"]
[list_item text1="17… Customizing&thinsp;Windows&thinsp;①" text2="[position][button]      " storage="17_window_1.ks"  shiryou="no"]
[list_item text1="18… Customizing&thinsp;Windows&thinsp;②" text2="[layopt][current]       " storage="18_window_2.ks"  shiryou="no"]
[anim name="prev" left="-100" time="0" effect="easeOutBack"]
[anim name="next" left=" 660" time="0" effect="easeOutBack"]
[return]



; ----------------------------------------------------------
*Sub_Page4
; ----------------------------------------------------------

; Selections display subroutine: Page 4.

[index_reset]
[list_item text1="19… Variables&thinsp;①&ensp;What&thinsp;Are&thinsp;They?               " text2="[eval][iscript]                " storage="19_variable_1.ks" shiryou="yes"]
[list_item text1="20… Variables&thinsp;②&ensp;Using&thinsp;Flags&thinsp;&&thinsp;Stats" text2="[eval][iscript][if]            " storage="20_variable_2.ks" shiryou="no"]
[list_item text1="21… Subroutines&thinsp;and&thinsp;Macros                     " text2="[call][return][macro][endmacro]" storage="21_macro.ks"      shiryou="yes"]
[list_item text1="22… Fonts                                 " text2="[loadcss][font][deffont]       " storage="22_font.ks"       shiryou="no"]
[list_item text1="23… Let's&thinsp;Play&thinsp;an&thinsp;Escape&thinsp;Game!                                   "                                         storage="23_escape.ks"     shiryou="no"]
[anim name="prev" left="-100" time="0" effect="easeOutBack"]
[anim name="next" left=" 960" time="0" effect="easeOutBack"]
[return]



; ----------------------------------------------------------
*Sub_Page5
; ----------------------------------------------------------

; Selections display subroutine: Page 5.

[index_reset]
[anim name="prev" left="-100" time="0" effect="easeOutBack"]
[anim name="next" left=" 960" time="0" effect="easeOutBack"]
[return]



; ----------------------------------------------------------
*Sub_Next
; ----------------------------------------------------------

; Subroutine to move to the next page.

; [ignore]～[endignore]
; The [ignore] tag checks the contents of the exp attribute (condition)
; and if the condition is true, ignores all script until [endignore].
;
; If the current page number is above 4, 
; ignore the process that moves to the next page.
;
; There are no more pages and the program will bug if it proceeds,
; so we have use [ignore] as a countermeasure.
[ignore exp="f.current_page_index >= 4"]

; [eval]
; Increase the current page number by 1 and update the current page label.
[eval exp="f.current_page_index += 1"]
[eval exp="f.current_page_label = '*Sub_Page' + f.current_page_index"]

; [cm]
; Release the free layer.
; (Clickable buttons and unfixed graphical buttons will be removed.)
[cm]

; [freeimage]
; Release layer 0.
; (The images and text that make up the list items will all be removed.)
[freeimage layer="0"]

; [call]
; Call the subroutine that displays the next page of contents.
[call target="&f.current_page_label"]

; [endignore]
; If ignoring the script, stop ignoring from until this point.
[endignore]

; [return]
; This label is called by a fix button.
; It is called as a subroutine, so a [return] tag is required.
[return]



; ----------------------------------------------------------
*Sub_Prev
; ----------------------------------------------------------

; Subroutine to proceed to the next page.

[ignore exp="f.current_page_index <= 1"]
[eval exp="f.current_page_index--"]
[eval exp="f.current_page_label = '*Sub_Page' + f.current_page_index"]
[cm]
[freeimage layer="0"]
[call target="&f.current_page_label"]
[endignore]
[return]



; ----------------------------------------------------------
*BackTitle
; ----------------------------------------------------------

[dialog type="confirm" text="Return&thinsp;to&thinsp;the&thinsp;welcome&thinsp;screen?" target="*BackTitleYes" target_cancel="*BackTitleNo" label_ok="Yes" label_cancel="No"]
[s]

; ------------------------------------------------------
*BackTitleNo
; ------------------------------------------------------

[cm]
[freeimage layer="0"]
[call target="*Sub_Page1"]
[s]

; ------------------------------------------------------
*BackTitleYes
; ------------------------------------------------------

[eval exp=" sf.title_moumita = 0 "]
[mask time="700"]
[destroy]
[mask_off time="0"]
[jump storage="title.ks"]