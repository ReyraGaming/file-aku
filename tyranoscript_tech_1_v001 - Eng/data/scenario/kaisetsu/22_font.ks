; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 22_font.ks ============================================
;
; This tutorial introduces the font file.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's have a look at fonts.[p][akn/def]

; ★Fonts is a general term for text design.

'Font' is a general term for text design. Let's look at some examples.[p]
You can specify which font to use in TyranoScript, but...[p][akn/dok]

; ★Fonts available in your computer may not be available to all players.

you have to be careful!![p]
Fonts that are available on your computer may not be available to players running your game.[p]
For example, you can almost always find 'Arial' on Windows, but it's usually not available on Macs.[p]
That's because 'Arial' is not installed on Macs by default.[p][akn/def]

; ★How to use fonts that are not in the player's computer?

...[p]
So let's see how to use fonts that are not available on a player's computer.[p]



; [font] Set unavailable fonts by using the face parameter.
[font face="TyranoFont"]



〔This is how it looks when the font you set is not available.〕[p][resetfont]
What do you think?[p]
It doesn't look that good...[p]
There are several options for these cases.[p][akn/hap]

; ★Option 1： Set multiple fonts.

The first option is to set multiple fonts.[p][akn/def]
When you set multiple fonts separated by commas, the device will use the first available font, so...[p]
if you set enough fonts to cover any environment, you'll probably have at least one that works.[p]

; [font] Set an unavailable font in the face parameter, but
; at the same time set an available font using a comma to separate the two.
[font face="TyranoFont, Meiryo"]

〔Here's how setting several fonts looks.〕[p][resetfont]
How's that? The font was different to before, right?[p][akn/hap]

; ★Option 2: Use a custom font.

The second option is to include the font that you want to use in the game package.[p][akn/def]
This option is used in this technical sample collection. How to do it:[p]
① Add the font file to the game package[p]
② Set a Web font in a CSS file[p]
③ Load the CSS file using the［loadcss]tag, then...[p]
④ Use the font name in the config file or［font］tag.[p]
Then you're all set!![p][akn/dok]

; It is a good idea to set multiple types of custom fonts.

...[p]
This method allows you to can provide and use the same font across multiple player environments...[p]
or so you might think, but there is a point to beware of...[p][akn/def]
Basically, there are various types of font file. Namely:[p]

[image layer="1" storage="color/col1.png" x="40" y=" 40" width="160" height="70"]
[image layer="1" storage="color/col2.png" x="40" y="120" width="160" height="70"]
[image layer="1" storage="color/col3.png" x="40" y="200" width="160" height="70"]
[image layer="1" storage="color/col4.png" x="40" y="280" width="160" height="70"]
[image layer="1" storage="color/col5.png" x="40" y="360" width="160" height="70"]
[ptext layer="1" x=" 40" y=" 40" width="160" align="center" color="white" size="50" edge="black" text="TTF"]
[ptext layer="1" x=" 40" y="120" width="160" align="center" color="white" size="50" edge="black" text="OTF"]
[ptext layer="1" x=" 40" y="200" width="160" align="center" color="white" size="50" edge="black" text="EOT"]
[ptext layer="1" x=" 40" y="280" width="160" align="center" color="white" size="50" edge="black" text="WOFF"]
[ptext layer="1" x=" 40" y="360" width="160" align="center" color="white" size="50" edge="black" text="SVG"]

That's quite a few different types.[p]
By the way, here's what they stand for:[p]

[image layer="2" storage="color/col1.png" x="240" y=" 40" width="660" height="70"]
[image layer="2" storage="color/col2.png" x="240" y="120" width="660" height="70"]
[image layer="2" storage="color/col3.png" x="240" y="200" width="660" height="70"]
[image layer="2" storage="color/col4.png" x="240" y="280" width="660" height="70"]
[image layer="2" storage="color/col5.png" x="240" y="360" width="660" height="70"]
[ptext layer="2" x="260" y=" 40"  color="white" size="50" edge="black" text="True&ensp;Type&ensp;Font"]
[ptext layer="2" x="260" y="120"  color="white" size="50" edge="black" text="Open&ensp;Type&ensp;Font"]
[ptext layer="2" x="260" y="200"  color="white" size="50" edge="black" text="Embedded&ensp;Open&ensp;Type"]
[ptext layer="2" x="260" y="280"  color="white" size="50" edge="black" text="Web&ensp;Open&ensp;Font&ensp;Format"]
[ptext layer="2" x="260" y="360"  color="white" size="50" edge="black" text="Scalable&ensp;Vector&ensp;Graphics"]

There we go.[p]
Don't worry... you don't have to remember all those.[p]

[freeimage layer="2" time="1000"][akn/sad]

Also, there may be some where a particular font is not compatible with the player's computer.[p]
For example, some old browser don't recognize 「EOT」format fonts.[p][akn/def]s
To resolve this, we have to fall back on the strategy of setting multiple fonts...[p]
The best approach is to include and set multiple types of font files.[p]
For more details, look up 'Web fonts'!![p]

; For example, see the below web site for reference.
; ･Using CSS3 Web fonts
; https://thinkit.co.jp/story/2011/08/18/2233

There are also web services available that will convert one type of font file into different formats.[p]

[freeimage layer="1" time="1000"]

That concludes our font tutorial.[p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]



★Side note: Setting fonts that have spaces in the filename.

Spaces cannot be used within tag parameters in TyranoScript.
Or rather, if they are included, they will be ignored when the script is run.
For this reason, using font names that include spaces such as 「MS Gotchic」 will cause problems.
In this cases, you can use [iscript] to set the font names as variables.

[iscript]
tf.face = 'MS Gothic, Meiryo, Jiyukubo'
[endscript]
[font face="& tf.face"]