; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 09_button.ks ==========================================
;
; Let's take a look at button objects.
; Button objects are used to perform a scenario jump when the player clicks (or taps the screen.)
;
; ★Main tags introduced:
; [button] … Places a button object. Button objects are placed on the free layer.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's go over button objects.[p]
A button object peforms a jump when the player clicks or taps on it.[p]
A button can be either an image or text.[r]
In this tutorial, we'll look at image buttons.[p][akn/hap]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ●
; ●Image button object basics.
; ●

Image button objects are placed using the［button］tag.[p]
I'll place one right now. Click on it to proceed.[l]

; [button] Place a basic button.
[button target=*Part2 x=145 y=195 graphic=09_button_A1.png]

; [s] Stop the game. Always place one of these after placing a button.
; If the game is not stopped, it will not wait for a click before proceeding.
[s]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; [cm] Release the free layer.
[cm]

[akn/def]
Thank you for clicking.[p]
That's how it works![p]
These are often used on title screens and for making selections that branch the story in visual novels.[p]

Button objects are placed on a special layer called the 'free layer'.[p]
The scenario will come to a halt when there is an object on the free layer,[p]
so be sure to place a tag［cm］at jump destination to remove the button.[p][akn/dok]
Image buttons don't just disappear by themselves, y'know![p][akn/def]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ●
; ●Creating a hint button.
; ●

Now let's look at how you can make your buttons cool.[p]
You can add hint text that appears when the player hovers the mouse pointer over it for a short time.[l]

; [button] Using the hint parameter.
[button target=*Part4 hint='Button' x=145 y=195 graphic=09_button_A1.png]
[s]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

[cm]

; ●
; ●Screen switching button.
; ●

What do you think?[p]
Now we'll make the image button change on mouse hover or click.[l]

; [button] Using the enterimg and clickimg parameters.
[button target=*Part5 enterimg=09_button_A2.png clickimg=09_button_A3.png hint=This is a button object. x=145 y=195 graphic=09_button_A1.png]
[s]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

; [wait] The player won't see the clickimg if [cm] is used right away, so we'll add a 300ms wait.
[wait time=300]
[cm]

; ●
; ●Buttons with sound effects.
; ●

You saw it change, right?[p]
Let's add a sound effect too![p]
Now the button will make a sound on mouse hover or when the mouse moves away or when clicked.[l]

; [ptext] Credit: The sound effect is by 'Music is VFR'.
[ptext layer="1" x="5" y="0" size="40" edge=black color=white text="Sound&ensp;effect&ensp;created&ensp;by&ensp;Music&ensp;is&ensp;VFR" name="credit"]

; [button] Button using the enterse、leavese、clickse parameters.
[button target=*Part6 enterse=09_button_1.ogg leavese=09_button_2.ogg clickse=09_button_3.ogg enterimg=09_button_A2.png clickimg=09_button_A3.png hint=This is a button object. x=145 y=195 graphic=09_button_A1.png]
[s]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------

[free layer=1 name=credit]
[wait time=300]
[cm]

; ●
; ●A button with entry and exit animations.
; ●

[akn/hap]
Buttons with sounds effects are pretty sweet, eh?[p][akn/def]
You can assign names to buttons in the same way as other objects.[p]
Then you can use the name to animate your buttons.[p]
Here is our button with entry and exit animations.[l]

; [keyframe]～[endkeyframe]x2 Define entry and exit animations.
[keyframe name=ZoomIn]
  [frame p=0%   scale=0 y=-15 opacity=0]
  [frame p=100% scale=1 y=0   opacity=1]
[endkeyframe]
[keyframe name=ZoomOut]
  [frame p=0%   scale=1 y=0   opacity=1]
  [frame p=100% scale=2 y=-15 opacity=0]
[endkeyframe]

[button target=*Part7 name=Button enterse=09_button_1.ogg leavese=09_button_2.ogg clickse=09_button_3.ogg enterimg=09_button_A2.png clickimg=09_button_A3.png hint=This is a text object. x=145 y=195 graphic=09_button_A1.png]
; [kanim][wa] Apply the entry key frame animation → Await completion.
; By enclosing this process in [button] ~ [s] you can add entry animation.
[kanim keyframe=ZoomIn name=Button time=1000]
[wa]
[s]



;-----------------------------------------------------------
*Part7
;-----------------------------------------------------------

; [kanim][wa] Apply the exit key frame animation → Await completion.
[kanim keyframe=ZoomOut name=Button time=250]
[wa]
[cm]

; ●
; ●A button that runs JavaScript.
; ●

[akn/hap]
Ha ha![p][akn/def]
Finally, let's have a look at adding functionality to buttons.[p]
Button objects can do more than just jump to a label.[p]
They can also run preset JavaScript when the jump is activated.[p]
You can use this to have buttons store values in variables.[l]

; [button]x2 An example using the exp parameter.
; The instant that the button is clicked, the text value 'Blue' or 'Red' is stored in the variable f.a.
[button target=*Part8 exp=" f.a = 'blue' " x=145 y=195 graphic=09_button_A1.png]
[button target=*Part8 exp=" f.a = 'red' " x=675 y=195 graphic=09_button_B1.png]
[s]



;-----------------------------------------------------------
*Part8
;-----------------------------------------------------------

[cm]

The button you pressed was... 

; [emb] We'll retrieve the value stored in the variable f.a as a message.
; Depending on the button clicked, f.a should now hold the value 'blue' or 'red'.
[emb exp=f.a]

!![p]
Well, that was our tutorial on image buttons.[p]
You can also use the［button］tag to create special buttons called 'fixed buttons'.[p][akn/def]
We'll go over these in the next tutorial![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]