; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 06_ptext.ks ===========================================
;
; 
;
; ★Main tags introduced:
; [ptext] … Places a text object on a specified layer.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's follow up the images tutorial with a text tutorial!![p]
This time, we'll look at how to place text objects.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

Use the［ptext］tag to place text objects on a layer.[p]
Let's go straight to an example.[p]

; [ptext] Place a text object.
[ptext layer="1" x="0" y="0" size="40" text="This&ensp;is&ensp;a&ensp;text&ensp;object."]

All right...[p]
I placed a text object on foreground layer 1.[p]
This is the most basic example.[p]
Let's take that away for now...[p]

; [freeimage] Release foreground layer 1 (releases all objects.)
[freeimage layer="1" time="700"]

and see some examples of what else we can do.[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

To format text, you can do almost everything that you can do with the［font］tag with［ptext］.[p]
For example, change the font size or color, or apply bold, stroke, shadow, etc...[p]

; [ptext]x6 You can apply various formats to text objects.
[ptext layer="1" x="0" y="  0" text="(1) This&ensp;is&ensp;a&ensp;text&ensp;object." size="40"]
[ptext layer="1" x="0" y=" 50" text="(2) This&ensp;is&ensp;a&ensp;text&ensp;object." size="40"   bold="bold"    ]
[ptext layer="1" x="0" y="100" text="(3) This&ensp;is&ensp;a&ensp;text&ensp;object." size="40"  color="0xff0000"]
[ptext layer="1" x="0" y="150" text="(4) This&ensp;is&ensp;a&ensp;text&ensp;object." size="40"   edge="0xff0000"]
[ptext layer="1" x="0" y="200" text="(5) This&ensp;is&ensp;a&ensp;text&ensp;object." size="40" shadow="0xff0000"]
[ptext layer="1" x="0" y="250" text="(6) This&ensp;is&ensp;a&ensp;text&ensp;object." size="40"   face="Meiryo"  ]

Like that![p]

; [freeimage] Release foreground layer 1.
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

Onto text direction...[p]
You can format text to appear vertically.[p]

; [ptext]x2 Place vertical and horizontal text objects. Use vertical="true" to set text to vertical.
[ptext layer="1" x="0" y="50" size="40" edge="0x000000" text="This&ensp;is&ensp;vertical." vertical="true" ]
[ptext layer="1" x="0" y=" 0" size="40" edge="0x000000" text="This&ensp;is&ensp;a&ensp;horizontal&ensp;text&ensp;object." vertical="false"]

Just like that![p]
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

Horizontal text can be aligned to the left, center, or right.[p]

; [ptext]x3 Use the width and align paraemeters to set the alignment.
[ptext layer="2" x="0" y="  0" text="(Left)This&ensp;is&ensp;a&ensp;text&ensp;object." size="40" width="960" align="left"  ]
[ptext layer="2" x="0" y="150" text="(Center)This&ensp;is&ensp;a&ensp;text&ensp;object." size="40" width="960" align="center"]
[ptext layer="2" x="0" y="300" text="(Right)This&ensp;is&ensp;a&ensp;text&ensp;object." size="40" width="960" align="right" ]

There we go.[p]
... It is rather messy, though.[p]
[image layer="1" x="  0" y="  0" storage="color/black.png"  width="960" height="460"]
[image layer="1" x="  0" y="  0" storage="color/red.png"    width=" 10" height="150"]
[image layer="1" x="470" y="155" storage="color/red.png"    width=" 10" height="150"]
[image layer="1" x="950" y="310" storage="color/red.png"    width=" 10" height="150"]
That's better![p]
As you can see, the objects are aligned to the left, center, and right.[p]
You can use this to center align the text in the name box or or align to the right when showing numbers.[p]
[freeimage layer="2" time="700" wait="false"]
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

［ptext］can also overwrite the content of an existing text object.[p]
Let's see this in action.[p]

; [ptext] Place a text object. Let's just call it "my_object".
[ptext layer="1" x="0" y="0" name="my_object" text="This&ensp;is&ensp;a&ensp;text&ensp;object." size="40" edge="0x000000" width="960" align="right"]

I placed a text object at the upper right.[p]
Now we'll overwrite this text...[p]

; [ptext] Overwrite the text. Set the name to "my_object".
[ptext layer="1" x="0" y="0" name="my_object" text="Overwritten!" overwrite="true"]

; If overwrite="true",
; it will overwrite the the contents of any text object with the same name.
; By the way, if no text objective matches the specified name,
; a new text object will be added, so be careful.

This way you can overwrite text while maintaining the text object position and formatting![p]
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------

So far we have displayed our text objects instantly, but...[p]
just as with image objects, we can have them appear over a set duration.[p]

; [ptext][wait] Place over a duration → Await completion.
[ptext     time="1400" layer="1" x="300" y="300" name="my_object" text="This&ensp;is&ensp;a&ensp;text&ensp;object." size="40" edge="0x000000"]
[wait      time="1400"]

... Got it?[p][akn/hap]
Remember these points and you too can be a text object master!![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]