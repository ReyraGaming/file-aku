; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 07_mtext.ks ===========================================
;
; We'll take a look at text effect objects.
;
; ★Main tags introduced:
; [mtext] … Places a text effect object.
;
; ==========================================================



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ●
; ●Go ahead and place a text effect object.
; ●

Now let's take a look at 'text effect objects'.[p]
These objects can produce flashy text effects and animations.[p]
Like this...[p]

; [mtext]x2 Place a text effect object.
; The second will appear after the first is completely onscreen.
[mtext text="Rumble..." layer="0" x="200" y="100" size="70" in_effect="wobble" time="0" color="0x000000" edge="0xffffff"]
[mtext text="Rumble..." layer="0" x="600" y="300" size="70" in_effect="wobble" time="0" color="0x000000" edge="0xffffff"]

That's just one example. There are many other types of effects.[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; ●
; ●Proceeding to the next line of script without waiting for the effect to end.
; ●

You can use multiple text effects at the same time, like this...[p]

; [mtext]x4 By setting the wait parameter to false, you can have the game proceed to the next line of script without waiting for the text to disappear.
[mtext text="Rumble..." layer="0" x="200" y="100" size="70" in_effect="wobble" time="0" color="0x000000" edge="0xffffff" wait="false"]
[mtext text="Rumble..." layer="0" x="460" y="300" size="70" in_effect="wobble" time="0" color="0x000000" edge="0xffffff" wait="false"]
[mtext text="Rumble..." layer="0" x="560" y="100" size="70" in_effect="wobble" time="0" color="0x000000" edge="0xffffff" wait="false"]
[mtext text="Rumble..." layer="0" x="100" y="300" size="70" in_effect="wobble" time="0" color="0x000000" edge="0xffffff"]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ●
; ●Leave it on the layer without fading out.
; ●

Text effect objects usually disappear, but you disable the exit animation so the text stays on the layer.[p]

; [mtext] Set the fadeout parameter to false to prevent the animation from exiting.
; Also, by using the name paramter, you can apply other operations to the text effect.
[mtext text="Classroom&emsp;7:30am" layer="0" x="5" y="5" size="40" in_effect="fadeInRightBig" time="0" color="0xc0006f" edge="0xffffff" fadeout="false" name="mtext"]

[akn/hap]
Tee-hee.[p][akn/def]
You can use text effects at the same time as changing the background.[p]

; [free] Select and release the text effect object.
[free layer="0" name="mtext"]

; [mtext][bg] Apply the text effect object and background change simultaneously.
[mtext text="Corridor&emsp;7:45am" layer="0" x="5" y="5" size="40" in_effect="fadeInRightBig" time="0" color="0xc0006f" edge="0xffffff" fadeout="false" name="mtext" wait="false"]
[bg storage="rouka.jpg"]

[akn/hap]
This is particularly useful in point and click adventure games![p]
[free layer="0" name="mtext"]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

; ●
; ●Using text effects on the message layer.
; ●

[akn/def]With a little tinkering, you can use text effects in the message window too![p]
Let's give it a try.[p][akn/dok]

; [glyph] Direcly set the location of the image awaiting a click.
[glyph fix="true" left="850" top="575"]

; [mtext] We'll look at layer=message0. This makes message layer 0 the subject of the output. 
[mtext layer="message0" x="35" y="505" text="WAAaaaAAaaaHHHHH!!!" size="80" edge="0x000000" fadeout="false" in_effect="bounceIn" name="mtext"]

[l]
[free name="mtext" layer="message0"]

; [glyph][mtext] Do the same thing.
[glyph fix="true" left="920" top="575"]
[mtext layer="message0" x="35" y="505" text="There's&ensp;a&ensp;M-MONSTER&ensp;behind&ensp;you!!!" size="45" edge="0x000000" fadeout="false" in_effect="bounceIn" name="mtext"]

[l]
[free name="mtext" layer="message0"]

; [glyph] Move the clickable image back to the original location.
[glyph fix="false"]

[akn/hap]
Psych![p]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

; ●
; ●Hide the screen and emphasize the text.
; ●

[akn/def]
You can also use text effects while masking the screen.[p]

; [layopt]x2 Hide message layer 0 and the fix layer.
[layopt layer=message0 visible=false]
[layopt layer=fix      visible=false]

; [image][anim] Stretch a black image across the screen and immediately set the opacity to 0.
[image layer=0 x=0 y=0 width=960 height=640 storage=color/black.png name=black]
[anim name=black opacity=0 time=0]

; [anim][wa] Set the opacity of the black image to 190 over 1,000ms and wait for the animation to complete.
[anim name=black opacity=190 time=1000 effect=easeOutExpo]
[wa]

; [mtext]x2 Place a text effect object.
[mtext layer=1 x=0 y=280 width=960 align=center size=36 text=Hey...                 in_shuffle=true out_shuffle=true]
[mtext layer=1 x=0 y=280 width=960 align=center size=36 text=Can&ensp;you...&ensp;hear&ensp;me? in_shuffle=true out_shuffle=true]

; [free] Remove the black image.
[free  layer=0 name=black time=1000]

; [layopt]x2 Unhide message layer 0 and the fix layer.
[layopt layer=message0 visible=true]
[layopt layer=fix      visible=true]

Pretty neat, eh.[p]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------

; ●
; ●Let's create end credits.[p]
; ●

One way to use text effects is in end credits.[p]

; [layopt]x2 Hide message layer 0 and the fixt layer.
[layopt layer=message0 visible=false]
[layopt layer=fix      visible=false]

; [image] Cover foreground layer 1 with a black image. (This hides the base layer and foreground layer 0.)
[image  layer=1 storage=color/black.png x=0 y=0 width=960 height=640 time=1000]

; [image] Place the image in the background images folder on foreground layer 2 as an image object.
[image layer=2 time=1000 x=40 y=100 width=400 folder=bgimage storage=room.jpg]

; ([mtext][wait])x2 Place a text effect object on foreground layer 2 using wait=false.
; Immediately after, use the [wait] tag and adjust the wait duration.
[mtext layer=2 text=Director  x=520 y=200 size=30 color=white wait=false in_effect=rotateIn time=3000 out_effect=rotateOut] [wait time=500]
[mtext layer=2 text=Ogawa&ensp;Yuji x=560 y=240 size=30 color=white wait=false in_effect=rotateIn time=3000 out_effect=rotateOut] [wait time=3000]

; [freeimage] Release foreground layer 2.
[freeimage layer=2 time=1000]

; [image]～[freeimage] Repeat the above operation.
[image layer=2 time=1000 x=520 y=300 width=400 storage=../bgimage/rouka.jpg]
[mtext layer=2 text=Scenario      x=80  y=340 size=30 color=white wait=false in_effect=rotateIn time=3000 out_effect=rotateOut] [wait time=500]
[mtext layer=2 text=Hasegawa&ensp;Yuko  x=120 y=380 size=30 color=white wait=false in_effect=rotateIn time=3000 out_effect=rotateOut] [wait time=3000]
[freeimage layer=2 time=1000]

; [freeimage] Remove the black image covering foreground layer 1.
[freeimage layer=1 time=1000]

; [layopt]x2 Show message layer 0 and the fix layer.
[layopt layer=message0 visible=true]
[layopt layer=fix      visible=true]

... ...[p][akn/hap]
Well, that was the text effect tutorial![p]
You can find a list of the parameters and effects used in the asset collection.[p]
Please check them out!![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]