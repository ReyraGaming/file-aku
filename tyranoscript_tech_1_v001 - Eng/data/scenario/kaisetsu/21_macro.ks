; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 21_macro.ks ===========================================
;
; This tutorial covers subroutines and macros.
;
; ★Main tags introduced:
; [call]     … Jump to a specific scenario file or label. Return to the origin using [return].
; [return]   … Return to where [call] was used.
; [macro]    … Start macro definition.
; [endmacro] … End macro definition.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's learn about subroutines and macros.[p]
This time, let's open a scenario file and see how it works.[p]
Open up the file located at 'data/scenario/kaisetsu/21_macro.ks'.[p]
...[p]
Both subroutines and macros are functions that concisely describe complex operations.[p]
To put that in another way...[p]
They allow you to give a name to a string of operations, then run those operations just by using the name.[p]
... Are you with me so far?[p][akn/dok]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

... ...[p]
That was probably a little vague, so let's look at a real life example.[p][akn/def]
Say we have a script that creates this effect:[p]



; [image][anim][wa][free] Place an image object > Animate it > Wait for the animation to end > Remove the object.
[image name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
[anim  name=macro_effect time=1000 left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
[wa]
[free  name=macro_effect layer=0]



The effect uses a couple of lines of script with ［image］,［anim］,［wa］, and［free］tags.[p]
Now then... 



[image name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
[anim  name=macro_effect time=1000 left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
[wa]
[free  name=macro_effect layer=0]

 you could... 

[image name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
[anim  name=macro_effect time=1000 left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
[wa]
[free  name=macro_effect layer=0]

 copy and paste the script every time that you want to use the effect...[p][akn/dok]
But it seems like a waste of time, right? I mean, you're doing the same thing over and over again.[p][akn/hap]
Here's where we can put  subroutines or macros to be use.[p][akn/def]
For example, with a subroutine, you could place the script at the end of the scenario or in a separate scenario file.[p]
Just don't forget to use a label and［return］tag.[p]
Then, if you place a［call］tag where you would like to run the script...

; [call] Jump to the subroutine label 「*Sub_Effect」. Returns here with the [return] tag.
[call target=*Sub_Effect]

[er]you can run the effect just like that[p]
For a macros, you would define the operations between［macro］and［endmacro］tags, then...[p]

; [macro]～[endmacro] Define the macro [eff].
[macro name=eff]
  [image name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
  [anim  name=macro_effect time=1000 left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
  [wa]
  [free  name=macro_effect layer=0]
[endmacro]



if you use the name of the macro...



; [eff] The macro that we defined above.
[eff]



[er]the effect will immediately run .[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

Now, let's take a look at what happens behind the scenes when you use a subroutine or macro.[p]
[image layer=1 storage=21_macro_A.png x=0 y=0 time=600]
So, we have the scenario that looks like this.[p]
[image layer=1 storage=21_macro_B.png x=0 y=0 time=600]
Then, at the very bottom of the scenario, we have a scenario for the subroutine.[p]
Now then...[p]
[image layer=1 storage=21_macro_D.png x=0 y=0 time=600]
The script is executed in order from the top to the bottom as indicated by the arrow.[p]
When it runs into the［call］tag...[p]
[image layer=1 storage=21_macro_C.png x=0 y=0 time=600 name=STACK]
...a flag is triggered that marks where it will need to return.[p]
... 'flag' here is just a figure of speech. It's also sometimes known as a 'stack'.[p]
[image layer=1 storage=21_macro_E.png x=0 y=0 time=600]
If the flag is up, the script will jump to the subroutine label.[p]
[image layer=1 storage=21_macro_F.png x=0 y=0 time=600]
The script is executed toward the bottom as usual...[p]
[image layer=1 storage=21_macro_G.png x=0 y=0 time=600]
And when it meets a［return］tag, it returns to the point where the flag was tripped.[p]
[free layer=1 name=STACK time=600]
If it's not needed any more, the flag is disposed of.[p]
[image layer=1 storage=21_macro_H.png x=0 y=0 time=600]
And script execution continues toward the bottom.[p]
... and that's what happens in the engine.[p]
[freeimage layer=1 time=1000][akn/hap]
That example was a subroutine, but macros work in exactly the same way.[p]
When script hits a tag defined in a macro, a flag is placed and the script jumps to the［macro］.[p]
When it hits［endmacro］, it returns to the flag and script execution proceeds toward the bottom.[p]
And that is what goes on behind the scenes.[p][akn/def]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

We've gone over how subroutines and macros work in much the same way...[p][akn/ang]
Functionally, subroutines are simpler. All they do is 'go and return'.[p]
Macros, on the other hand... [akn/def] are a bit more advanced.[p]
You can use parameters in macros and pass values to parameters in macros.[p]
Bear in mind: unlike subroutines, macros must be defined before they can be used.[p]

Let's see an example of passing parameters to a macro![p]
We'll use time=%time to set the time parameter which we'll pass to the macro［eff2］in an［anim］tag.[p]
Let's do it![l]



[macro name=eff2]
  [image               name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
  ; [anim] Let's take a look at time=%time in this tag. If we write like this:
  ; when the macro [eff2] is called as [eff2 time=555], 555 will be passed to [anim].
  [anim  time=%time    name=macro_effect left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
  [wa]
  [free                name=macro_effect layer=0]
[endmacro]



[eff2 time=250]
[eff2 time=500]
[eff2 time=1000]
[eff2 time=2000]



[er]... and done![p]
In addition to setting the time attribute directly, we can also set a default value that will be...[p]
used whenever the time attribute is not specified.[p]



[macro name=eff3]
  [image                    name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
  [anim  time=%time|1500    name=macro_effect left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
  [wa]
  [free                     name=macro_effect layer=0]
[endmacro]



[eff3 time=500]
[eff3]
[eff3 time=500]
[eff3]



Say that we want the game to wait 1,500ms if the time attribute is not set.[p]

This is kind of an advanced technique...[p]
You can also run JavaScript in parameters passed to macros.[p]
For example... to access the time parameter passed to a macro, we'll use「mp.time」.[p]
If you use this and the「&」symbol...



[macro name=eff4]
  [image                    name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
  ; [anim] Writing time=&mp.time will have exactly the same action as writing time=%time.
  [anim  time=&mp.time      name=macro_effect left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
  [wa]
  [free                     name=macro_effect layer=0]
[endmacro]



[eff4 time=250]
[eff4 time=500]
[eff4 time=1000]
[eff4 time=2000]



[p]And that's how you can pass parameters to macros.[p]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

...[p]
As we just saw, you can make your script much cleaner by using subroutines and macros.[p][akn/hap]
They can also make rewriting or redesigning much easier.[p][akn/def]



; [macro]～[endmacro] Define the macro [strong].
[macro name=strong]
  [font edge=blue]
[endmacro]



For example, let's say we create a macro［strong］that highlights text.[p]
When the［strong］macro is used, text is highlighted using a blue stroke.[p][akn/ang]
You just used a［resetfont］, then randomly decide 'highlighted text would look better yellow'.[p][akn/dok]
If you used the［font］tag to highlight text throughout your game, it could be a LOT of work to change them all!![p]
You'd have to find and change all of the［font］tags in your scenario files.[p][akn/def]
But, if you use the［strong］macro, the only part you would need to change is the［strong］macro definition.[p]
If you change the contents of the definition, anywhere that uses the［strong］macro in the game will change.[p][akn/hap]

Subroutines and macros really are useful, so be sure to put them to use!![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]



;-------------------------------------------------------
*Sub_Effect

[image name=macro_effect layer=0 left=430 top=270 width=100 height=100 storage=color/white.png]
[anim  name=macro_effect time=1000 left="-=100" top="-=100" width="+=200" height="+=200" opacity=0]
[wa]
[free  name=macro_effect layer=0]

[return]