; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 11_glink.ks ===========================================
;
; How to use text button objects that can be used for selection choices, etc.
; This tutorial will also cover customizing buttons, etc.
;
; ★Main tags introduced.
; [glink]   … Place a text button object.
; [loadcss] … Read external CSS. (Used to place buttons with custom designs.)

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Text button objects are created using text.[p]
You don't have to prepare an image to use, so they're easy to make.[p]
Let's see some examples...

; [glink] Place a text button.
[glink x="370" y="300" text="Next" target="*Part1"]

; [s] Stop the game. Always use this after placing a button.
[s]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

There are 9 types of pre-made buttons available using the color parameter.[p]
Here are all 9 colors.

; [glink] Let's try the different colors. There are 9 altogether.
[glink x="230" y=" 60" text="black"  target="*Part2" color="Black" ]
[glink x="230" y="140" text="gray"   target="*Part2" color="Gray"  ]
[glink x="230" y="220" text="white"  target="*Part2" color="White" ]
[glink x="230" y="300" text="orange" target="*Part2" color="Orange"]
[glink x="230" y="380" text="red"    target="*Part2" color="Red"]
[glink x="490" y=" 60" text="blue"  target="*Part2" color="Blue"   ]
[glink x="490" y="140" text="rosy"  target="*Part2" color="Rose"   ]
[glink x="490" y="220" text="green" target="*Part2" color="Green"  ]
[glink x="490" y="300" text="pink"  target="*Part2" color="Pink"   ]
[s]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

You can also set background images. The image will be stretched to fit the button size.

; [glink] Example using the graphic and enterimg parameters.
[glink x="370" y="300" text="Next" target="*Part3" graphic="11_glink_A.png" enterimg="11_glink_B.png"]
[s]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

You can adjust the width, height, font size, and font color.[p]

; [glink]x3 Example using the size/width/height/font_color parameters.
[glink x="230" y="100" width="345" height="45" text="Next" target="*Part4" color="blue" size="40" font_color="0x000000"]
[glink x="230" y="200" width="345" height="45" text="Next" target="*Part4" color="blue" size="40" font_color="0x000000"]
[glink x="230" y="300" width="345" height="45" text="Next" target="*Part4" color="blue" size="40" font_color="0x000000"]
[s]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

[akn/dok]
You can also add sound effects, as with regular buttons.

; [ptext] Credit: Sound effect created by 'Music is VFR'.
[ptext layer="1" x="5" y="0" size="40" edge=black color=white text="Sound&ensp;effect&ensp;created&ensp;by&ensp;Music&ensp;is&ensp;VFR" name="credit"]

; [glink] Example using enterse/leavese/clickse parameters.
[glink enterse=09_button_1.ogg leavese=09_button_2.ogg clickse=09_button_3.ogg x="370" y="300" text="Next" target="*Part5" color="pink"]
[s]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

; [free] Remove the credit.
[free layer=1 name=credit]

Text buttons can also be animated.

; [glink]x3 Set the name parameter to be able to animate the button later.
[glink x="1320" y="100" width="200" text="Next" target="*Part6" color="red" name="glink1"]
[glink x="1320" y="200" width="200" text="Next" target="*Part6" color="red" name="glink2"]
[glink x="1320" y="300" width="200" text="Next" target="*Part6" color="red" name="glink3"]

; [anim]x3 Insert a short [wait], then apply the animation.
[anim name="glink1" time="500" left="-=1000"]
[wait time="100"]
[anim name="glink2" time="500" left="-=1000"]
[wait time="100"]
[anim name="glink3" time="500" left="-=1000"]
[s]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------

[akn/def]
……[p]
Those are the fundamentals of how to place text buttons using the［glink］tag, but...[p][akn/hap]
If you happen to know HTML or CSS, you can do all sorts[r] of other things!!![p][akn/def]
... Would you like to know more?[l]

[glink x="320" y="200" width="200" text="Sure!" target="*Part6.2" color="red"]
[glink x="320" y="300" width="200" text="Nah" target="*Part6.1" color="black"]
[s]

;-------------------------------------------------------
*Part6.1

[akn/hap]
Not right now?[p][akn/def]
All right, then. That's the end of our text button object tutorial.[p]
[jump target="*End"]

;-------------------------------------------------------
*Part6.2

［glink］'s color parameter is the equivalent of HTML's classes.[p]
You can use CSS to create your very own unique buttons.[p][akn/hap]
Just have a look at these buttons that I made![p][akn/def]
[jump target="*Part7.1"]


;-----------------------------------------------------------
*Part7.1
;-----------------------------------------------------------

These are flat design buttons.

; [glink]x3 Using custom values int eh color parameter.
; This button uses the [loadcss file="data/others/glink/glink.css"] tag in 「first.ks」.
; (Same for the following examples.)
[glink x="230" y="100" text="Next" target="*Part7.2" color="my-blue"]
[glink x="230" y="200" text="Next" target="*Part7.2" color="my-red"]
[glink x="230" y="300" text="Next" target="*Part7.2" color="my-orange"]
[s]



;-----------------------------------------------------------
*Part7.2
;-----------------------------------------------------------

Here is a simple text button with a background. The frame is a regular image object.

[image layer=1 storage=../image/11_glink_A.png x=320 y=70 width=320 height=320]
[glink x="230" y="100" text="Next" target="*Part7.3" color="my-text"]
[glink x="230" y="200" text="Next" target="*Part7.3" color="my-text"]
[glink x="230" y="300" text="Next" target="*Part7.3" color="my-text"]
[s]



;-----------------------------------------------------------
*Part7.3
;-----------------------------------------------------------

[freeimage layer="1"]

This design uses an image for the background.

[glink x="230" y="100" text="Next" target="*Part7.4" color="my-image"]
[glink x="230" y="200" text="Next" target="*Part7.4" color="my-image"]
[glink x="230" y="300" text="Next" target="*Part7.4" color="my-image"]
[s]



;-----------------------------------------------------------
*Part7.4
;-----------------------------------------------------------

Using animation #1.

[glink x="230" y="100" text="Next" target="*Par7.5" color="my-kanim"]
[glink x="230" y="200" text="Next" target="*Par7.5" color="my-kanim"]
[glink x="230" y="300" text="Next" target="*Par7.5" color="my-kanim"]
[s]



;-----------------------------------------------------------
*Par7.5
;-----------------------------------------------------------

Using animation #2.

[glink x="230" y="100" text="Next" target="*Part8" color="my-anim"]
[glink x="230" y="200" text="Next" target="*Part8" color="my-anim"]
[glink x="230" y="300" text="Next" target="*Part8" color="my-anim"]
[s]



;-----------------------------------------------------------
*Part8
;-----------------------------------------------------------

... etc, etc.[p][akn/hap]
Once you get the hang of it, you can create pretty much any kind of button!![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]