; [set_default_view] This macro creates the tutorial screen. (See "macro.ks" for details.)
[set_default_view]

; [akn/def] A standalone macro. While keeping "Akane" in the name box, change Akane's expression. It is defined in "macro.ks".
[akn/def]



; == 01_text.ks ============================================
;
; Let's start by displaying the text.
;
; ★The tags we'll use are:
; [l]   … Awaits a click
; [r]   … New line
; [er]  … Clear the messages
; [cm]  … Clears the messages. See below for the difference between this and [er].
; [p]   … Awaits a click, then clears the message when a click occurs.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Displaying text in the message window is super simple![l][er]
Just type text in the scenario file and it will appear in the window.[l][er]

Plus, you can use 'tags' to do things like:[l][er]
Have your game 'await a click'...[l][er]
Insert a[r]'line break'...[l][er]
Have your game [wait time="600"]'wait' for a set amount of time...[l][er]
Or you can clear all text from [er]the message window.[l][er]

If you don't use tags, text will wrap around the message window like this... WAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH[l][er]
So be sure to make use of them![l][er]

Let's take a look at the basic tags![l][er]
Use［l］to await a click,［er］or［cm］to clear messages, and［r］to insert a line break.[l][er]
You can also await a click and clear messages using［p］.[p]

; --The difference between [er] and [cm].
; [er] waits for a click to the currently in operation message layer, whereas
; [cm] waits for a click to all message layers.
; Also, [cm] also waits for a click to the free layer (the layer upon which button objects are placed)
; [cm] clears a variety of things.
; If in doubt, you should be fine just and using [cm].

[akn/hap]
Play around with these tags and you'll master them in no time!![p]


;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]