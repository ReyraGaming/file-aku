; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 19_variable_1.ks ======================================
;
; Learn about variables.
;
; ★Main tags introduced:
; [iscript] … Run whatever is between this tag and [endscript] as JavaScript.
; [eval] … Run the contents of the exp parameter as JavaScript.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

; ●
; ●You can branch the story by using variables-based flags or friendliness levels, etc.
; ●

Let's take a look at a slightly advanced technique: variables!![p][akn/hap]
You can use variables in a variety of ways.[p]
For example, you could branch a story using flags or friendliness...[p]
or use input from the player to show a name, and much, much, more.[p]


;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ●
; ●Variables are boxes in which 'something' is stored.
; ●

[chara_move name=akane left="+=200"][akn/def]

We'll start with an explanation of what variables are.[p]

[image layer=1 storage=19_variable_A.png x=100 y=190 zindex=10 name=boxF time=1000 wait=false]
[image layer=1 storage=19_variable_B.png x=100 y=190 zindex=20 name=boxF time=1000]

A variable is a box that's used to store something.[p]
For example, they might contain friendliness levels, or the hero's name...[p][akn/hap]
That's the first thing to know! Variables are boxes![p][akn/def]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; ●
; ●To place something in a variable, we use JavaScript. To use JavaScript, we use ［eval］ or ［iscript］.
; ●

To place something in a variable, we use JavaScript.[p]
To run JavaScript in TyranoScript games, we use the［eval］or［iscript］tags.[p]
Let's go ahead and place something in a variable.[p]



; [iscript]～[endscript] Store the value 100 in the variable f.xxx.
[iscript]
f.xxx = 100
[endscript]



[image layer=1 storage=19_variable_G.png x=480 y=320 time=1000 name=javascript]

I just ran the JavaScript「f.xxx = 100」.[p]

[image layer=1 storage=19_variable_C.png x=100 y=190 zindex=30 name=boxF time=1000]
[image layer=1 storage=19_variable_F.png x=100 y=0 zindex=15 name=boxF,itemF time=1000]
[anim name=itemF time=1000 top=190][wa]

Now we have a variable named「f.xxx」which contains the value「100」.[p][wa]
In JavaScript, the code「□ = ●」means「insert ● in □」.[p]
[akn/dok]
Remember, it does NOT mean「□ equals ●」!![p]

[free layer=1 name=javascript time=1000]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ●
; ●Use the ［emb］ tag to display the contents of the variable as a message.
; ●
[akn/def]
Now let's check the contents of our variable.[p]
To display the variable contents in a message, we use the［emb］tag.[p]
Let's give it a shot.[p][anim name=itemF time=1000 top=0]
The content of the variable「f.xxx」is「



; [emb] Display the contents of the variable f.xxx as a message.
[emb exp=f.xxx]



」.[p][wa][akn/hap]
Of course it contains 100 since we inserted that value earlier![p][akn/def][freeimage layer=1 time=1000]
...[p]
Now we've covered 'how to store something in a variable' and 'how to display it as a message'.[p]
BUT!!! You haven't mastered variables yet!![p]
To really master variables, we'll need a little more knowledge.[p]



;-----------------------------------------------------------
*Part3_Select
;-----------------------------------------------------------

[glink target="*Hensu_Shurui"  text="Learn&ensp;about「types&ensp;of&ensp;variables」  " x="50" y=" 50"]
[glink target="*Nakami_Shurui" text="Learn&ensp;about「types&ensp;of&ensp;content」  " x="50" y="150"]
[glink target="*Enzan"         text="Learn&ensp;about「mathematical&ensp;operations」  " x="50" y="250"]
[glink target="*Hikaku"        text="Learn&ensp;about「comparisons&ensp;and&ensp;conditions」  " x="50" y="350"]
[glink target="*Entity"        text="Learn&ensp;about「entities」" x="50" y="450"]
[glink target="*End"           text="End"                       x="190" y="550"]
[s]



;-----------------------------------------------------------
*Hensu_Shurui
;-----------------------------------------------------------

[image layer=1 storage=19_variable_A.png x=100 y=190 zindex=10 name=boxF time=1000 wait=false]
[image layer=1 storage=19_variable_B.png x=100 y=190 zindex=20 name=boxF time=1000 wait=false]
[image layer=1 storage=19_variable_C.png x=100 y=190 zindex=20 name=boxF time=1000]

In the last example, we used a variable named「f.xxx」.[p]
You may have wondered, 'what is f.xxx??'[p]
The 'xxx' part can be any name you like.[p]
The 'f.' shows what type of variable it is.[p]

[image layer=1 storage=19_variable_D.png x=000 y=0 zindex=20 name=boxSF time=1000]
[image layer=1 storage=19_variable_E.png x=270 y=0 zindex=20 name=boxTF time=1000]

There are 3 types of variable.[p]
「sf.」is a「system variable」,「f.」is a「game variable」,「tf.」is a temporary variable.[p]

[anim name=boxF  opacity=064 time=600]
[anim name=boxSF opacity=255 time=600]
[anim name=boxTF opacity=064 time=600]
[wa]

「System variables」are variables that are unique to an entire game.[p]
These variables are shared across any save data, such as config settings, etc.[p]

[anim name=boxF  opacity=255 time=600]
[anim name=boxSF opacity=064 time=600]
[anim name=boxTF opacity=064 time=600]
[wa]

「Game variables」are unique to each individual save data. You'll probably use this type most of the time.[p]

[anim name=boxF  opacity=064 time=600]
[anim name=boxSF opacity=064 time=600]
[anim name=boxTF opacity=255 time=600]
[wa]

「Temporary variables」are variables which are only for temporary use.[p]
These variables are only saved temrporarily, so it's best not to use them for much longer than a single click.[p]

[freeimage layer=1 time=1000]

On the subject of variable names... you can use pretty much whatever you like in the place of「xxx」.[p]
But there are a few rules.[p]

Rule 1: Variable names must be made up of single or double byte characters or '_'.[p]
However, some environments don't play nicely with double byte characters, so single byte and '_' are recommended.[p]
Some valid examples would be:「f.akane_love」,「f.playerName」,or「f.lucky7」.[p]

Rule 2: No symbols or spaces can be used other than '_'.[p]
For example,「f.x/y」or「f.x y」 would be no good.[p]

Rule 3: The first character cannot be a number.[p]
So「f.2xx」would be no good.[p]
Those are the rules.[p]

[image layer=1 storage=color/white.png x=0 y=130 width=960 height=250 name=bg]
[anim  name=bg opacity=100 time=0]
[ptext layer=1 color=black edge=white text="OK:&ensp;Single&ensp;or&ensp;double&ensp;byte&ensp;characters&ensp;or&ensp;underscore." x=30 y=160 size=25]
[ptext layer=1 color=black edge=white text='NG:&ensp;Any&ensp;symbols&ensp;other&ensp;than&ensp;underscore&ensp;or&ensp;space.' x=30 y=230 size=25]
[ptext layer=1 color=black edge=white text='Exception:&ensp;Names&ensp;cannot&ensp;begin&ensp;with&ensp;a&ensp;number.'  x=30 y=300 size=25]

That covers the variable types and name rules.[p]

[freeimage layer=1 time=1000]
[jump target="*Part3_Select"]



;-----------------------------------------------------------
*Nakami_Shurui
;-----------------------------------------------------------

Variables can store various types of content... [p]

[image layer=1 storage=19_variable_F.png x=50 y=-80 time=600 name=typeA]

In the earlier example, we stored '100' in a variable named 「f.xxx」, right?[p]

[ptext layer=1 color=black edge=white text=Numbers   x=340 y=060 size=50 name=typeA]

In this case, the '100' is a number.[p]
...[p]
Uh, well of course it is.[p]
But there are other types of content besides numbers...[p]

[image layer=1 storage=19_variable_H.png x=50 y=070 time=600 wait=false name=typeB]
[ptext layer=1 color=black edge=white text="Text&ensp;Strings" x=340 y=210 size=50 time=600 name=typeB]

[image layer=1 storage=19_variable_I.png x=50 y=220 time=600 wait=false name=typeC]
[ptext layer=1 color=black edge=white text="True&ensp;or&ensp;False"   x=340 y=360 size=50 time=600 name=typeC]

The most common type of value are 'text strings' or 'true or false'.[p]
There are also 'arrays' and 'objects', but those are advanced types.[p]
Basically, it's best to get the hang of 'numbers', 'text strings', and 'true or false'.[p]

[anim name=typeA opacity=255 time=600]
[anim name=typeB opacity=064 time=600]
[anim name=typeC opacity=064 time=600]
[wa]

You can manage 'things that go up and down' like friendliness using numbers.[p]
「0」,「999」,「-100」,and「3.14」are all examples of numbers.[p]

[anim name=typeA opacity=064 time=600]
[anim name=typeB opacity=255 time=600]
[anim name=typeC opacity=064 time=600]
[wa]

You can manage things like the 'hero's name' using text strings.[p]
When using text strings in JavaScript, they should be enclosed in 「'」.[p]
The 「'」s indicate that between them is a single piece of information.[p]
For example, to store the value 'Hanako' in the variable 「f.xxx」, you would write: f.xxx = 'Hanako'[p]

[anim name=typeA opacity=064 time=600]
[anim name=typeB opacity=064 time=600]
[anim name=typeC opacity=255 time=600]
[wa]

Finally...[p]
For binary states, such as whether a flag has been triggered or not, use 'true or false'.[p]
To set「f.xxx」to true, you would use「f.xxx = true」.[p]
To set「f.xxx」to false, you would use「f.xxx = false」.[p]


[anim name=typeA opacity=255 time=600]
[anim name=typeB opacity=255 time=600]
[anim name=typeC opacity=255 time=600]
[wa]

...[p]
These 3 types of variable can make creating your games much easier.[p]

[freeimage layer=1 time=1000]
[jump target="*Part3_Select"]



;-----------------------------------------------------------
*Enzan
;-----------------------------------------------------------

To add or subtract variables, we use functions called operations.[p]
For example, if you want to add 10 to the value stored in 「f.xxx」.[p]

[image layer=1 storage=19_variable_J.png x=190 y=260 time=600 name=typeA]

f.xxx = f.xxx + 10[p]

...[p]
That makes sense, right?[p]

[image layer=1 storage=19_variable_K.png x=190 y=260 time=600 name=typeA]

Let's start by looking at the right of the equal sign.[p]
Say we have the expression 「f.xxx + 10」...[p]
The value stored in f.xxx is currently 100...[l][r]

[image layer=1 storage=19_variable_L.png x=190 y=260 time=600 name=typeA]

So there result of「f.xxx + 10」is「110」.[p]

[image layer=1 storage=19_variable_M.png x=190 y=260 time=600 name=typeA]

If we recall the left side, the equation becomes「f.xxx = 110」.[p]
So the value in「f.xxx」is「110」.[p]
So the contents of「f.xxx」changed from「100」to「110」!![p]

...[p]
... ...[p]
Even if it doesn't make much sense right now, just go ahead and have a play with it.[p]

[image layer=1 storage=19_variable_J.png x=190 y=260 time=600 name=typeA]

By the way, you can write「f.xxx = f.xxx + 10」in another way.[p]

[anim name=typeA top=150 time=600]
[image layer=1 storage=19_variable_N.png x=190 y=300 time=600 name=typeB wait=false]

f.xxx += 10[wa][p]

This uses the special annotation「+=」.[p]
Both of these lines have the same effect and both add 10 to the value stored in f.xxx.[p]
...「+=」is a bit easier to follow, right?[p]
The basic form is「f.xxx = f.xxx + 10」, but it's probably best to bear both ways in mind.[p]

[freeimage layer=1 time=1000]

You can perform addition using「+」, but there are various other operations available.[p]

[image layer=1 storage=19_variable_O.png x=50 y=30 time=600 name=typeB wait=false]

Subtraction is「-」, multiplication is「*」, division is「/」, and remainder is「%」.[p]
A remainder calculation returns the remainder. For example,「10%3」 returns the remainder of 10 divided by 3.[p]
In this case it will be 1.[p]
Exponentials are [erfpe,performed with「**」. For example,「f.xxx cubed」 can be calculated using「f.xxx ** 3」.[p]
As with「+=」, you can write these operations as「-=」,「*=」,「/=」,「%=」,or 「**=」.[p]

[freeimage layer=1 time=1000]

...[p]
By the way, if you want to calculate one part of a formula before another, you can use 「()」 as with regular math.[p]
And that concludes our tutorial about mathematical operations![p]

[jump target="*Part3_Select"]



;-----------------------------------------------------------
*Hikaku
;-----------------------------------------------------------

There may be times when you want to check the condition of the contents of a variable. For example...[p]
―― ≪Is her friendliness level over 100?≫[p]
―― ≪Is his cooking level over 50?≫[p]
―― ≪Has the flag been triggered?≫[p]
―― ≪Is the password correct?≫[p]
To check these conditions, we use 'comparisons'.[p]
You can test if a condition is true or false by performing a comparison.[p]
The comparison will show if the condition is either true or false.[p]
Here's how we carry out comparisons.[p]

[image layer=1 storage=19_variable_P.png x=50 y=30 time=600 name=typeB wait=false]

For example, to check the condition「f.xxx is greater than 3」, you can write 「f.xxx > 3」.[p]
To check the condition「f.yyy is not equal to 10」, you can write「f.yyy != 10」.[p]
Etc, etc.[p]
You can also conjoin multiple conditions using 'and' or 'or'.[p]

[freeimage layer=1 time=1000]
[image layer=1 storage=19_variable_Q.png x=50 y=50 time=600 name=typeB wait=false]

「(1) and (2)」 means 「both (1) AND (2) are true」.[p]
「(1) or (2)」 means 「either (1) OR (2) are true」.[p]
So how would we check if「f.xxx is greater than 3 and f.yyy is not equal to 10」?[p]
We'd use「f.xxx > 3 && f.yyy != 10」.[p]

[freeimage layer=1 time=1000]
[jump target="*Part3_Select"]



;-----------------------------------------------------------
*Entity
;-----------------------------------------------------------

The contents of variables aren't only for displaying in messages![p]
You can also use variables as 'tag parameters'.[p]
These functions are known as 'entities'.[p]
[image layer=1 storage=19_variable_R.png x=50 y=250 time=600 name=typeB wait=false]
When we want to use a variable as a parameter, we add「&」to the start of the variable name.[p]
This way, the contents of the variable is passed to the tag as a parameter.[p]
Let's have a try at this.[p]
The variable f.xxx currently contains 100.[p]
Let's use this as the time parameter in a［bg］tag.[p]



; [bg] Looking at time="&f.xxx"
[bg storage="rouka.jpg" time="&f.xxx"]



How was that?[p]
100 miliseconds is just a blink of an eye.[p]
We can also carry out operations at the same time as passing parameters.[p]
[image layer=1 storage=19_variable_S.png x=50 y=250 time=600 name=typeB wait=false]
For example, we could pass the number stored in「f.xxx plus 1000」 as a parameter.[p]



; [bg] Looking at time="&f.xxx + 1000".
[bg storage="room.jpg" time="&f.xxx + 1000"]



... just like that.[p]
How it works is this: when you add「&」 to the start of the parameter...[p]
TyranoScript interprets the following characters as a JavaScript string.[p]
Put this to use in your games!![p]
[freeimage layer=1 time=1000]
[jump target="*Part3_Select"]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]