; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 15_input_1.ks =========================================
;
; This tutorial explains how to allow the player enter text in the game.
;
; ★Main tags introduced:
; [edit]   … Place an 'input field' on the free layer.
; [commit] … Copies the contents of the input fieldto a variable specified in [edit].

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Adventure games often have a point where the games asks the player to enter some text.[p][akn/hap]
For example, it might be the name of the player's character, or the hero's pet vulture.[p][akn/def]
You can use the［edit］and［commit］tags to add this functionality to your TyranoScript games.[p]
Let's look at a few examples using these tags.[p][akn/def]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

First, we'll have the player input the hero's name and then show that in a message.[p]
Okay, go ahead and enter your name here.[p]

; ------------------------------------------------------
*Part1_Edit

[layopt layer="message0" visible="false"]
[image layer="1" storage="15_input_A.png" x="0" y="0"]

; [edit] Place an input field.
[edit name="f.player_name" left="380" top="350" width="260" height="40" size="30" maxchars="20" initial="Hanako"]

[button graphic="config/arrow_next.png" target="*Part1_Commit" x="440" y="430"]
[s]

; ------------------------------------------------------
*Part1_Commit

; [commit] This flag moves the contents of [edit] to the variable f.player_name.
[commit]

; [cm] Release the free layer. The input field is locate on the free layer, so this removes the input field.
[cm]

[freeimage layer="1"]
[layopt layer="message0" visible="true"]
Your name is「

; [emb] Check the contents of the f.player_name variable.
[emb exp="f.player_name"]

」??[l][r]
[link target="*Part1_OK"  ]【1】That's right[endlink]
[link target="*Part1_Edit"]【2】Not really[endlink]
[s]

; ------------------------------------------------------
*Part1_OK

[cm]
[akn/hap]
All right!! Now you're a true TyranoScripter![p]
We're expecting BIG things from you, [emb exp="f.player_name"]![p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

[akn/def]
You can place multiple input fields at once. For example, you can have separate fields for the first name and last name.[p]

; ------------------------------------------------------
*Part2_Edit

[layopt layer="message0" visible="false"]
[image layer="1" storage="15_input_A.png" x="0" y="0"]

; [edit]x2 Place 2 input fields.
[edit name="f.player_fname" left="380" top="350" width="120" height="40" size="30" maxchars="4" initial="Hanako"]
[edit name="f.player_name"  left="510" top="350" width="120" height="40" size="30" maxchars="4" initial="Yamada"]

[button graphic="config/arrow_next.png" target="*Part2_Commit" x="440" y="430"]
[s]

; ------------------------------------------------------
*Part2_Commit

[commit]
[cm]
[freeimage layer="1"]
[layopt layer="message0" visible="true"]
So your name is 「[emb exp="f.player_fname"] [emb exp="f.player_name"]」, right?[l][r]
[link target="*Part2_OK"  ]【1】That's right[endlink]
[link target="*Part2_Edit"]【2】No[endlink]
[s]

; ------------------------------------------------------
*Part2_OK

[cm]

[akn/hap]
I see...[p][akn/def]
Should I call you by your first name or your family name?[l][r]

[link target="*Part2_Name" ]【1】Call me by my first name[endlink]
[link target="*Part2_Fname"]【2】My last name is fine[endlink]
[s]

; ------------------------------------------------------
*Part2_Fname

; I like your last name.

; [eval] Store the value 0 in the variable f.name_type
[eval exp="f.name_type = 0"]
[jump target="*Part2_End"]

; ------------------------------------------------------
*Part2_Name

; What a nice name.

; [eval] Store the value 1 in the variable f.name_type
[eval exp="f.name_type = 1"]
[jump target="*Part2_End"]

; ------------------------------------------------------
*Part2_End

[cm]

; [macro]～[endmacro]
; Define the macro [Hanako] that users [emb] to display the hero's name.
[macro name="Hanako"]

  ; [if]-[else]-[endif] Branches depending on the value stored in the variable f.name_type.
  [if exp="f.name_type == 0"]
    [emb exp="f.player_name"]
  [else]
    [emb exp="f.player_fname"]
  [endif]

[endmacro]

[akn/hap]
I see.[p]
Well, I'll call you '[Hanako]', then.[p][akn/def]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ------------------------------------------------------
*Part3_Edit

By the way...[p]
Do you have a favorite dinosaur, [Hanako]?[p]
You must have one. Everyone has a favorite dinosaur.[p]
Tell me what yours is.[p]

[layopt layer=message0 visible=false]
[image layer=1 storage=15_input_B.png x=0 y=0]
[edit name="f.like_dragon" left="310" top="300" width="330" height="60" size="40" initial="Tyranosaur"]
[button graphic="config/arrow_next.png" target="*Part3_Commit" x="660" y="295"]
[s]

; ------------------------------------------------------
*Part3_Commit

[commit]
[cm]
[freeimage layer=1]
[layopt layer=message0 visible=true]
You like [emb exp="f.like_dragon"]s?[p]

; [if]-[elsif]-[elsif]-[else]-[endif]
; Branch depending on the contents of f.like_dragon.

[if exp="f.like_dragon == 'Tyranosaur'"]
  [akn/hap]
  I knew it! I knew it would be a tyranosaur!![p]

[elsif exp="f.like_dragon == 'Pteranodon'"]
  [akn/hap]
  A pteranodon!! They are pretty cool!![p]

[elsif exp="f.like_dragon == 'Brontosaurus'"]
  [akn/hap]
  Brontosaurus!! The king of the long necked dinos![p]
  Not the smartest of dinos, but still very cool!![p]

[else]
  [akn/dok]
  Hm. Not really my cup of tea...[p]

[endif]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

[akn/def]
...[p]
And that's how you can use player text input in your games.[p][akn/def]
See you next time![p]

; [jump] Jump to the contents page.
[jump storage="select.ks"]