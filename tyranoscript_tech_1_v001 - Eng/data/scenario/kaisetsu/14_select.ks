; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 14_select.ks ==========================================
;
; The various ways to show selections.
;
; ★The main tags introduced:
; [link]   … Link the message between this and the [endlink] tag.
; [glink]  … Place a text object button.
; [button] … Place an image object button.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Selections are usually a key feature in visual novel games.[p][akn/hap]
Stories that branch based on the player's choices are so much fun![p][akn/def]
TyranoScript provides various kinds of selections to use in your games.[p]
Let's take a look at them in order.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

Old fashioned sound novels used lots of text links.[p]
These allow the player to click on text in the message window.[p]
... What would you have for breakfast?![r]

; [link]～[endlink]x2 Place 2 text links. Both jump to *Part2.
[link target="*Part2"]【1】Pancakes[endlink]
[link target="*Part2"]【2】Toast[endlink]
[s]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

[er]You can change the color of the link text.[p]
... Which is your preferred snack?![r]

; [font][link]～[endlink]x2 You can use teh [font] tag to set the link color.
[font color=red   edge=white][link target="*Part3"]【1】Popcorn[endlink]
[font color=green edge=white][link target="*Part3"]【2】Potato chips[endlink]
[s]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

[er]Alternatively, you can use text object for selections.[p]
... How do you like your afternoon tea??

; [glink] Place a text button object.
[glink x="320" y="200" width="200" text="With&ensp;milk" target="*Part4" color="white"]
[glink x="320" y="300" width="200" text="With&ensp;lemon" target="*Part4" color="orange"]
[s]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

[cm]You can place text buttons on the message layer.[p]
... If you were an insect, what kind would you be?

[glink x=" 50" y="560" width="300" text="A&ensp;cricket" target="*Part5" color="green" size=24]
[glink x="500" y="560" width="300" text="An&ensp;ant"       target="*Part5" color="black" size=24]
[s]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

[cm]You can also create selections using image buttons.[p]
Who would you go on a date with??

; [button] Place an image button object.
[button graphic="../fgimage/chara/mascot_1/normal.png" enterimg="../fgimage/chara/mascot_1/happy.png" x=70 y=160  target="*Part6"]
[button graphic="../fgimage/chara/mascot_2/normal.png" enterimg="../fgimage/chara/mascot_2/happy.png" x=600 y=140 target="*Part6"]
[s]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------

[cm]You can create all sorts of selections using these methods.[p][akn/hap]
Find out which ones work best for you!![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]