; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 12_anim.ks ============================================
;
; An overview of how to move objects.
;
; ★Main tags introduced:
; [anim]        … Move an object.
; [chara_move]  … Move a character object.
; [quake]       … Shake the screen.
; [kanim]       … Execute a defined key frame animation.
; [keyframe]    … Define the start of a key frame animation.
; [endkeyframe] … Define the end of a keyframe animation. 
; [frame]       … Define a key frame.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

TyranoScript has powerful animation functions.[p]
Any of the objects we've looked at so far can be animated![p]

; [image][ptext] Set a name paramter and place an object.
[image name=obj_image layer=1 x=150 y=200 storage=05_image_B.png]
[ptext name=obj_ptext layer=1 x=600 y=270 size=40 text=Text Object edge=black]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

Let's take a look at several animation-related tags, starting with［anim］.[p]

; ●
; ● Basic usage examples.
; ●

; [anim]x3 Animate a text object.
; Multiple [anim] tags that reference the same object will be executed in order, as opposed to simultaneously.
[anim  name=obj_ptext top=120 time=700]
[anim  name=obj_ptext top=420 time=700]
[anim  name=obj_ptext top=270 time=700]

; [wa] Wait for the animation to end.
[wa]

; [anim]x3 [wa]  Animate an image object.
[anim  name=obj_image width=300 time=700]
[anim  name=obj_image width=0   time=700]
[anim  name=obj_image width=200 time=700]
[wa]



; ●
; ● Relative usage example.
; ●

This tag usually uses absolute values for starting position and size, but...[p]
you can also use relative positions. See the tutorial project script for details.[p]

; [anim]x3 [wa] Relative settings can be used by setting parameters to values such as 「top="-=100"」.
; In these cases, parameters should be enclosed by 「"」 (quotation marks.)
; If quotation marks are not used (e.g. 「top=-=100」), the engine will  unbe able to interpret the tag correctly.
[anim  name=obj_image top="-=100" time=700]
[anim  name=obj_image top="+=200" time=700]
[anim  name=obj_image top="-=100" time=700]
[wa]



; ●
; ● An example of proceeding to the message during animation and without using [wa].
; ●

Moving on...[p]

; [anim]x6 An example of outputting a message after [anim] and without using [wa].
[anim  name=obj_image width=300 time=700]
[anim  name=obj_image width=0   time=700]
[anim  name=obj_image width=200 time=700]
[anim  name=obj_image top="-=100" time=700]
[anim  name=obj_image top="+=200" time=700]
[anim  name=obj_image top="-=100" time=700]

You can proceed through messages while animation is happening. Like this.[p]

; [wa] Place the [wa] here!
[wa]
[er]



; ●
; ● An example of how to select all objects beneath a layer.
; ●

We usually use the name parameter to specify which object to animate, but...[p]
you can also 'select all objects under a specified layer'.[p]

; [anim] Example of selecting which objects to animate using the layer parameter instead of the name parameter.
[anim  layer=1 top="-=200" opacity=0 time=700]
[wa]
[freeimage layer=1]

[akn/hap]
Check out the tag reference guide for more details about these kinds of tag.[p]



; ●
; ● An example pulling a offscreen object into the screen.
; ●

[akn/def]
You can place objects off-screen and then move them into the screen.[p]

[glink x=1320 y=100 width=200 text=Next target=*Part2 color=red name=glink1]
[glink x=1320 y=200 width=200 text=Next target=*Part2 color=red name=glink2]
[glink x=1320 y=300 width=200 text=Next target=*Part2 color=red name=glink3]

[anim name=glink1 time=300 left="-=1000"]
[wait time=100]
[anim name=glink2 time=300 left="-=1000"]
[wait time=100]
[anim name=glink3 time=300 left="-=1000"]
[s]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

There is a tag especially for animating character objects.[p][akn/dok]
And that is... the［chara_move］tag![p]
Use this tag to avoid having to move a character off-screen and re-enter in a new position.[p]

; ●
; ● Example of the character animation tag [chara_move].
; ●

; [chara_move] You can hide > move > show a character with this one tag.
[chara_move name=akane time=1000 left=40  top=40 width=600 ]
[chara_move name=akane time=1000 left=340 top=40           ]
[chara_move name=akane time=1000 left=280 top=40 width=400 ]
[wa]

[akn/def]
How was that?![p]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

[akn/def]
There is also a tag to 'shake the whole screen'.[p]
That is the［quake］tag and it looks like this in action.[p]

; [quake] Shake the screen.
[quake time=1000]

You can quake horizontally too.[p]

; [quake] Shake the screen horizontally.
[quake time=300 vmax=0 hmax=10 count=8]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

[akn/dok]
In addition to the［anim］tag, there is the［kanim］tag...[p]
You can use this to create complex animations using key frame animation![p][akn/def]
To use key frames, you first set 'how the object will move', then execute it.[p]
Let's see this in action.[p]

; [keyframe]～[endkeyframe] Set the key frame animation my_anim2.
[keyframe name=my_anim2]
  ; [frame] Set the animation to be 25% complete when it has moved 50px to the right.
  [frame p=25%  x=50]
  ; [frame] Set the animation to be 75% complete when it has moved 100px to the right.
  [frame p=75%  x=-100]
[endkeyframe]

; [kanim][wa] Execute the set key frame animation and wait for it to complete.
[kanim name=akane keyframe=my_anim2 time=700]
[wa]

; [stop_kanim] Stop the key frame animation.
[stop_kanim name=akane]

[akn/hap]
You can move objects like that.[p]
You can also set animations to loop a set amount of times.[p]

; [kanim] Example using the count parameter.
[kanim name=akane keyframe=my_anim2 time=700 count=3]
[wa]
[stop_kanim name=akane]

[akn/def]
［kanim］tag animation is smoother than the［anim］tag, and it can animate multiple objects at once.[p]

; [keyframe]～[endkeyframe] Set the key frame animation my_anim.
[keyframe name=my_anim]
  ; [frmae]x5 Set the scale/rotate/opacity parameters.
  [frame p=0%   scale=1   rotate=0deg   opacity=1]
  [frame p=25%                          opacity=0]
  [frame p=50%  scale=1.5               opacity=1]
  [frame p=75%            rotate=360deg          ]
  [frame p=100% scale=1                          ]
[endkeyframe]

[kanim name=akane keyframe=my_anim]
[wa]
[stop_kanim name=akane]

[akn/hap]
That concludes our introduction to animation![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]