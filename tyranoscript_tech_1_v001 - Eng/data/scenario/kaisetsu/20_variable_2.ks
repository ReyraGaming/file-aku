; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]


; == 20_variable_2.ks ======================================
;
; Branch the story depending on friendship level or flags.
;
; ★Main tags introduced:
; [iscript] … Run JavaScript.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

; [iscript]～[endscript] Inititialize the variables.
; Set the variable f.flag to false
; Store the value 0 in the variable f.favo_akane.
[iscript]
f.flag = false
f.favo_akane = 0
[endscript]






; ==========================================================
*P1

In this tutorial we'll see how to branch a story based on the player's choices or flag status!!![p]
Let's perform a short role play.[p][akn/hap]
And I'll be the president of the school Visual Novel Club.[p][akn/dok]
You can be... a student who wants to join the club.[p]
You came today to ask if you can join the club.[p]
KNOCK KNOCK. Ka-chik.[p][akn/def]
「Ah, hello. Are you here to ask about joining?」[p]
「My name is Akane. I'm the club president.」[p]
[glink color=blue size=30  x=210 width=400 y=150 target=*P1A text=「Pleased to meet you.」]
[glink color=blue size=30  x=210 width=400 y=250 target=*P1B text=「YOU're the president??」]
[s]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P1A

; [iscript]～[endscript] Akane's friendship level +100. Add 100 to the variable f.favo_akane.
[iscript]
f.favo_akane += 100
[endscript]

[akn/hap]
「Pleased to meet you too.」[p]
[jump target=*P2]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P1B

; [iscript]～[endscript] Akane's friendship level -200. Subtract 200 from the variable f.favo_akane.
[iscript]
f.favo_akane -= 200
[endscript]

[akn/dok]
「H-hunh.」[p]
「That wasn't very nice...」[p]
[jump target=*P2]






; ==========================================================
*P2
[akn/def]
「Well, let's talk about club, shall we?」[p]
「So you're interested in game development?」[l]
[glink color=blue size=30  x=210 width=400 y=150 target=*P2A text=「Yes, that's right」]
[glink color=blue size=30  x=210 width=400 y=250 target=*P2B text=「nope」]
[s]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P2A

; [iscript]～[endscript] Akane's friendship level +100. Add 100 to the variable f.favo_akane.
; Also raise the flag by storing true in the variable f.flag. (The default was false.)
[iscript]
f.favo_akane += 100
f.flag = true
[endscript]

「Oh, you do!」[p]
「Well that makes sense!! You did come here to join the club, after all.」[p]
[jump target=*P3]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P2B

[akn/sad]
「You... don't? Well.. okay, then.」[p]
「It takes all sorts to make a world.」[p]
[jump target=*P3]






; ==========================================================
*P3

[akn/def]
「Well, do you like visual novels? [l] I'm actually a fan of them...」
[glink color=blue size=30  x=210 width=400 y=100 target=*P3A text=「I like them]
[glink color=blue size=30  x=210 width=400 y=200 target=*P3B text=「I hate them」]
[glink color=blue size=30  x=210 width=400 y=300 target=*P3C text=「I've never played one」]
[s]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P3A

; [iscript]～[endscript] Akane's friendship level +100. Add 100 to the variable f.favo_akane.
[iscript]
f.favo_akane += 100
[endscript]

[akn/hap]
「Really? That's great!」[p]
[jump target=*P4]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P3B

; [iscript]～[endscript] Akane's friendship level -100. Subtract 100 from the variable f.favo_akane.
[iscript]
f.favo_akane -= 100
[endscript]

[akn/dok]
「What the...」[p]
「Well, there's no accounting for taste.」[p]
[jump target=*P4]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P3C

; [iscript]～[endscript] Akane's friendship level +100. Add 100 to the variable f.favo_akane.
[iscript]
f.favo_akane += 100
[endscript]

「I see.」[p]
「Well, this would be a great chance to get to know them!!」[p]
[jump target=*P4]






; ==========================================================
*P4

「…………」[p]

; [jump]x4 Branch the endings here
; The cond parameter is the main point.
; cond is short for 'condition'
; The tag will execute only if the content of the cond parameter is true.
[jump target=*P4A cond="f.favo_akane >=  200 && f.flag"]
[jump target=*P4B cond="f.favo_akane >=  200"]
[jump target=*P4C cond="f.favo_akane >=    0"]
[jump target=*P4D]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P4A

[akn/ang]
It's decided, then!![p]
[akn/hap]
You!![p]
You'll make a visual novel game with me!!!![p]
～Ending 1～[p]
Condition: You friendship level was over 200 and 'the interest in game development' flag was raised.[p]
[jump target=*End]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P4B

「Well, why don't you try playing a game that I made.」[p]
「I think you'll really enjoy it!!」[p]
～Ending 2～[p]
Condition: The conditions for Ending 1 are not met, but the friendship is over 200[p]
[jump target=*End]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P4C

[akn/dok]
「Well, let's wrap things up here.」[p]
「I'm sure there are other clubs for you to look at. Goodbye!」[p]
～Ending 3～[p]
Condition:  The conditions for Endings 1 & 2 are not met, but the friendship level is over 0[p]
[jump target=*End]

; ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---
*P4D

[akn/sad]
「... I don't think this club is for you.」[p]
「I think it would be best for both of us if you joined another club.」[p]
～Ending 4～[p]
Condition:  The conditions for Endings 1-3 are not met[p]
[jump target=*End]






;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]