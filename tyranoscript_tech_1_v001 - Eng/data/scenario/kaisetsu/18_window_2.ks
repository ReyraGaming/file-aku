; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]


; == 18_window_2.ks ========================================
;
; This tutorial covers how to place several message windows and
; place characters over the windows.
;
; ★Main tags introduced:
; [current] … Change the message output layer.
; [er] … Clear the messages.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

; [chara_new][chara_face]x4 Define Akane who will go on top of the message window.
[chara_new  name="s_akane"  jname="あかね" storage="./chara/s_akane/normal.png"]
[chara_face name="s_akane"   face="angry"  storage="chara/s_akane/angry.png"   ]
[chara_face name="s_akane"   face="doki"   storage="chara/s_akane/doki.png"    ]
[chara_face name="s_akane"   face="happy"  storage="chara/s_akane/happy.png"   ]
[chara_face name="s_akane"   face="sad"    storage="chara/s_akane/sad.png"     ]

; [chara_new][chara_face]x4 Define Yamato who will go on top of the message window.
[chara_new  name="s_yamato" jname="やまと" storage="chara/s_yamato/normal.png" ]
[chara_face name="s_yamato"  face="angry"  storage="chara/s_yamato/angry.png"  ]
[chara_face name="s_yamato"  face="happy"  storage="chara/s_yamato/happy.png"  ]
[chara_face name="s_yamato"  face="sad"    storage="chara/s_yamato/sad.png"    ]
[chara_face name="s_yamato"  face="tohoho" storage="chara/s_yamato/tohoho.png" ]

Let's look at more advanced ways to customize message windows![p]
You can add character portraits to message windows and have multiple message windows on-screen.[p]

First, let's switch the message window design to something else...[p]

; [clearfix][free] Remove the role buttons and character name window.
[clearfix]
[free  name="chara_name_area"  layer="message0"]

; [deffont][resetfont] Set and apply the default font.
[deffont size="32" face="logo type Gothic, Meiryo, sans-serif" color="0xffffff" edge="0x000000"]
[resetfont]

; [position] Configure the message window settings.
[position layer="message0" left="  0" top="340" width="960" height="260" frame="window7/_frame.png" margint="52" marginl="300" marginr="90" marginb="10" opacity="210"]

; [ptext] Place the name.
[ptext    layer="message0"    x="100"   y="350" text="Akane" zindex="102" size="32" face="logo type Gothic, Meiryo, sans-serif"color="0xffffff" edge="0x000000" name="chara_name"]

There we go.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

Using character portraits on messages windows is easy.[p]
When you place the portrait, set the output layer to 'message layer'.[p]

; [chara_hide] Remove Akane's portrait.
[chara_hide name="akane"]

; [chara_show] Place Akane's face on the message layer.
[chara_show name="s_akane" layer="message0" left="100" top="400"]

Ah! The order is wrong! My face is behind the message window![p]
To avoid this, always set the zindex when you place the portrait.[p]

; [chara_hide] Remove Akane's face from the message layer.
[chara_hide name="s_akane" layer="message0"]

; [chara_show] Set the zindex paramter and place Akane's face on the message layer.
[chara_show name="s_akane" layer="message0" left="100" top="400" zindex="101"]

[chara_mod  name="s_akane" face="happy"]
That's better!![p]
[chara_mod  name="s_akane" face="default"]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

Now let's add another message window.[p]
This example uses message layers 0 and 1.[p]
The current message layer is 0, so we'll use 1, which is empty.[p]

; [position][ptext][chara_show][layopt] Message window settings > Place the name > Place the face > Display the layer.
[position   layer="message1" left="  0" top=" 40" width="960" height="260" frame="window7/_frame.png" margint="52" marginl="300" marginr="90" marginb="10" opacity="210"]
[ptext      layer="message1"    x="100"   y=" 50" text="Yamato" zindex="102" size="32" face="logo type Gothic, Meiryo, sans-serif"color="0xffffff" edge="0x000000" name="chara_name"]
[chara_show layer="message1" left="100" top="100" name="s_yamato" time="0" zindex="101"]
[layopt layer=message1 visible=true]

Ta-dah.[p]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

This is just a simple example, though...[p]
You can use the［current］or［er］tags to change the output message layer.[p]

; Define the macro [mes0].
; Using [mes0] will prepare message layer 0 for use.
[macro name="mes0"]
[current layer="message0"]
[er]
[endmacro]

; Define the macro [mes1].
[macro name="mes1"]
[current layer="message1"]
[er]
[endmacro]

[mes0]
For example, like this...[l][er]
Hey there, Yamato.[l]

[mes1]
[chara_mod  name="s_yamato" face="tohoho" time="0"]
Hi there.[l][er]
What are you doing here?[l]

[mes0]
[chara_mod  name="s_akane" face="happy" time="0"]
I'm explaining how to customize message layers!![l]

[mes1]
[chara_mod  name="s_yamato" face="default" time="0"]
Oh, I see.[l][er]
Can I go now?[l]

[mes0]
[chara_mod  name="s_akane" face="default" time="0"]
Uh... sure.[l][er]

; [chara_hide][free][layopt] Release the objects on message layer 1.
[chara_hide layer="message1" name="s_yamato" time="0"]
[free       layer="message1" name="chara_name"]
[layopt     layer="message1" visible="false"]

And that pretty well covers it!![p]

; [chara_hide][free] Release the objects on message layer 0.
[chara_hide layer="message0" name="s_akane" time="0"]
[free       layer="message0" name="chara_name"]


;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]