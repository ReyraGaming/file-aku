; [set_default_view] This macro creates the tutorial screen. (See 'macro.ks' for details.)
[set_default_view]

; [akn/def] A standalone macro. It enteres 'Akane' to the name box and changes Akane's expression. It is defined in 'macro.ks'.
[akn/def]



; == 02_decotext.ks ========================================
;
; Let's see how to format text.
;
; ★Main Tags in This Tutorial:
; [font]       … Used to adjust the text settings (font / color / size / storke / shadow)
; [deffont]    … Set the default font settings.
; [resetfont]  … Apply the default font settings.
; [ruby]       … Insert Ruby.
; [graph]      … Insert an image inline.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's take a look at how to format text.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ★
; ★Temporarily change the text formatting.
; ★

You can temporarily change the message text format by using the［font］tag.[p]
You can use this tag to:

; [font]x7 Change various font settings.
[font bold="true"                  ] make text bold,[l]
[font bold="false" color="0x00FFFF"] change the text color,[l]
[font color="0xFFFFFF" size="40"   ] change the text size,[l]
[font face="Impact" size="25"      ] and change the font face.[p]

[resetfont]In later  versions of TyranoScript,
[font edge="0xFF0000" shadow="none"] you can add strokes,[l]
[font edge="none" shadow="0xFF0000"] and shadows, too!![p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; ★
; ★Set and apply the default font formatting.
; ★

This might be a little hard to read?[p]
Let's go back to the original settings.[p]
To revert to original text settings, use the［resetfont］tag, or the［er］or［cm］tags. [p]

; [resetfont] Apply the default font settings.
[resetfont]

That's better.[p]
You can also change the default font settings, which will change the font settings for the entire game.[p]
To do this, use the［deffont］tag instead of the［font］tag.[p]

; [deffont][resetfont] Change and apply the default font settings.
[deffont size="30"]
[resetfont]

After adjusting the default font settings with this tag, you can apply the changes by using the［resetfont］tag.[p]
The new settings will be used throughout the game.[p]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ★
; ★Using Ruby. This is usually used for pronunciation guides for languages such as Chinese or Japanese, so we'll skip this in the English tutorial.
; ★

;Let's take a look at how to use Ruby.[p]
;In TyranoScript,
;
; [ruby]x4 Apply Ruby to the following characters.
;[ruby text="This"]you 
;[ruby text="is"]can 
;[ruby text="usually"]add Ruby 
;[ruby text="used"]to 
;[ruby text="for"]characters 
;[ruby text="pronunciation"]as you 
;[ruby text="guides"]as you like.[p]

;Please note: Ruby must be applied one character at a time.[p]
;Also, depending upon the font face, the spacing between characters may change.[p]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

; ★
; ★Text display speed.
; ★

You can also temporarily change the speed at which text is displayed. 

; [delay] Change the  display speed to 500ms (0.5 seconds) per character.
[delay speed="400"]

Like this. 

; [nowait] Display text instantly.
[nowait]

Or display text instantly.[p]

; [endnowait] End nowait.
[endnowait]

; [resetdelay] Return to the default text display speed.
[resetdelay]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

; ★
; ★Insert an image in-line.
; ★

So then![p]
As we saw, there are all sorts of ways to format text.[p]
You can also insert images into the text using the［graph］tag.[p]
You might use this to insert emoji like this...

; [graph] Display an image in-line.
[graph storage="02_decotext_A.png"]

[p]or you could insert symbols or images like 

; [graph]x3 Display the images in-line.
[graph storage="02_decotext_B.png"]
[graph storage="02_decotext_C.png"]
[graph storage="02_decotext_D.png"]

[p][akn/hap]
I'm sure you'll find many uses for this![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]