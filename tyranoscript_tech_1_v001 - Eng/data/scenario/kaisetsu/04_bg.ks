; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 04_background.ks ======================================
;
; Here we look at how to manage the background.
;
; ★Main tags used:
; [bg] … Draws or switches the background.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Setting background images in TyranoScript is easy peasy!![p]
The only tag you need is［bg］.[p]
Let's change the classroom background to something else.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; [bg] Set a background image. (If a background is already set, it will switch to the newly designated background.)
; By default it will fade in over 1000ms.
[bg storage="rouka.jpg"]

Now we're the corridor.[p]

; [bg] 20ms。
[bg storage="room.jpg" time="200"]

And just like that, we're back in the classroom![p]

; [bg] vanish in over 2000ms.
[bg storage="mori.jpg" time="2000" method="vanishIn"]

Now we're in a forest.[p]

; [bg] rotate in over 1000ms.
[bg storage="eki.jpg" time="1000" method="rotateInDownRight"]

We rotated to the train station![p]

; [bg] Return to the classroom.
[bg storage="room.jpg"]

And so on, and so forth...[p]
There are many transition effects available to use when changing backgrounds.[p]
For details, see the TyranoScript tag reference.[p]
We'll also cover them in the TyranoScript Technical Sample Collection Vol 2!![p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

Now let's have a chat about layers.[p]
You might be wondering which layer the［bg］tag uses to display backgrounds...[p]
The answer is: the base layer.[p]
The base layer is located at the very back of all other layers.[p]
As a rule, it shouldn't be used to display anything other than backgrounds.[p]
Just consider the base layer to be 'for background use only!'[p][akn/hap]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

Let's see some examples of what else we can do...[p]
Here are a couple of nifty ways to change the background.[p]
You can change the background while temporarily hiding the the game screen.[akn/def][p]
Let's start with a vortex effect.[p]

; [chara_hide_all] Hide all characters.
[chara_hide_all time="0"]

; [layopt]x2 Hide message layer 0 and the fix layer. (The objects will still remain visible.)
[layopt layer="message0" visible="false"]
[layopt layer="fix"      visible="false"]

; [image]x2 Use [wait] to add a delay interval between showing two black images and hide the game screen.
[image layer="1" x="0"  y="0" width="960" height="640" storage="04_bg.png"       name="maskA" time="1000" wait="false"]
[wait time="200"]
[image layer="1" x="0"  y="0" width="960" height="640" storage="color/black.png" name="maskB" time="1000"]

; [bg] Instantly change the background.
[bg time="0" storage="mori.jpg"]

; [free]x2 Remove the two black images that are hiding the screen.
[free layer="1" name="maskB" time="1000" wait="false"] [wait time="200"]
[free layer="1" name="maskA" time="1000"]

; [chara_show] Show Akane.
[chara_show name="akane"]

; [layopt]x2 Unhide message layer 0 and the fix layer.
[layopt layer="message0" visible="true"]
[layopt layer="fix"      visible="true"]

We did that using the［image］and［free］tags.[p]
Here's another example: blocking out the screen.[p]

; [keyframe]～[endkeyframe]x8 Define key frame animation.
[keyframe name="slideInA"]
  [frame p="  0%" rotate="-40deg" x="-800"]
  [frame p=" 25%" rotate="-40deg" x="0"]
  [frame p="100%" rotate="-40deg" x="0"]
[endkeyframe]
[keyframe name="slideInB"]
  [frame p="  0%" rotate="-80deg" x="800"]
  [frame p=" 15%" rotate="-80deg" x="800"]
  [frame p=" 50%" rotate="-80deg" x="0"]
  [frame p="100%" rotate="-80deg" x="0"]
[endkeyframe]
[keyframe name="slideInC"]
  [frame p="  0%" rotate="-35deg" x="-1500"]
  [frame p=" 35%" rotate="-35deg" x="-1500"]
  [frame p=" 75%" rotate="-35deg" x="0"]
  [frame p="100%" rotate="-35deg" x="0"]
[endkeyframe]
[keyframe name="slideInD"]
  [frame p="  0%" rotate="-80deg" x="1500"]
  [frame p=" 50%" rotate="-80deg" x="1500"]
  [frame p="100%" rotate="-80deg" x="0"]
[endkeyframe]
[keyframe name="slideOutA"]
  [frame p="  0%" rotate="-40deg" x="0"]
  [frame p=" 50%" rotate="-40deg" x="0"]
  [frame p="100%" rotate="-40deg" x="800"]
[endkeyframe]
[keyframe name="slideOutB"]
  [frame p="  0%" rotate="-80deg" x="0"]
  [frame p=" 35%" rotate="-80deg" x="0"]
  [frame p=" 75%" rotate="-80deg" x="-1200"]
  [frame p="100%" rotate="-80deg" x="-1200"]
[endkeyframe]
[keyframe name="slideOutC"]
  [frame p="  0%" rotate="-35deg" x="0"]
  [frame p=" 15%" rotate="-35deg" x="0"]
  [frame p=" 50%" rotate="-35deg" x="1500"]
  [frame p="100%" rotate="-35deg" x="1500"]
[endkeyframe]
[keyframe name="slideOutD"]
  [frame p="  0%" rotate="-80deg" x="0"]
  [frame p=" 25%" rotate="-80deg" x="-1500"]
  [frame p="100%" rotate="-80deg" x="-1500"]
[endkeyframe]

[chara_hide_all time="0"]
[layopt layer="message0" visible="false"]
[layopt layer="fix"      visible="false"]

; [image]x4 [kanim]x4 [wa] Show the belt → Apply the appearance key frames → Await completion.
[image layer="1" x="-350"  y="-100" width=" 800" height="400" storage="color/col1.png" name="a" zindex="4"]
[image layer="1" x="-250"  y="   0" width="1000" height="440" storage="color/col2.png" name="b" zindex="3"]
[image layer="1" x="-250"  y="   0" width="1400" height="600" storage="color/col3.png" name="c" zindex="2"]
[image layer="1" x="  30"  y="   0" width="1400" height="600" storage="color/col4.png" name="d" zindex="1"]
[kanim keyframe="slideInA" name="a" time="1500"]
[kanim keyframe="slideInB" name="b" time="1500"]
[kanim keyframe="slideInC" name="c" time="1500"]
[kanim keyframe="slideInD" name="d" time="1500"]
[wa]

; [bg] Instantly change the background.
[bg time="0" storage="eki.jpg"]

; [kanim]x4 [wa] [freeimage] Use the exit key frame animation → Await completion → Release foreground layer 1.
[kanim keyframe="slideOutA" name="a" time="1500"]
[kanim keyframe="slideOutB" name="b" time="1500"]
[kanim keyframe="slideOutC" name="c" time="1500"]
[kanim keyframe="slideOutD" name="d" time="1500"]
[wa]
[freeimage layer="1"]

[chara_show name="akane"]
[layopt layer="message0" visible="true"]
[layopt layer="fix"      visible="true"]
That's one way to use key frame animation.[p]
It takes a little work to put together effects like this, but once it's done you can easily re-use them.[p]
Try coming up with your own unique effects![p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]