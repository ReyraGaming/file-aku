; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 13_clickable.ks =======================================
;
; This tutorial explains how to create a clickable area.
;
; ★Main tags introduced:
; [clickable] … Create a clickable area.
; 
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Do you know about the［clickable］tag?[p]
You can use it to easily create clickable areas.[p]
If the player clicks on one, the game will jump to a pre-determined label.[p][akn/hap]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------


Let's give it a try.[p][akn/def]
There's a clickable map on my face right now. Go ahead and click on it.

; [clickable] Create a clickable area that will jump to "*Part2" when clicked.
[clickable x="373" y="56" width="250" height="233" target="*Part2" opacity="0" mouseopacity="50" color="0xffffff"]

; [s] Stop the game.
[s]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

[akn/hap]
You clicked my face!![p][akn/def]
You can place multiple clickable areas...[p]
You can also adjust the frame designs and opacity as you like.[p]
Let's take a look of a few examples.[p]
I pasted some clickable areas on me.[r]There are 4 in all!!

; [clickable]x4 A few examples of clickable maps and designs.
[clickable x="432" y="148" width="114" height="134" target="*Pos_1" opacity=" 64" mouseopacity="128" border="5px:dotted:black" color="0xffffff     "]
[clickable x="530" y="141" width=" 56" height=" 55" target="*Pos_2" opacity="128" mouseopacity="255" border="5px:outset:white" color="rgba(0,0,0,0)"]
[clickable x="431" y="303" width="101" height=" 96" target="*Pos_3" opacity=" 64" mouseopacity="128" border="5px:groove:white" color="lightblue    "]
[clickable x="374" y=" 48" width="123" height="103" target="*Pos_4" opacity=" 64" mouseopacity="128" border="1px:solid :black" color="pink         "]

; ※ Side Note ※ 
; You can set the border parameter like the CSS border property.
; (In this case, you need to use colons instead of spaces.)
; Have a look into CSS borders for more details.

; [s] Stop the game.
[s]

; Below are several information labels.
; All of them will display a simple message and then jump to the "*Part3" label.

;-------------------------------------------------------
*Pos_1
...[p]
Is there something on my face?[p]
[jump target="*Part3"]

;-------------------------------------------------------
*Pos_2
That's my hairpin.[p]
Isn't it cute![p]
[jump target="*Part3"]

;-------------------------------------------------------
*Pos_3
You like my ribbon?[p]
You must like the color red.[p]
[jump target="*Part3"]

;-------------------------------------------------------
*Pos_4
My hair looks a little messy, doesn't it?[p]
That's on purpose!![p]
[jump target="*Part3"]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

You can use clickable maps in a variety of ways.[p]
For example, you can have the player explore the background.[p]
We see this a lot in escape games and adventure games.[p]
Let's have a go at this. Try clicking different items in the background![p]
When you want to finish, click the 'Back' button at the top right.[p]

; [layopt] Hide message layer 0.
[layopt layer="message0" visible="false"]

; [clearfix] Release the roll button.
[clearfix]

; [position] Change the window background image.
[position frame="window0/_frame4.png"]

; [chara_hide_all] Remove all of the characters.
[chara_hide_all]

; [macro]～[endmacro]x2
; Define the macro [sm] which shows message layer 0. (sm is short for show message)
[macro name="sm"]
  [layopt layer="message0" visible="true"]
[endmacro]

; Define the macro [jc] which jumps to *Set_Clickable_Map. (jc is short for jump clickable)
[macro name="jc"]
  ; [clearstack] Clear the stack.
  [clearstack]
  [jump target="*Set_Clickable_Map"]
[endmacro]



;-----------------------------------------------------------
*Set_Clickable_Map
;-----------------------------------------------------------

; ●
; ●Label to display clickable maps.
; ●

; [layopt] Hide message layer 0.
[layopt layer="message0" visible="false"]

; [button] Show the back button.
[button graphic="config/c_btn_back.png" enterimg="config/c_btn_back2.png" target="*Part4" x="860" y="10"]

; [clickable]x18 Place clickable maps.
[clickable  1 Clock       x="561" y="150" width=" 46" height=" 53" target="*Pos_01" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  2 Speaker x="440" y="149" width=" 54" height=" 42" target="*Pos_02" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  3 Blackboard       x="267" y="210" width="348" height=" 91" target="*Pos_03" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  4 Blackboard shelf     x="426" y="299" width=" 38" height=" 14" target="*Pos_04" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  5 Board eraser   x="280" y="286" width=" 47" height=" 13" target="*Pos_05" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  6 Desk       x="418" y="310" width="104" height=" 97" target="*Pos_06" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  7 TV     x="167" y="200" width=" 90" height=" 58" target="*Pos_07" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  8 Screen x="114" y=" 75" width="152" height=" 49" target="*Pos_08" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable  9 Classroom library   x="116" y="261" width="134" height=" 62" target="*Pos_09" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 10 Noticeboard     x="618" y="210" width=" 65" height=" 91" target="*Pos_10" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 11 Door       x="785" y="230" width="116" height="185" target="*Pos_11" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 12 Right window   x="783" y="143" width="176" height=" 76" target="*Pos_12" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 13 Left window   x=" 12" y=" 55" width=" 45" height="305" target="*Pos_13" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 14 Curtain   x=" 60" y=" 89" width=" 15" height="270" target="*Pos_14" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 15 Cupboard   x="121" y="327" width="128" height=" 98" target="*Pos_15" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 16 Fluorescent light     x="190" y="  2" width="656" height=" 74" target="*Pos_16" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 17 Desk         x="  4" y="426" width="943" height="203" target="*Pos_17" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]
[clickable 18 Switch   x="739" y="266" width=" 28" height=" 48" target="*Pos_18" opacity="0" mouseopacity="50" border="1px:dashed:black" color="0xffffff"]

; [s] Stop the game.
[s]

; ●
; ●Below are the destination labels for the clickable maps.
; ●

;-------------------------------------------------------
*Pos_01
[sm]
It's a clock, but there are no hands.[p]
The current time is... 7:13am.[p]
[jc]

;-------------------------------------------------------
*Pos_02
[sm]
The speakers are put away.[p]
They don't make a sound.[p]
[jc]

;-------------------------------------------------------
*Pos_03
[sm]
There is an empty blackboard.[p]
Someone did a good job of cleaning it.[p]
[jc]

;-------------------------------------------------------
*Pos_04
[sm]
It's the blackboard shelf.[p]
There isn't a spot of chalk  dust on it right now.[p]
[jc]

;-------------------------------------------------------
*Pos_05
[sm]
It's the blackboard eraser.[p]
It's as clean as the blackboard.[p]
[jc]

;-------------------------------------------------------
*Pos_06
[sm]
That's the teacher's lecturn.[p]
!? There's a teacher's lecturn...[wait time="1500"] but no teacher.[p]
[jc]

;-------------------------------------------------------
*Pos_07
[sm]
That is one old school TV.[p]
Just look at how big it is.[p]
[jc]

;-------------------------------------------------------
*Pos_08
[sm]
The projector screen is rolled up.[p]
We don't use it that often.[p]
[jc]

;-------------------------------------------------------
*Pos_09
[sm]
That's the classroom library.[p]
It's mostly dictonaries and textbooks.[p]
[jc]

;-------------------------------------------------------
*Pos_10
[sm]
It's a noticeboard.[p]
Oh...[p]
It says "This week is school cleaning week! Let's all do our bit and[r] clean the school grounds!!"[p]
[jc]

;-------------------------------------------------------
*Pos_11
[sm]
The door is closed.[p]
...?[p]
It won't budge.[p]
[jc]

;-------------------------------------------------------
*Pos_12
[sm]
There's a small window.[p]
... there's no way I could reach that.[p]
[jc]

;-------------------------------------------------------
*Pos_13
[sm]
The weather outside is beautiful.[p]
[jc]

;-------------------------------------------------------
*Pos_14
[sm]
The curtains are open.[p]
[jc]

;-------------------------------------------------------
*Pos_15
[sm]
That's the class cupboard.[p]
We keep our drawing and writing utentsils in there.[p]
[jc]

;-------------------------------------------------------
*Pos_16
[sm]
The classroom lights are turned off right now.[p]
[jc]

;-------------------------------------------------------
*Pos_17
[sm]
The class desks and chairs are neatly lined up.[p]
[jc]

;-------------------------------------------------------
*Pos_18
[sm]
That's the light switch.[p]
It's in the off position right now.[p]
[jc]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

[cm]
[position frame="window0/_frame.png"]
[sm]
[chara_show name="akane"]
Ready to move on?[p]
[chara_mod name="akane" face="happy"]
Isn't that tag great!![p]
[jump storage="select.ks"]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]