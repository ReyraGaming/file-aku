; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 17_window_1.ks ========================================
;
; Tutorial about customizing the message window.

; ★Main tags introduced:
; [position]        … Adjust message window settings (background color, background image, inner margin / position / size / text alignment)
; [button role=...] … Place a role button.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

; [akn/def] A macro to change Akane's expression.
[akn/def]
In TyranoScript, you can the［position］tag to easily customize the message window.[p]
You can set a background color or image, set the size or position, inner margins, and so on.[p]

[akn/hap]
Let's see some examples.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ★
; ★Remove the role buttons.
; ★

[akn/dok]
Whoa...[p]
Let's remove the role buttons first.[p]
[akn/def]
Role buttons are buttons like those at the bottom of the screen that are used to save or load the game, etc.[p]
Let's remove them for now.[p]

; [clearfix] This tag removes all objects on the fix layer.
; All types of role buttons such as save or load are displayed on the fix layer, so
; we'll use that tag to clear them all.
[clearfix]

[akn/def]
Okay, they're gone.[p]



; ★
; ★Remove the character name window.
; ★

Let's also clear the character name.[p]

; The [free] tag removes objects with a particular name from a specified layer.
; Remove the object chara_name_area from message layer 0.
[free name="chara_name_area" layer="message0"]

And it's gone.[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; ★
; ★Set the message window.
; ★

Okay, now we're ready. Let's set the message window.[p]

; [position]x3 Configure the message window settings.
; 1) The background image is disabled. Background color is black. Opacity is 128 (255 is completely opaque.)
; 2) The width is 800px, height is 240px. The position is 240px, position is 70px from the left and 380px from the top.
; 3) The upper, left, lower, and right margins are 25-70-60-50px respectively.
[position frame="none" color="0x000000" opacity="128"]
[position width="800" height="240" left="70" top="380"]
[position margint="25" marginr="25" marginb="25" marginl="25"]

; ※ Side Note ※
; The settings can be configured in a single [position] tag,
; but they have been split into 3 here for ease of reading.

; ※ Side Note ※
; If the layer parameter is not specified, the settings will be applied to message layer 0.

Done!![p]
And there we have a very simple, slightly transparent black window.[p]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ★
; ★Adjust various message window settings.
; ★

Let's make a few more changes...[l][r]

; ※ Please Note ※
; Using the [position] tag resets the innert margins.
; Any time you change the window settings using the [position] tag,
; please remember to set the inner margins.

[position opacity="255"]
 [position margint="25" marginr="25" marginb="25" marginl="25"]

We can change the opacity from 128 to 255...[l][r]

[position color="0x0000cc"]
 [position margint="25" marginr="25" marginb="25" marginl="25"]

Change the background color from black to blue...[l][r]

[position width="800" height="540" left="80" top="50"]
 [position margint="25" marginr="25" marginb="25" marginl="25"]

Change the window position and size, and the inner margins...[l][r]

[position vertical="true"]
 [position margint="25" marginr="25" marginb="25" marginl="25"]

And change the window text alignment.[p]
That's... a little hard to read, so let's put it back how it was.[p]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

[position vertical="false"]
[position left="0" top="440" width="960" height="200"]
 [position margint="50" marginl="25" marginr="25" marginb="10"]

This is the original position and inner margins.[p]
Now let's add a background image.[p]

[position frame="window0/_frame.png"]
 [position margint="50" marginl="25" marginr="25" marginb="10"]

Ta-dah. Pretty neat, eh.[p]
Now let's add the text object where the character name will do and add the role button objects back.[p]
[ptext name="chara_name_area"  layer="message0" zindex="102" size="32" face="ロゴたいぷゴシック,メイリオ,sans-serif" x="36" y="445" color="0xffffff" edge="0x000000"]
[chara_config ptext="chara_name_area"]

[button name="role_button" role="skip"       graphic="window0/skip.png"   enterimg="window0/skip2.png"   x="& 0 * 80" y="615"]
[button name="role_button" role="auto"       graphic="window0/auto.png"   enterimg="window0/auto2.png"   x="& 1 * 80" y="615"]
[button name="role_button" role="save"       graphic="window0/save.png"   enterimg="window0/save2.png"   x="& 2 * 80" y="615"]
[button name="role_button" role="load"       graphic="window0/load.png"   enterimg="window0/load2.png"   x="& 3 * 80" y="615"]
[button name="role_button" role="quicksave"  graphic="window0/qsave.png"  enterimg="window0/qsave2.png"  x="& 4 * 80" y="615"]
[button name="role_button" role="quickload"  graphic="window0/qload.png"  enterimg="window0/qload2.png"  x="& 5 * 80" y="615"]
[button name="role_button" role="backlog"    graphic="window0/log.png"    enterimg="window0/log2.png"    x="& 6 * 80" y="615"]
[button name="role_button" role="window"     graphic="window0/close.png"  enterimg="window0/close2.png"  x="& 7 * 80" y="615"]
[button name="role_button" role="fullscreen" graphic="window0/screen.png" enterimg="window0/screen2.png" x="& 8 * 80" y="615"]
[button name="role_button" role="menu"       graphic="window0/menu.png"   enterimg="window0/menu2.png"   x="& 9 * 80" y="615"]
[button name="role_button" role="sleepgame"  graphic="window0/config.png" enterimg="window0/config2.png" x="&10 * 80" y="615" storage="config.ks"]
[button name="role_button" role="title"      graphic="window0/title.png"  enterimg="window0/title2.png"  x="&11 * 80" y="615"]

[akn/def]Voila.[p]
Now we're back to the original window.[p]



;-----------------------------------------------------------
*Part5.1
;-----------------------------------------------------------

; ★
; ★Various windows. 
; ★

[akn/hap]Now you can make your very own message windows!![p]
[akn/def]Hey.[p]Let's take a look at some different designs for message windows.[p]

; [call] At the very bottom of this scenario file
; you can find subroutine labels with the message window settings.
; These tags call those labels.
[call target="*Sub_Window_1"]

#Akane
This is the default message window.[p]



;-----------------------------------------------------------
*Part5.2
;-----------------------------------------------------------

[call target="*Sub_Window_2"]

#【Akane】
This is a moody window with a black to red gradation.[p]



;-----------------------------------------------------------
*Part5.3
;-----------------------------------------------------------

[call target="*Sub_Window_3"]

#Akane
This is a band-type window that lines up the character name and text windows.[p]



;-----------------------------------------------------------
*Part5.4
;-----------------------------------------------------------

[call target="*Sub_Window_4"]

#【Akane】
This dark to transparent window works with pretty much any background.[p]



;-----------------------------------------------------------
*Part5.5
;-----------------------------------------------------------

[call target="*Sub_Window_5"]

#【Akane】
This window has simplified role buttons.[p]



;-----------------------------------------------------------
*Part5.6
;-----------------------------------------------------------

[call target="*Sub_Window_6"]

#akane
This window is like lined paper.[p]
#akane:happy
And that's just the tip of the iceberg of what you can do!![p]




;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [deffont] Restore the default settings.
[deffont size="40" color="0xffffff" edge="0x000000"]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "8";
[endscript]

; [jump] Jump to the contents page.
[jump storage="select.ks"]



; ★
; ★Below are the subroutine labels with the window settings.
; ★



; ----------------------------------------------------------
*Sub_Window_1
; ----------------------------------------------------------

; Reset the message box.
[hidemenubutton]
[clearfix]
[free name="chara_name_area" layer="message0"]

; Message box design
[position vertical="false"]
[position frame="window1/frame.png" opacity="230" ]
[position width="960" height="210" top="430" left="0"]
[position margint="45" marginr="70" marginb="60" marginl="50"]

; Character name window
[ptext         name="chara_name_area" layer="message0" color="0xfafafa" size="24" x="40" y="435"]
[chara_config ptext="chara_name_area"]

; Font
[deffont color="0x444444" edge="0xffffff"]
[resetfont]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "8";
[endscript]

; Role buttons
[button name="role_button" role="skip"       graphic="window1/skip.png"   enterimg="window1/skip2.png"   x=" 35" y="610"]
[button name="role_button" role="auto"       graphic="window1/auto.png"   enterimg="window1/auto2.png"   x="110" y="610"]
[button name="role_button" role="save"       graphic="window1/save.png"   enterimg="window1/save2.png"   x="185" y="610"]
[button name="role_button" role="load"       graphic="window1/load.png"   enterimg="window1/load2.png"   x="260" y="610"]
[button name="role_button" role="quicksave"  graphic="window1/qsave.png"  enterimg="window1/qsave2.png"  x="335" y="610"]
[button name="role_button" role="quickload"  graphic="window1/qload.png"  enterimg="window1/qload2.png"  x="410" y="610"]
[button name="role_button" role="backlog"    graphic="window1/log.png"    enterimg="window1/log2.png"    x="485" y="610"]
[button name="role_button" role="window"     graphic="window1/close.png"  enterimg="window1/close2.png"  x="560" y="610"]
[button name="role_button" role="fullscreen" graphic="window1/screen.png" enterimg="window1/screen2.png" x="635" y="610"]
[button name="role_button" role="menu"       graphic="window1/menu.png"   enterimg="window1/menu2.png"   x="710" y="610"]
[button name="role_button" role="sleepgame"  graphic="window1/sleep.png"  enterimg="window1/sleep2.png"  x="785" y="610" storage="config.ks"]
[button name="role_button" role="title"      graphic="window1/title.png"  enterimg="window1/title2.png"  x="860" y="610"]

[return]



; ----------------------------------------------------------
*Sub_Window_2
; ----------------------------------------------------------

; Reset the message box
[hidemenubutton]
[clearfix]
[free name="chara_name_area" layer="message0"]

; Message box design
[position left="0" top="480" width="960" height="160" frame="window2/_frame.png" opacity="200"]
[position marginl="218" margint="46" marginr="180"]

; Character name window
[ptext name="chara_name_area" shadow="0x000000" layer="message0" color="0xffffff" size="20" x="217" y="510"]
[chara_config ptext="chara_name_area"]

; Font
[deffont size="22" edge="none" shadow="0x000000" color="0xffffff"]
[resetfont]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "-14";
[endscript]

; Role buttons
[button name="role_button" role="auto"      graphic="window2/auto.png"   enterimg="window2/auto2.png"   x="828" y="498" width="40" hint="Enable auto mode."  ]
[button name="role_button" role="skip"      graphic="window2/skip.png"   enterimg="window2/skip2.png"   x="872" y="498" width="40" hint="Enable skip mode."]
[button name="role_button" role="backlog"   graphic="window2/back.png"   enterimg="window2/back2.png"   x="916" y="498" width="40" hint="Open the backlog."      ]
[button name="role_button" role="save"      graphic="window2/save.png"   enterimg="window2/save2.png"   x="828" y="542" width="40" hint="Open the save window."      ]
[button name="role_button" role="load"      graphic="window2/load.png"   enterimg="window2/load2.png"   x="872" y="542" width="40" hint="Open the load window."      ]
[button name="role_button" role="sleepgame" graphic="window2/config.png" enterimg="window2/config2.png" x="916" y="542" width="40" hint="Open the config window." storage="config.ks"]
[button name="role_button" role="quicksave" graphic="window2/qsave.png"  enterimg="window2/qsave2.png"  x="828" y="584" width="40" hint="Quick save."  ]
[button name="role_button" role="quickload" graphic="window2/qload.png"  enterimg="window2/qload2.png"  x="872" y="584" width="40" hint="Quick load."  ]
[button name="role_button" role="title"     graphic="window2/title.png"  enterimg="window2/title2.png"  x="916" y="584" width="40" hint="Return to the title screen."    ]

[return]



; ----------------------------------------------------------
*Sub_Window_3
; ----------------------------------------------------------

; Reset the message box
[hidemenubutton]
[clearfix]
[free name="chara_name_area" layer="message0"]

; Message box design
[position left="0" top="496" width="960" height="90" frame="window3/_frame.png" opacity="200"]
[position marginl="300" margint="1" marginr="100"]

; Character name window
[ptext name="chara_name_area" shadow="0x000000" layer="message0" color="0xffffff" size="22" x="198" y="512" width="80" align="center"]
[chara_config ptext="chara_name_area"]

; Font
[deffont size="22" edge="none" shadow="0x000000" color="0xffffff"]
[resetfont]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "-14";
[endscript]

; Role buttons
[button name="role_button" role="window"    graphic="window3/close.png"  enterimg="window3/close2.png"  x="940" y="497" width="20" hint="Close the message window."]
[button name="role_button" role="auto"      graphic="window3/auto.png"   enterimg="window3/auto2.png"   x="556" y="594" width="40" hint="Enable auto mode."      ]
[button name="role_button" role="skip"      graphic="window3/skip.png"   enterimg="window3/skip2.png"   x="600" y="594" width="40" hint="Enable skip mode."    ]
[button name="role_button" role="backlog"   graphic="window3/back.png"   enterimg="window3/back2.png"   x="644" y="594" width="40" hint="Open the backlog."          ]
[button name="role_button" role="save"      graphic="window3/save.png"   enterimg="window3/save2.png"   x="688" y="594" width="40" hint="Open the save window."          ]
[button name="role_button" role="load"      graphic="window3/load.png"   enterimg="window3/load2.png"   x="732" y="594" width="40" hint="Open the load window."          ]
[button name="role_button" role="sleepgame" graphic="window3/config.png" enterimg="window3/config2.png" x="776" y="594" width="40" hint="Open the config window." storage="config.ks"]
[button name="role_button" role="quicksave" graphic="window3/qsave.png"  enterimg="window3/qsave2.png"  x="820" y="594" width="40" hint="Quick save."      ]
[button name="role_button" role="quickload" graphic="window3/qload.png"  enterimg="window3/qload2.png"  x="864" y="594" width="40" hint="Quick load."      ]
[button name="role_button" role="title"     graphic="window3/title.png"  enterimg="window3/title2.png"  x="908" y="594" width="40" hint="Return to the title screen."        ]

[return]



; ----------------------------------------------------------
*Sub_Window_4
; ----------------------------------------------------------

; Reset the message box
[hidemenubutton]
[clearfix]
[free name="chara_name_area" layer="message0"]

; Message box design
[position left="0" top="340" width="960" height="300" frame="window4/_frame.png" opacity="200"]
[position marginl="210" margint="160" marginr="162"]

; Character name window
[ptext name="chara_name_area" shadow="0x000000" layer="message0" color="0xffffff" size="24" x="208" y="482"]
[chara_config ptext="chara_name_area"]

; Font
[deffont size="24" edge="none" shadow="0x000000" color="0xffffff"]
[resetfont]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "-8";
[endscript]

; Role buttons
[showmenubutton]

[return]



; ----------------------------------------------------------
*Sub_Window_5
; ----------------------------------------------------------

; Reset the message box
[hidemenubutton]
[clearfix]
[free name="chara_name_area" layer="message0"]

; Message box design
[position left="0" top="420" width="960" height="220" frame="window5/_frame.png" opacity="200"]
[position marginl="210" margint="60" marginr="40"]

; Character name window
[ptext name="chara_name_area" shadow="0x000000" layer="message0" color="0xffffff" size="28" x="205" y="446"]
[chara_config ptext="chara_name_area"]

; Font
[deffont size="28" edge="none" shadow="0x000000" color="0xffffff"]
[resetfont]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "-8";
[endscript]

; Role buttons
[button name="role_button" role="sleepgame" graphic="window5/system.png"  enterimg="window5/system2.png" x="458" y="610" storage="config.ks"]
[button name="role_button" role="save"      graphic="window5/save.png"    enterimg="window5/save2.png"   x="537" y="610"]
[button name="role_button" role="load"      graphic="window5/load.png"    enterimg="window5/load2.png"   x="590" y="610"]
[button name="role_button" role="quicksave" graphic="window5/qsave.png"   enterimg="window5/qsave2.png"  x="648" y="610"]
[button name="role_button" role="quickload" graphic="window5/qload.png"   enterimg="window5/qload2.png"  x="719" y="610"]
[button name="role_button" role="backlog"   graphic="window5/log.png "    enterimg="window5/log2.png"    x="794" y="610"]
[button name="role_button" role="auto"      graphic="window5/auto.png"    enterimg="window5/auto2.png"   x="839" y="610"]
[button name="role_button" role="skip"      graphic="window5/skip.png"    enterimg="window5/skip2.png"   x="897" y="610"]

[return]



; ----------------------------------------------------------
*Sub_Window_6
; ----------------------------------------------------------

; Reset the message box
[hidemenubutton]
[clearfix]
[free name="chara_name_area" layer="message0"]

; Message box design
[position left="0" top="420" width="960" height="220" frame="window6/_frame.png" opacity="255"]
[position marginl="50" margint="50" marginr="160"]

; Character name window
[ptext name="chara_name_area" color="0xf36daa" layer="message0" size="28" x="56" y="440]
[chara_config ptext="chara_name_area"]

; Font
[deffont size="28" color="0x444444" edge="none" shadow="0xffffff"]
[resetfont]
[iscript]
TYRANO.kag.config.defaultLineSpacing = "-3";
[endscript]

; Role buttons
[button name="role_button" role="auto"    graphic="window6/auto.png"    enterimg="window6/auto2.png"   x="810" y="470"]
[button name="role_button" role="skip"    graphic="window6/skip.png"    enterimg="window6/skip2.png"   x="810" y="508"]
[button name="role_button" role="backlog" graphic="window6/back.png"    enterimg="window6/back2.png"   x="810" y="546"]
[button name="role_button" role="menu"    graphic="window6/menu.png"    enterimg="window6/menu2.png"   x="810" y="584"]

[return]