; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]




; == 10_fixbutton.ks =======================================
;
;This tutorial covers the fixed button object. 
;
; ★Main tags introduced:
; [button fix=true] … Places a fixed button. The fixed button is located on the fix layer.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Now let's look at fixed button objects!![l][er]
As you might have guessed from the name, fixed buttons are buttons that remain 'fixed' on the game screen.[l][er]
So far, so easy!![l][er][akn/hap]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

Let's go to it![l][er]



; [button] Place a fixed button.
[button fix="true" target="*Sub_Return" x="145" y="195" graphic="09_button_A1.png" auto_next=no]



Ta-dahh!![l][er]
This is a regular button with a sound effect attached.[l][er]
The player can click the button at any time during the game and it will remain on the screen.[l][er]
Give it a shot. Trying clicking it a few times.[l][er][akn/def]


Fixed buttons are placed using the［button］tag, just like regular image buttons.[l][er][akn/hap]
The only difference is that the fix parameter is set to true.[l][er]
Fixed buttons are placed on a different layer to regular buttons.[l][er]
They are placed on a layer reserved for fixed objects.[l][er][akn/def]
That layer is called the 'fix layer'.[l][er][akn/dok]

...[l][er]
That might seem a little complex at first...[l][er]
But don't worry about that for now![l][er][akn/def]
[jump target="*Part2"]

; ------------------------------------------------------
*Sub_Return

[playse storage="09_button_3.ogg"]
[return]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

Let's see what happens when a fixed button is pressed...[l][er]
Image that a［call］tag is activated at the instant that the button is clicked.[l][er]
Do you know what a［call］tag is? [l][er]
A［call］tag jumps to a label, then returns to the jump origin when it runs into a［return］tag.[l][er]
You can set whether or not the fixed button should proceed to the next tag after running into the ［return］ tag.[l][er]
To put it another way, you can set whether the story should proceed or not at the time that the button is pushed.[l][er]



; [button] Place a fixed button. Using auto_next=yes, the message can proceed while the button is pressed.
[button fix="true" target="*Sub_Return" x="675" y="195" graphic="09_button_B1.png" auto_next=yes]



Okay.[l][er]
The fixed button on the right allows the story to proceed when the button is pressed.[l][er]
Otherwise, it's just the same as the button on the left.[l][er]
Have a go at pressing them and see the difference.[l][er][akn/dok]

...[l][er]
... ...[l][er]
... ... ...[l][er]
How was that? Could you tell the difference?[l][er][akn/def]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

The fixed buttons we placed so far only play a sound effect and don't do anything else...[l][er]
One way to use fixed buttons is to create help screens that are always visible.[l][er]



; [button] Place a fix label.
[button fix="true" target="*Sub_Show" x="30" y="30" graphic="10_fixbutton_A.png" auto_next=false]



Ta-dah!![l][er]
Now if you press the「i」button at the top left, you'll see information about TyranoScript.[l][er][akn/hap]
There's a little bit of a knack to this type of script.[l][er][akn/def]
[jump target="*Part4"]

; ------------------------------------------------------
*Sub_Show

; [button] Place a help screen using a fixed button.
[button fix="true" target="*Sub_Hide" graphic="10_fixbutton_B.png" name="info" auto_next="false"]
; [anim]x2 [wa] Fade in animation → Await completion.
[anim name="info" time="  0" opacity="  0"]
[anim name="info" time="350" opacity="255"]
[wa]
[return]

; ------------------------------------------------------
*Sub_Hide

; [anim][wa] Facde out animation → Await cmpletion.
[anim name="info" time="350" opacity="0"]
[wa]
; [clearfix] You can release a specific fixed button using the name parameter.
[clearfix name="info"]
[return]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

Now let's remove all of the fixed buttons.[l][er]



; [clearfix] Release all fixed buttons.
[clearfix]



I released them.[l][er][akn/dok]
... Oh...[l][er]
The Save and Load buttons at the bottom of the screen disappeared too.[l][er][akn/def]
That's right... The buttons at the bottom of the screen are also a type of fixed button.[l][er]
These are called 'role buttons'. They're called that because they each have a role.[l][er]
Role buttons are super easy to place.[l][er][akn/hap]
All you have to do is set predefined keywords such as 'save' or 'load' as the role parameter![l][er][akn/def]
There are many keywords available to use.[l][er]



; [button]x12 Place lots of role buttons.
[button name="role_button" role="skip"       graphic="window0/skip.png"   enterimg="window0/skip2.png"   x="& 0 * 80" y="615"]
[button name="role_button" role="auto"       graphic="window0/auto.png"   enterimg="window0/auto2.png"   x="& 1 * 80" y="615"]
[button name="role_button" role="save"       graphic="window0/save.png"   enterimg="window0/save2.png"   x="& 2 * 80" y="615"]
[button name="role_button" role="load"       graphic="window0/load.png"   enterimg="window0/load2.png"   x="& 3 * 80" y="615"]
[button name="role_button" role="quicksave"  graphic="window0/qsave.png"  enterimg="window0/qsave2.png"  x="& 4 * 80" y="615"]
[button name="role_button" role="quickload"  graphic="window0/qload.png"  enterimg="window0/qload2.png"  x="& 5 * 80" y="615"]
[button name="role_button" role="backlog"    graphic="window0/log.png"    enterimg="window0/log2.png"    x="& 6 * 80" y="615"]
[button name="role_button" role="window"     graphic="window0/close.png"  enterimg="window0/close2.png"  x="& 7 * 80" y="615"]
[button name="role_button" role="fullscreen" graphic="window0/screen.png" enterimg="window0/screen2.png" x="& 8 * 80" y="615"]
[button name="role_button" role="menu"       graphic="window0/menu.png"   enterimg="window0/menu2.png"   x="& 9 * 80" y="615"]
[button name="role_button" role="sleepgame"  graphic="window0/config.png" enterimg="window0/config2.png" x="&10 * 80" y="615" storage="config.ks"]
[button name="role_button" role="title"      graphic="window0/title.png"  enterimg="window0/title2.png"  x="&11 * 80" y="615"]



Ta-dahh.[l][er]
These are all role buttons.[l][er]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

There are other ways to use these...[p]
Let's place buttons that can control my position.[l][er]



[button fix="true" target="*Sub_Left"  x=" 30" y="30" graphic="10_fixbutton_C.png" auto_next=false]
[button fix="true" target="*Sub_Right" x="130" y="30" graphic="10_fixbutton_D.png" auto_next=false]



All right...[l][er]
If you press the「←」buton, I'll move to the left. Press the「→」button and I'll move to the right.[l][er]
Um. How you might use this... is up to your imagination. [l][er][akn/dok]
[jump target="*Part6"]

; ------------------------------------------------------
*Sub_Left

[chara_move name="akane" left="-=160" anim="true" time="300"]
[return]

; ------------------------------------------------------
*Sub_Right

[chara_move name="akane" left="+=160" anim="true" time="300"]
[return]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------


Fixed buttons are are pretty much only used for role buttons in regular visual novels.[l][er]
But they can be very useful if you want to create a point-and-click game with some advanced gimmicks.[l][er][akn/def]
Once you  get the hang of them, they can really increase the range of creative expression, so do give them a try.[l][er]
... and that ends our tutorial on fixed buttons.[l][er]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]