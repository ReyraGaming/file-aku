; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 05_image.ks ===========================================
;
; This tutorial is about image objects.
;
; ★Main tags introduced:
; [image] … Place an objection on the specified layer.
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's go over how to place image objects onscreen![p]
An 'object' is something that we place on a layer...[p]

; [image] Place an image object.
[image layer="1" x="0" y="0" storage="05_image_A.png" time="700"]

For example, I'm a 'character object' and the character name window is a 'text object'.[p]
Different layers hold different objects, just like this.[p]

; [freeimage] Release a layer.
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ●
; ●A simple example.
; ●

There are various types of objects. Let's start with the basic 'image object'.[p][akn/hap]
To place an image object on a specific layer, we use the［image］tag![p][akn/def]

; [image] Place an image object.
[image layer="1" x="150"  y="150" storage="05_image_B.png"]

Ta-dahh![p]
This image is on foreground layer 1.[p]
It was done using only the x and y coordinates. Simple as can be.[p]

; ●
; ●How to remove.
; ●

Now, how do we remove an image?[p]
You can remove all objects on a layer by using the［freeimage］tag.[p]
Removing an image is also sometimes called 'releasing an image'...[p]
Now let's release foreground layer 1.[p]

; [freeimage] Release the layer.
[freeimage layer="1"]

BOOM!![p][akn/def]
I released it.[p]
... and that is all you need to simply place and release objects.[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; ●
; ●About width and height.
; ●

We also use the［image］tag to set the width and height of an object.[p]
The units we use are pixels.[p]
Let's have a go at it.[p]

; [image]x3 Let's look at three examples where we set and don't set the height and width.
[image layer="1" x=" 80"  y="150" storage="05_image_B.png"                         ]
[image layer="1" x="330"  y="150" storage="05_image_B.png" width="300"             ]
[image layer="1" x="740"  y="150" storage="05_image_B.png" width="100" height="400"]

Ta-dahh! Going from left to right, we have... [p]
[ptext layer="1" text="No&ensp;settings"   x=" 80" y="110" edge="0x000000" size="32"]
an image with no set width or height ,[p]
[ptext layer="1" text="Width&ensp;only"     x="330" y="110" edge="0x000000" size="32"]
an image with only the width set,[p]
[ptext layer="1" text="Width&ensp;&amp;&ensp;height" x="670" y="110" edge="0x000000" size="32"]
and an image with both the width and height set.[p]
If no settings are used, the image will be displayed with its original size.[p]
If either the width or the height only is set, the unset parameter will be automatically set to keep the aspect ratio.[p]
If both are set, the image will be sized to match the settings.[p]
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ●
; ●About fade-in and waiting.
; ●

The［image］tag places objects instantly by default, but...[p]
you can make an object appear over a set duration by using the time parameter.[p]

; [image]x3 An example of using the time parameter.
[image layer="1" x="130"  y="150" storage="05_image_B.png" time="700"]
[image layer="1" x="380"  y="150" storage="05_image_B.png" time="700"]
[image layer="1" x="630"  y="150" storage="05_image_B.png" time="700"]

... just like that.[p]
[freeimage layer="1" time="700"]
By default, the next line of script won't executed until the image is fully displayed.[p]
If you'd like to move to the next line of script as an image is displayed, you can use the wait parameter.[p]

; [image]x3 Use the time parameter, but set wait=false.
[image layer="1" x="130"  y="150" storage="05_image_B.png" time="900" wait="false"]
[image layer="1" x="380"  y="150" storage="05_image_B.png" time="900" wait="false"]
[image layer="1" x="630"  y="150" storage="05_image_B.png" time="900" wait="false"]

Use this to do things like move to the next message box text as the images appear![p]
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

; ●
; ●Set the layer order.
; ●

Now let's see how the 'layer order' works when several objects are placed on a single layer.[p]

; [image]x3 An example with 3 layers.
[image layer="1" x=" 80"  y=" 80" width="200" storage="color/col1.png" time="700"]
[image layer="1" x="140"  y="140" width="200" storage="color/col3.png" time="700"]
[image layer="1" x="200"  y="200" width="200" storage="color/col5.png" time="700"]

New objects are placed on the foremost layer.[p][akn/hap]
But, you can freely set the layer order with the zindex parameter.[p]

; [image]x3 An example of setting the zindex parameter.
[image layer="1" x="580"  y=" 80" width="200" storage="color/col1.png" time="700" zindex="3"]
[image layer="1" x="640"  y="140" width="200" storage="color/col3.png" time="700" zindex="1"]
[image layer="1" x="700"  y="200" width="200" storage="color/col5.png" time="700" zindex="2"]

See? The layer order was different that time.[p]
The higher the zindex value, the further forward the order of the object.[p]
[freeimage layer="1" time="700"]



;-----------------------------------------------------------
*Part5
;-----------------------------------------------------------

; ●
; ●Assigning objects by assigning names.
; ●

You can also assign names to objects.[p]
We do this with the name parameter.[p]
This helps to identify and categorize objects.[p]

; [image]x3 An example of using the name parameter. In order A, B, B.
[image layer="1" x="130"  y="150" storage="05_image_B.png" name="A"]
[image layer="1" x="380"  y="150" storage="05_image_B.png" name="B"]
[image layer="1" x="630"  y="150" storage="05_image_B.png" name="C"]

Assigning a name to an object allows you to control it individually.[p]

; [free] Release object A from foreground layer 1.
[free layer="1" name="A"]

Like that! [l]

; [free] Release object B from foreground layer 1.
[free layer="1" name="B"]

And that![p]

; [image]x3 An example of separating multiple names sepsarated by commas. All objects will be named C.
[image layer="1" x="130"  y="150" storage="05_image_B.png" name="C,A"]
[image layer="1" x="380"  y="150" storage="05_image_B.png" name="C,B"]
[image layer="1" x="630"  y="150" storage="05_image_B.png" name="C,B"]

You can also assign multiple names to a single object.[p]

; [free] Release object A from foreground layer 1.
[free layer="1" name="C"]

... just like that.[p]



;-----------------------------------------------------------
*Part6
;-----------------------------------------------------------

; ●
; ●Set the horizontal position using a keyword.
; ●

...[p]
One last small point..[p][akn/hap]
You can set the horizontal position of an object using keywords as well as numbers![p][akn/def]
Do this by assigning values to the keywords in advance in config.ks.[p]
For example, let's place some images using the keywords 'left', 'left_center', 'center','right_center', and 'right'.[p]

; [image]x5 Example of using the pos parameter.
; The pos parameter can be abbreviated like this: left_center → lc
[image layer="1" pos="l"  y="170" storage="05_image_B.png" width="160"]
[image layer="1" pos="lc" y="150" storage="05_image_B.png" width="160"]
[image layer="1" pos="c"  y="170" storage="05_image_B.png" width="160"]
[image layer="1" pos="rc" y="150" storage="05_image_B.png" width="160"]
[image layer="1" pos="r"  y="170" storage="05_image_B.png" width="160"]

Here's how it works.[p]
[ptext layer="1" text="left"          x=" 40" y="330" edge="0x000000" size="20" align="center" width="160"]
[ptext layer="1" text="left_center"   x="220" y="120" edge="0x000000" size="20" align="center" width="160"]
[ptext layer="1" text="center"        x="400" y="330" edge="0x000000" size="20" align="center" width="160"]
[ptext layer="1" text="right_center"  x="580" y="120" edge="0x000000" size="20" align="center" width="160"]
[ptext layer="1" text="right"         x="760" y="330" edge="0x000000" size="20" align="center" width="160"]
The images were placed using the keywords next to them.[p][akn/hap]
This can be handy, so do bear it in mind.[p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]