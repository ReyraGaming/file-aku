; [set_default_view] This macro creates the explanation screen. (For details, see 「macro.ks」)
[set_default_view]



; == 23_escape.ks ==========================================
;
; An escape game that uses all TyranoScript functions.
;
; ★Main tags introduced:
; [] … 
;
; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

[akn/hap]
I made a mini escape game using the TyranoScript techniques in the technical samples.[p]
[akn/def]
See if you can beat it![p]
[mask time=500]
[wait time=500]
[destroy]
[mask_off time=0]


;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to contents page.
[jump storage="escape/_start.ks"]