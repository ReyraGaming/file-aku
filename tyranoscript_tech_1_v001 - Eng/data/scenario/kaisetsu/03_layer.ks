; [set_default_view] This macro creates the tutorial screen. (For details see 'mcro.ks'.)
[set_default_view]

; [akn/def] A standaloen macro. Display 'Akane' in teh name box and change Akane's expression. Defined in 「macro.ks」.
[akn/def]



; == 03_layer.ks ===========================================
;
; Information about layers.
;
; ★The main tags introduced:
; [layopt   … Toggle a layer between visible and hidden.
; [trans]   … Transition a layer from back to top.
; [wt]      … Wait for the transition to complete.
; [backlay] … Instantly copy the information on the top the layer to the back.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Let's learn about layers.[p]
The TyranoScript game screen is actually made up of a number of 'layers' stacked on each other like deck of cards.[p]
For example, there is a 'base layer', a 'foreground layer', and 'message layer'.[p]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; ★
; ★Let's see them in action.
; ★

Let's look at a visual explanation of this![p]
[image layer="2" page="fore" name="image" storage="03_layer_A.png" y="100" time="700"]
Say we have a game screen like this...[p]
The layers that make up the screen are stacked up like this...[p]

; [image] Let's move the foreground layer 2 to the 'back'. We'll do this using page=back.
[image layer="2" page="back" name="image" storage="03_layer_B.png" y="100" time="0"]

; [trans] Let's carry out a transition on foreground layer 2. We'll move the 'back' to the 'front'.
[trans layer="2" method="fadeInRight" time="700"]

; [wt] Wait for the transition to complete.
[wt]

The names of the layers are below each image.[p]
[ptext layer="2" x=" 10" y="345" text="Base&ensp;Layer"     size="24" align="center" width="240" time="700"]
[ptext layer="2" x="250" y="345" text="Foreground&ensp;Layer"       size="24" align="center" width="240" time="700"]
[ptext layer="2" x="480" y="345" text="Message&ensp;Layer" size="24" align="center" width="240" time="700"]
[ptext layer="2" x="720" y="345" text="Fix&ensp;Layer" size="24" align="center" width="240" time="700"]
There we have it!![p]
There are also 'free layers' which we can use to place objects like selection buttons onscreen.[p]
What's more, the foreground layer and message layers can be split into multiple layers.[p]
... ...[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; ★
; ★Decide the order of the layers.
; ★

There is one important point to bear in mind about layers.[p]
The order in which the layers are stacked is fixed and can't be changed.[p]
As you can see above, layers towards the right are stacked above the layers to the left.[p]
For this reason, it's not possible to place a foreground layer above the messaage layer.[p]

; [freeimage] Release foreground layer 2.
[freeimage layer="2" time="700"]



;-----------------------------------------------------------
*Part3
;-----------------------------------------------------------

; ★
; ★Layers can be visible or hidden. This is toggled using [layopt].
; ★

Layers can be visible or hidden.[p]
Their visibility is toggled using the［layopt］tag.[r]We'll see a couple of examples of how to use this.[p]
If you ever find yourself thinking, 'Why can't I can't see my image?!!'...[p]
it might be because the image's layer is set to 'hidden'.[p]
For example, I'm now on foreground layer 0. Let's change this layer to...[p]

; [layopt] Set foreground layer 0 (Akane's layer) to hidden.
[layopt layer="0" visible="false"]

Hidden![p]
Now I'm invisible!![p]

Now if we set the layer is to visible... [p]

; [layopt] Set foreground layer 0 (Akane's layer) to visible.
[layopt layer="0" visible="true"]

I'm back![p]



;-----------------------------------------------------------
*Part4
;-----------------------------------------------------------

; ★
; ★The concepts of front and back. These are switched using [trans].
; ★

This next part is just a little tricky...[p]
With the exceptions of free layers and fix layers, all layers have a front and a back.[p]
The player usually sees the fronts of the layers, but...[p]
the layers have 'backs' that they usually don't see.[p]
You can use this to stealthily change the back side of the layer, then flip the back to the front.[p]
Use the［trans］tag to flip the back to the front and vice-versa.[p]
What's a good way to describe this...?[l] I know!![p]
It's like flipping a whiteboard!![p]
Imagine writing on the back of a white board, then flipping it over to show the back.[p]
Let's take a look at this in action![p]

; [image]x3 Write on the back of the foreground layer using page=back.
[image layer="1" page="back" x="130"  y="150" storage="05_image_B.png"]
[image layer="1" page="back" x="380"  y="150" storage="05_image_B.png"]
[image layer="1" page="back" x="630"  y="150" storage="05_image_B.png"]

... I've placed 3 images on the back of foreground layer 1.[p]
You can't see them yet because they're on the back of the layer.[p]
And now... [l]

; [trans] Bring the back of foreground layer 1 to the front.
[trans layer="1" time="1000" method="rotateIn"]

I flipped the back to the front!![p]
There we have it.[p]
Related tags are［wt］and［backlay］. Look these up in the TyranoScript reference when you have time.[p]

That's the end of our layers tutorial.[p]
That actually wasn't so hard, was it?[p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]