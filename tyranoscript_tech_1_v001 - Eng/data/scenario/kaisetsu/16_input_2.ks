; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]



; == 16_input_2.ks =========================================
;
; Introduces how to provide the player with a fixed input method.
;
; ★Main tags introduced:
; [iscript] … Execute JavaScript.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

In the last tutorial we saw how to use input fields to have the player freely input text.[p]
Now we'll go one step further and see how to create an on-screen keyboared.[p]
Here is an on-screen keyboard that allows the player to only enter numbers.[l]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

; [image][image][ptext] Show an input field with a black background and white frame in foreground layer 1.
; This time, we'll set the black background as name=black so we can animate later using [anim].
[image layer="1" x="  0" y=" 0" width="960" height="640" storage="color/black.png" name="black"]
[image layer="1" x="375" y="90" width="215" height=" 55" storage="color/white.png" ]
[ptext layer="1" x="375" y="90" name="ptext" overwrite="true" text="" color="0x666666" size="42" width="215" align="right"]

; [anim] Set the black background.
[anim name="black" opacity="100" time="0"]

; [eval] Store '' in the game variable f.input.
[eval exp=" f.input = '' "]

; ※ Side Note ※
; We'll store the player input in the game variable f.input, so let's prep for that.
; '' is a text value, but there's no content.
; This is sometimes called an 'empty string'.

; [button]x12 Place 12 buttons to be used for input. They may be pressed any amount of times, so we use fixed buttons.
; We'll use the exp parameter for the numerical buttons.
; When a button is pressed, a specific value will be stored in the temporary variable tf.num.
[button graphic="../fgimage/num/1.png"    x="366" y="160" exp=" tf.num = '1' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/2.png"    x="444" y="160" exp=" tf.num = '2' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/3.png"    x="522" y="160" exp=" tf.num = '3' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/4.png"    x="366" y="238" exp=" tf.num = '4' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/5.png"    x="444" y="238" exp=" tf.num = '5' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/6.png"    x="522" y="238" exp=" tf.num = '6' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/7.png"    x="366" y="316" exp=" tf.num = '7' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/8.png"    x="444" y="316" exp=" tf.num = '8' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/9.png"    x="522" y="316" exp=" tf.num = '9' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/0.png"    x="444" y="394" exp=" tf.num = '0' " name="fix_phone" fix="true" target="*Sub_Input"]
[button graphic="../fgimage/num/back.png" x="366" y="394"                      name="fix_phone" fix="true" target="*Sub_Back" ]
[button graphic="../fgimage/num/ok.png"   x="522" y="394"                      name="fix_phone"            target="*OK"       ]

; [s] Stop.
[s]

; ------------------------------------------------------
*Sub_Input

; This is the label used when the number button is pressed.
; This label is called by the fix button, so it is a subroutine label.
; By the way, this information 'which button did you use to get here?' can be confirmed by
; checking the value stored in the temporary variable tf.num.

; [iscript]～[endscript] If the final input is less than 8 characters, add the last pressed button to the end of the string.
[iscript]
if (f.input.length < 8) {
f.input = f.input + tf.num
}
[endscript]

; ★ Additional note about JavaScript (1)
; 
; The below codes means 'if the condition is fulfilled, execute'.
; 
;     if (condition) {
;         execute
;     }

; ★ Additional note about JavaScript (2)
;
; The variable f.input contains a text string.
; Actually, text strings have a hidden capability...
; By adding '.length' to the end of the variable, you can see how many characters are in the text string.
; In other words...
; 
;     f.input.length
;
; ... is how many characters there are in the variable f.input.


; [ptext] Overwrite f.input with the content of the text object.
[ptext layer="0" x="0" y="0" name="ptext" overwrite="true" text="&f.input"]

; [return] Return.
[return]

; ------------------------------------------------------
*Sub_Back

; This label removes 1 character. This is also a subroutine label.

; [iscript] Remove the final character in the f.input.
[iscript]
f.input = f.input.substr(0, f.input.length - 1)
[endscript]

; ★ Additional note about JavaScript (3)
;
; By adding '.substr(a, b)' to the end of a variable name that contains a text string,
; you can obtain the string of characters from start point 'a' to end point 'b'.
; Please note: the first character is character '0' rather than 'character '1'.

[ptext layer="0" x="0" y="0" name="ptext" overwrite="true" text="&f.input"]
[return]

; ------------------------------------------------------
*OK

; The confirm label. Actually, the button to jump here is not a fix button, so
; this label is not a fix button. (For this [return] can't be used.)

; [cm][freeimage][clearfix] Release the free layer, foreground layer 1, and
; release the object name=fix_phone on the fix layer.
[cm]
[freeimage layer="1"]
[clearfix name="fix_phone"]

[akn/hap]
You entered '[emb exp="f.input"]'.[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

[akn/def]
That's one example of an on-screen keyboard.[p]
As you see, on-screen keyboards are useful when you want to limit what kind of characters the player can input.[p]



;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]