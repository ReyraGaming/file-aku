; [set_default_view] This macro sets the tutorial screen. For details, see 'macro.ks'.
[set_default_view]

; [akn/def] This macro is definied in 'macro.ks'. It displays 'Akane' in tne name box and sets her expression.
[akn/def]


; == 08_character.ks =======================================
;
; Let's learn how to control characters.
;
; ★Main tags introduced:
; [chara_new]       … Register a character.
; [chara_face]      … Add an expression to a registered character.
; [chara_delete]    … Unregister (delete) a character.
; [chara_show]      … Show a registered character as an object.
; [chara_mod]       … Change the expression of a visible character object.
; [chara_hide]      … Release a visible character object.
; [chara_hide_all]  … Release all visible character objects.
; [chara_ptext]     … Set the text object to be used as the character's name.
; [chara_config]    … Adjust the various character control settings.

; ==========================================================



;-----------------------------------------------------------
*Start
;-----------------------------------------------------------

Tutorial START!![p]
A 'character object' is... [p]
Standing right in front of you!![r]
I'M a character object!![p]
More specifically, I'm a character object on foreground layer 0.[p][akn/dok]
Don't confuse me with a run-of-the-mill image object!![p][akn/def]
Character objects are image objects with LOTS of useful functions.[p]
Basic functions include 'switch image', 'automatic positioning', and 'automatic focus'. There are many more.[p][akn/hap]
So be sure to use character objects for your character!![p][akn/def]



;-----------------------------------------------------------
*Part1
;-----------------------------------------------------------

Moving on...[p]
Let's watch a skit that uses a variety of character object-related tags.[p]
Let the play begin.[p]

; [chara_config] Set the character focus 'brightness', enable the expression memory function, change teh expression change duration to 0.
[chara_config talk_focus="brightness" memory="true" time="0"]

; [chara_new][chara_face]x4 Register Yamato
[chara_new  name="yamato" jname="Yamato" storage="chara/yamato/normal.png" ]
[chara_face name="yamato"  face="angry"  storage="chara/yamato/angry.png"  ]
[chara_face name="yamato"  face="happy"  storage="chara/yamato/happy.png"  ]
[chara_face name="yamato"  face="sad"    storage="chara/yamato/sad.png"    ]
[chara_face name="yamato"  face="tohoho" storage="chara/yamato/tohoho.png" ]

; [chara_new][chara_face]x2 Register the Tyrano mascot character. The name will initially be '???'.
[chara_new   name="tyrano" jname="？？？" storage="chara/mascot_1/normal.png" ]
[chara_face  name="tyrano"  face="happy"  storage="chara/mascot_1/happy.png"  ]
[chara_face  name="tyrano"  face="angry"  storage="chara/mascot_1/angry.png"  ]

; [chara_show] Show the Tyrano mascot.
[chara_show  name="tyrano" zindex="10"]

; #tyrano ← This special expression has the same effect as using [chara_ptext name=tyrano].
#tyrano
*ROAR*!![p]

; #akane:doki ← This special expression has the same effect as using [chara_ptext name=akane face=doki].
#akane:doki
Wh-wh-who are you??[p]

#tyrano:happy
RWAR!!![p]

#akane
I have no idea what you're saying...[p]
I don't even know your name...[p]
I'll just call you... Terri.[p]
... Is that okay with you?[p]

; [iscript]～[endscript] Directly executes JavaScript 
; Change the Tyrano mascot name from '???' to 'Terri'.
[iscript]
TYRANO.kag.stat.charas['tyrano'].jname = 'Terri'
[endscript]

#tyrano:default
RAWR!![p]

#akane:default
How cute... I wonder if she can understand me.[p]

#
RAWR-RAWR.[p]

; [chara_show] Show Yamato.
[chara_show  name="yamato" top="30"]

#yamato
Hey there.[p]

#yamato:angry
What do you have there? A stuffed toy?[p]

; [chara_config] Use the 'up' animation over 150ms.
[chara_config talk_anim="up" talk_anim_time="150"]
[chara_mod name="tyrano" reflect="true"] 
#tyrano:angry
[font size="100"]*GWAR-RAWR!!![p]

#yamato:tohoho
YIKES!!!!![p][resetfont]

; [chara_move] Move Yamato 80 pixels to the left.
[chara_move name="yamato" left="-=80" anim="true" wait="false" time="200"]

H-hey, Akane! What is this thing?!![p]

[chara_mod name="tyrano" face="default"]
; [chara_config] Set the animation to 'none'.
[chara_config talk_anim="none"]
#akane:angry
This is Terri.[p]
Don't call her a 'thing'!![p]

#yamato
... Terri?[p]

#akane:angry
WhatEVER. We have better things to do. Right, Terri?[p]

[chara_mod name="tyrano" reflect="false"] 
#tyrano:default
RAWR.[p]

; [chara_hide]x2 Akane and the Tyrano mascot exit.
[chara_hide name="akane" wait="false" pos_mode="false"]
[chara_hide name="tyrano"]

#yamato:default
... What just happened?[p]

; [chara_show] Akane reappears.
[chara_show name="akane"]
#akane
You were mean to Terri!![p]
C'mere, Yamato!![p]

#yamato:tohoho
Su-sure...[p]

; [chara_config] Set the auto focus to 'none', expression memory to 'disabled', and the focus animation to 'down'.
[chara_config talk_focus="none" memory="false" talk_anim="down"]

; [chara_hide_all] Exit all characters.
[chara_hide_all]

#All three
(Chatter-chatter-chatter)[p]



;-----------------------------------------------------------
*Part2
;-----------------------------------------------------------

; [chara_show]x3 Have all three enter using wait=false.
[chara_show name="akane"  left=520 top=40  wait="false"]
[chara_show name="tyrano" left=330 top=140 wait="false" zindex="10"]
[chara_show name="yamato" ]

#akane
And that was our little skit.[p]
#tyrano:happy
RAWRR!!![p]

[chara_config time="600" talk_anim="none"]

;-----------------------------------------------------------
*End
;-----------------------------------------------------------

; [jump] Jump to the contents page.
[jump storage="select.ks"]