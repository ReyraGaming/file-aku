;Do not delete this file!
;
;make.ks is a special .ks file that is called when data is loaded.
;Add any processes that you would like to rebuild at the time of loading, e.g. resetting the fix layers.
;
;make.ks is called by the [call] tag, so [return] is required at the end.
[return]